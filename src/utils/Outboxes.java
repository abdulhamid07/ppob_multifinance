
package utils;

import ppob_biller.Settings;
import java.sql.*;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author ngonar
 */
public class Outboxes {

    Connection conx = null;
    Connection conOtomax = null;
    Settings setting = new Settings();

    private static final Logger logger = Logger.getLogger(Outboxes.class);

    public Outboxes() {
        //this.conx = setting.getConnection();
        //this.conOtomax = setting.getConnectionOtomax();
    }

    public String getLastMessage(String msg) {
        String result = "";

        try {

            //get regex format for the request
            this.conx = setting.getConnection();
            
            Statement st   = conx.createStatement();
            String sql = "select top 1 message from outboxes where message like '%"+msg+"%' and message not like '%akan segera diproses%' "
                    + " and day(create_date)=day(getdate()) and month(create_date)=month(getdate()) and year(create_date)=year(getdate()) "
                    + " order by id desc";
            
            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            if (rs.next())
                result= rs.getString("message");

            st.close();
            rs.close();
            conx.close();

            return result;

        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return result;
    }

    public void sendToOutbox(String inbox_id, String msg
            ,String receiver, String receiver_type, String transaction_id,
            String user_id, String sender ,String media_type_id, String response_code){
        String sql = "INSERT INTO [outboxes] "+
                         "  ([inbox_id] "+
                         "  ,[message] "+
                         "  ,[status] "+
                         "  ,[create_date] "+
                         "  ,[receiver] "+
                         "  ,[receiver_type] "+
                         "  ,[transaction_id] "+
                         "  ,[user_id] "+                         
                         "  ,[sender] "+
                         "  ,[response_code] "+
                         "  ,[media_type_id]) "+
                     " VALUES  ("+
                     "       ?"+
                     "      ,?"+
                     "      ,'0' "+
                     "      ,getdate() "+
                     "      ,?"+
                     "      ,?"+
                     "      ,?"+
                     "      ,?"+               
                     "      ,?"+
                     "      ,?"+
                     "      ,?)";

       // System.out.println(inbox_id+"-"+msg+"-"+receiver+"-"+transaction_id
       //         +"-"+receiver_type+"-"+user_id+"-"+sender+"-"+media_type_id+"-"+response_code);

        try {
            this.conx = setting.getConnection();
            PreparedStatement st = conx.prepareStatement(sql);
            
            st.setInt   (1, Integer.parseInt(inbox_id));
            st.setString(2, msg);
            st.setString(3, receiver);
            st.setString(4, receiver_type);
            st.setInt   (5, Integer.parseInt(transaction_id));
            st.setInt   (6, Integer.parseInt(user_id));
            st.setString(7, sender);
            st.setString(8, response_code);
            st.setInt   (9, Integer.parseInt(media_type_id));
            
            st.execute();
            st.close();
            conx.close();
        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage());
        }
    }

}
