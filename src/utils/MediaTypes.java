
package utils;

import ppob_biller.Settings;
import java.sql.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;


/**
 *
 * @author ngonar
 */
public class MediaTypes {

    Connection conx = null;
    Settings setting = new Settings();

    public MediaTypes() {
        //this.conx = setting.getConnection();
    }

    public int getMediaTypeId(String name) {
        
        int media_type_id = 0;
        String sql = "select id from media_types where name = '"+name+"'";
        try {
            this.conx = setting.getConnection();
            Statement st = conx.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                media_type_id = rs.getInt("id");
            }

            rs.close();
            st.close();
            conx.close();
        }
        catch(Exception e) {
            System.out.println("Error @MediaType : "+e.getMessage());
        }
        
        return media_type_id;

    }

}
