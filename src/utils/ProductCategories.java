
package utils;

import ppob_biller.Settings;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
/**
 *
 * @author ngonar
 */
public class ProductCategories extends Categories {

    Connection conx = null;
    Settings setting = new Settings();

    private static final Logger logger = Logger.getLogger(ProductCategories.class);

    public ProductCategories() {
        //this.conx = setting.getConnection();

    }

    public void setConnection(java.sql.Connection con) {
        this.conx = con;
    }

    public int getFormatBalasanId(String id, String user_id) {

        try {

            int cat = 0;
            int pin=0, sn=0, pass=0, vcode=0;

            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            String sql = "select top 1 pin, sn, pass, vcode from product_categories where id='"+id+"'";

            //System.out.println(sql);
            //logger.info(sql);
            
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                //cat = rs.getInt("format_balasan_id");

                pin     = rs.getBoolean("pin")?1:0;
                sn      = rs.getBoolean("sn")?1:0;
                pass    = rs.getBoolean("pass")?1:0;
                vcode   = rs.getBoolean("vcode")?1:0;
                
            }

            sql = "select id from format_balasans where user_id = '"+user_id+"' "+
                  " and pin = '"+pin+"' and sn = '"+sn+"' and pass='"+pass+"' "
                  + "and vcode='"+vcode+"' and response_stat_id = 2";

            //logger.info(sql);
            
            rs = st.executeQuery(sql);
            if (rs.next()) {
                cat = rs.getInt("id");
            }

            rs.close();
            st.close();
            conx.close();
            
            return cat;

        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage());
        }

        return 0;
    }
    
    public int getProductCategoryId(String code){

        try {
            int cat = 0;

            this.conx = setting.getConnection();
            Statement st = conx.createStatement();
            String sql = "select top 1 product_category_id from products where code = '"+code+"'";

            //System.out.println(sql);
            //logger.info(sql);

            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                cat = rs.getInt("product_category_id");
            }

            st.close();
            rs.close();
            conx.close();

            return cat;
        }
        catch(Exception e) {
            //System.out.println("Error @ProductCategories");
            logger.log(Level.FATAL,e.getMessage());
        }

        return 0;
    }

    public String[] getStrukturVoucher(int id) {

        String[] struktur = new String[4];

        try {

            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            String sql = "select pin,vcode,pass,sn from product_categories where id = '"+id+"'";

            //System.out.println(sql);
            //logger.info(sql);

            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                
                struktur[0] = rs.getString("pin");
                struktur[1] = rs.getString("vcode");
                struktur[2] = rs.getString("pass");
                struktur[3] = rs.getString("sn");

            }

            st.close();
            rs.close();
            conx.close();

            return struktur;
        }
        catch(Exception e) {
            //System.out.println("Error @ProductCategories");
            logger.log(Level.FATAL,e.getMessage());
        }

        return struktur;
    }

}
