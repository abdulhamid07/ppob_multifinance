/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author Administrator
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//package iso8583;
/**
 *
 * @author ngonar
 */
import java.io.IOException;
import org.jpos.iso.*;

import org.jpos.iso.packager.*;
import java.util.HashMap;
import java.util.Map;

public class ParseISOMessage {

    /*
   public static void main(String[] args) throws IOException, ISOException {
        // Create Packager based on XML that contain DE type
        GenericPackager packager = new GenericPackager("D:/Pelangi/PELANGI-INTAKA/PelangiClientPC/isoSLSascii.xml");
        //GenericPackager packager = new GenericPackager("iso87ascii.xml");

        // Print Input Data
        //String data = "0200B2200000001000000000000000800000201234000000010000011072218012345606A5DFGR021ABCDEFGHIJ 1234567890";
        //String data = "2800001000008181000020091014011452074410101001SLS0000000000001007ST145S3";
        //String data = "21004030004180810000059950200000014338820110201065634601207000000007009010100JTM1504ZMSDY060310000000011121503780000000000000";
        //String data = "210040300041808100000599504000000000006200910140111366015074410000074410101SLS0000000000001044ST145S35436000000016710 54001";

        //response inquiry SLS
        //String data = "211050300041828100000599501360000000005594500000000000220120628151332601007451001707451010300000000000000000002343000000051102010130520284995624628644671172477387768199AMIR MAHMUD              51102022-1234567    R1  000000450000003200201104201002200000000000000016025000000000000000000000000000000000112470001129000000000000000000000000000000000201105201001220000000000000036720000000000000000000000000000000000111610001124700000000000000000000000000000000";

        //String data = "2210503200418281000005995013600000000095620000000025253201207051849282012070660100745100170745101030000000000000000000223300000005110202029381101907125991891045170641707189045U9MARLINA DEWI             51102022-1234567    R1  000002200000001600201105201002200000000000000094020000000000000000000000000000000000276460002769300000000000000000000000000000000";

        //1987 - net req
        //String data = "0800823A0000000000000400000000000000042009061390000109061304200420001";

        //1987 - net response
        //String data = "0810823A000002000000048000000000000004200906139000010906130420042000001031128";

        //1987 - trx req
        //String data = "0200323A40010841801038000000000000000004200508050113921208050420042251320720000010000001156040800411        01251146333156336000299 ";



        //1987 - rev req
//                String data = "0410F23A40010A4182020000004000000000191111111110000000000180000000000030000"+
//                "090806465100331613451909080908601006000200000000000343000003948"+
//                "0380811001200000628110012000004096565733236003000331700039480908064651000000"+
//                "0003132020000331609080645190000000020000000000000";

        //rev response
//                String data = "0400F23A40010841820200000040000000001911111111100000000001800000000000300000"+
//                                "908064651003316134519090809096010060002000000000003430003948"+
//                                "0380811001200000409656573320000000300000136003000331700039480908064651000000"+
//                                "0003132020000331609080645190000000020000000000000";

        //1987 - rev rep req
//                String data = "0401F23A40010841820200000040000000001911111111100000000001800000000000300000"+
//                "908064652003316134519090809096010060002000000000003430003948"+
//                "0380811001200000409656573320000000300000136003000331800039480908064652000000"+
//                "0003132020000331609080645190000000020000000000000";

        String data = "0410F23A40010A4182020000004000000000191111111110000000000180000000000030000"
                + "090806465200331613451909080908601006000200000000000343940003948        "
                + "0380811001200000409656573320000000300000136003000331800039480908064652000000"
                + "0003132020000331609080645190000000020000000000000 ";

        data = "211040300041828100000599502000001171338201207251614206010074510017074510103003300000000000000020310000000000000000000000000000000";

        System.out.println("DATA : " + data);

Map<String, String> bitMapNyo = new HashMap<String, String>();
bitMapNyo = getBitmap(data);

        //System.out.println(String.format("%10s", "foo").replace(' ', '0'));

        // Create ISO Message
        ISOMsg isoMsg = new ISOMsg();
        isoMsg.setPackager(packager);
        isoMsg.unpack(data.getBytes());

        String fieldDescription = packager.getFieldDescription(isoMsg, 2);
        System.out.println("desctiption : " + fieldDescription);
        // print the DE list
        logISOMsg(isoMsg, packager);
    }

     *
     *
     */
    private static void logISOMsg(ISOMsg msg, GenericPackager packager) {
        System.out.println("----ISO MESSAGE-----");
        try {
            /*	System.out.println("  MTI : " + msg.getMTI());
            for (int i=1;i<=msg.getMaxField();i++) {
            if (msg.hasField(i)) {
            System.out.println("    Field-"+i+" : "+msg.getString(i));
            }
            }
             *
             */
            System.out.println("  MTI : " + msg.getMTI());

            System.out.println("  Bitmap : " + msg.getValue(-1).toString());

            String bitmapString = msg.getValue(-1).toString();
            bitmapString = bitmapString.replace("}", "");
            bitmapString = bitmapString.replace("{", "");
            String[] //bitmapArray = { msg.getMTI() };
                    bitmapArray = bitmapString.split(",");

            for (int i = 0; i < bitmapArray.length; i++) {
                String string = bitmapArray[i].trim();
                System.out.println("bitmap " + i + " : " + packager.getFieldDescription(msg, Integer.parseInt(string)) + string.trim());
            }
            System.out.println("=====================================");

            System.out.println("  2 : " + msg.getString(2));
            System.out.println("  3 : " + msg.getString(3));
            System.out.println("  4 : " + msg.getString(4));
            System.out.println("  5 : " + msg.getString(5));
            System.out.println("  6 : " + msg.getString(6));
            System.out.println("  7 : " + msg.getString(7));
            System.out.println("  11 : " + msg.getString(11));
            System.out.println("  12 : " + msg.getString(12));
            System.out.println("  13 : " + msg.getString(13));
            System.out.println("  15 : " + msg.getString(15));
            System.out.println("  18 : " + msg.getString(18));
            System.out.println("  26 : " + msg.getString(26));
            System.out.println("  32 : " + msg.getString(32));
            System.out.println("  33 : " + msg.getString(33));
            System.out.println("  37 : " + msg.getString(37));
            System.out.println("  39 : " + msg.getString(39));
            System.out.println("  40 : " + msg.getString(40));
            System.out.println("  41 : " + msg.getString(41));
            System.out.println("  42 : " + msg.getString(42));
            System.out.println("  48 : " + msg.getString(48));
            System.out.println("  49 : " + msg.getString(49));
            System.out.println("  55 : " + msg.getString(55));
            System.out.println("  60 : " + msg.getString(60));
            System.out.println("  63 : " + msg.getString(63));
            System.out.println("  70 : " + msg.getString(70));
            System.out.println("  73 : " + msg.getString(73));
            System.out.println("  90 : " + msg.getString(90));

            // String[] iwan = parseBit48Inquiry(msg.getString(48));

            //System.out.println("iwan : "+iwan[3]);

        } catch (ISOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("--------------------");
        }

    }

    public static String[] parseBit48Inquiry(String msg) {
        String[] hasil = new String[65];

        try {
            hasil[0] = msg.substring(0, 7);
            System.out.println("switcher id : " + hasil[0]);// switcher id - 7
            hasil[1] = msg.substring(7, 19);
            System.out.println("sub id : " + hasil[1]);// subscriber id - 12
            hasil[2] = String.valueOf(msg.charAt(19));
            System.out.println("bill status : " + hasil[2]); //bill status - 1
            hasil[3] = msg.substring(20, 22);
            System.out.println("outstanding bill : " + hasil[3]); //outstanding bill - 2
            hasil[4] = msg.substring(22, 54);
            System.out.println("switcher ref : " + hasil[4]); //switcher ref no - 32
            hasil[5] = msg.substring(54, 79);
            System.out.println("sub name : " + hasil[5]); //Subscriber name - 25
            hasil[6] = msg.substring(79, 84);
            System.out.println("service unit : " + hasil[6]); //Service unit - 5
            hasil[7] = msg.substring(84, 99);
            System.out.println("service unit phone : " + hasil[7]); //Service unit phone - 15
            hasil[8] = msg.substring(99, 103);
            System.out.println("sub segmentation : " + hasil[8]); //subscriber segmentation - 4
            hasil[9] = msg.substring(103, 112);
            System.out.println("power consuming cat : " + hasil[9]);  //Power consuming cat - 9
            hasil[10] = msg.substring(112, 121);
            System.out.println("total admin charge : " + hasil[10]); //Total admin charges - 9

            hasil[11] = msg.substring(121, 127);
            System.out.println("bill period : " + hasil[11]); //bill period - 6
            hasil[12] = msg.substring(127, 135);
            System.out.println("due date : " + hasil[12]); //due date - 8
            hasil[13] = msg.substring(135, 143);
            System.out.println("meter read date : " + hasil[13]); //meter read date - 8
            hasil[14] = msg.substring(143, 154);
            System.out.println("total electricity bill : " + hasil[14]); //total electricity bill - 11
            hasil[15] = msg.substring(154, 165);
            System.out.println("incentive : " + hasil[15]); //incentive - 11
            hasil[16] = msg.substring(165, 175);
            System.out.println("vat : " + hasil[16]); //vat - 10
            hasil[17] = msg.substring(175, 184);
            System.out.println("penalty fee : " + hasil[17]); //penalty fee - 9
            hasil[18] = msg.substring(184, 192);
            System.out.println("prev meter reading : " + hasil[18]); //prev meter reading 1 - 8
            hasil[19] = msg.substring(192, 200);
            System.out.println("curr meter reading : " + hasil[19]); //curr meter reading 1 - 8
            hasil[20] = msg.substring(200, 208);
            System.out.println("prev meter reading 2 : " + hasil[20]); //prev meter reading 2 - 8
            hasil[21] = msg.substring(208, 216);
            System.out.println("curr meter reading 2 : " + hasil[21]); //curr meter reading 2 - 8
            hasil[22] = msg.substring(216, 224);
            System.out.println("prev meter reading 3 : " + hasil[22]); //prev meter reading 3 - 8
            hasil[23] = msg.substring(224, 232);
            System.out.println("curr meter reading 3 : " + hasil[23]); //curr meter reading 3 - 8


            if (Integer.parseInt(hasil[2]) > 1) {
                hasil[24] = msg.substring(232, 238); //bill period - 6
                hasil[25] = msg.substring(238, 246); //due date - 8
                hasil[26] = msg.substring(246, 254); //meter read date - 8
                hasil[27] = msg.substring(254, 265); //total electricity bill - 11
                hasil[28] = msg.substring(265, 276); //incentive - 11
                hasil[29] = msg.substring(276, 286); //vat - 10
                hasil[30] = msg.substring(286, 295); //penalty fee - 9
                hasil[31] = msg.substring(295, 303); //prev meter reading 1 - 8
                hasil[32] = msg.substring(303, 311); //curr meter reading 1 - 8
                hasil[33] = msg.substring(311, 319); //prev meter reading 2 - 8
                hasil[34] = msg.substring(319, 327); //prev meter reading 2 - 8
                hasil[35] = msg.substring(327, 335); //prev meter reading 3 - 8
                hasil[36] = msg.substring(335, 343); //prev meter reading 3 - 8
            }

            if (Integer.parseInt(hasil[2]) > 2) {
                hasil[37] = msg.substring(343, 349);
                hasil[38] = msg.substring(349, 357);
                hasil[39] = msg.substring(357, 365);
                hasil[40] = msg.substring(365, 376);
                hasil[41] = msg.substring(376, 387);
                hasil[42] = msg.substring(387, 397);
                hasil[43] = msg.substring(397, 406);
                hasil[44] = msg.substring(406, 414);
                hasil[45] = msg.substring(414, 422);
                hasil[46] = msg.substring(422, 430);
                hasil[47] = msg.substring(430, 438);
                hasil[48] = msg.substring(438, 446);
                hasil[49] = msg.substring(446, 454);
            }

            if (Integer.parseInt(hasil[2]) > 3) {
                hasil[50] = msg.substring(454, 460);
                hasil[51] = msg.substring(460, 468);
                hasil[52] = msg.substring(468, 476);
                hasil[53] = msg.substring(476, 487);
                hasil[54] = msg.substring(487, 498);
                hasil[55] = msg.substring(498, 508);
                hasil[56] = msg.substring(508, 517);
                hasil[57] = msg.substring(517, 525);
                hasil[58] = msg.substring(525, 533);
                hasil[59] = msg.substring(533, 541);
                hasil[60] = msg.substring(541, 549);
                hasil[61] = msg.substring(549, 557);
                hasil[62] = msg.substring(557, 564);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return hasil;
    }

    public Map<String, String> getBitmap(String data) throws ISOException {
        // Create Packager based on XML that contain DE type
        Map<String, String> bitMap = new HashMap<String, String>();

        GenericPackager packager = new GenericPackager("packager/iso87ascii.xml");

        // Create ISO Message
        ISOMsg isoMsg = new ISOMsg();
        isoMsg.setPackager(packager);

        isoMsg.unpack(data.getBytes());

        //isoMsg.get
        String bitmapString = isoMsg.getValue(-1).toString();
        bitmapString = bitmapString.replace("}", "");
        bitmapString = bitmapString.replace("{", "");
        String[] bitmapArray = bitmapString.split(",");

        System.out.println("=================GETBITMAP=====================");
        bitMap.put("MTI", isoMsg.getMTI());
        System.out.println("MTI " + bitMap.get("MTI"));

        for (int i = 0; i < bitmapArray.length; i++) {
            String bitId = bitmapArray[i].trim();

            bitMap.put(bitId, isoMsg.getString(bitId));
            System.out.println("bitmap " + bitId + " : " + bitMap.get(bitId));
        }
        
        System.out.println("=================GETBITMAP=====================");
        return bitMap;
        //return bitmapArray;
    }
}
