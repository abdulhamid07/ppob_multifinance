
package utils;

import ppob_biller.Settings;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author ngonar
 */
public class FormatBalasans {

    Connection conx = null;
    Connection conOtomax = null;
    Settings setting = new Settings();
    ProductCategories prodCat = new ProductCategories();
    Products prod = new Products();

    private static final Logger logger = Logger.getLogger(FormatBalasans.class);

    public FormatBalasans() {
        //this.conx = setting.getConnection();
        //this.conOtomax = setting.getConnectionOtomax();
    }

    public String getFormatResponse(String code) {
        String format = "";

        try {
            String sql = "select response from format_responses where code='"+code+"'";

            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                format = rs.getString("response");
            }

            rs.close();
            st.close();
            conx.close();

        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage());
        }

        return format;
    }

    public String produkSalah() {

        String result = "908";

        try {
            
        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage());
        }

        return "";
    }

    public String tujuanSalah() {
        String result = "906";

        try {
            
        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage());
        }

        return result;
    }

    public String getStatusName(String code) {

        String hasil = "";

        try {
            String sql = "select name from response_stats where code='"+code+"'";

            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                hasil = rs.getString("name");
            }

            rs.close();
            st.close();
            conx.close();

            return hasil;

        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage());
        }

        return hasil;

    }

    public int getFormatBalasanId(int user_id, String response_code, String produk) {
        try {
            
            String sql = "select top 1 format_balasans.id from format_balasans "+
                         " left outer join response_stats "+
                         " on response_stats.id = format_balasans.response_stat_id " +
                         " where user_id = "+user_id+" and response_stats.code='"+response_code+"'";

            //logger.log(Level.INFO,sql);

            int format_balasan_id = 0;

            this.conx = setting.getConnection();
            Statement st = conx.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                format_balasan_id = rs.getInt("id");
            }
            else {
                format_balasan_id = 4;
            }

            st.close();
            rs.close();
            conx.close();

            return format_balasan_id;
            
        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage());
        }

        return 4;
    }

    public String fillUpFormat(String format) {

        try {
            //loop for all defined columns

            //replace by the key and the source

            //return the result
        }
        catch(Exception e) {            
        }

        return "";
    }

    /*public void sendToOtomax(String hp, String msg) {
        try
        {
            
            
            String sql     = "INSERT INTO outbox "+
                                "(tgl_entri, "+
                                "tgl_status, "+
                                "pengirim, "+
                                "penerima, "+
                                "tipe_penerima,"+
                                "pesan,"+
                                "kode_reseller,"+
                                "kode_inbox,"+
                                "kode_transaksi,"+
                                "status,"+
                                "bebas_biaya,prioritas) "+
                                "VALUES "+
                                "(GETDATE(),"+
                                "GETDATE(),'',"+
                                "'"+hp+"',"+
                                "'S',"+
                                "?,"+
                                "'',"+
                                "0,"+
                                "NULL,"+
                                "0,0,1)";

            conOtomax = setting.getConnectionOtomax();
            PreparedStatement st   = conOtomax.prepareStatement(sql);

            st.setString(1, msg);
            st.executeUpdate();
            
            //logger.log(Level.INFO,sql);

            st.close();
            conOtomax.close();
        }
        catch (Exception e)
        {
            System.out.println("Error @Format Balasan SMS");
            logger.log(Level.INFO, "Error @Format Balasan : {0} SMS"+e.getMessage());
            
        }
    }
    */
    
    public static String getCharacterDataFromElement(Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
          CharacterData cd = (CharacterData) child;
          return cd.getData();
        }
        return "";
    }

    public String getFormatBalasanPLNBuy(String xml, String user_id, String trxid) {

        try {

            String transid="",partnertid="",restype="",rescode="",idpel="",nama="",sntoken="",plnref="",refnbr="";
            String kwh="",tagihan="",admin="",meterai="",ppn="",ppj="",angsuran="",rptoken="",jmkwh="",info="",saldo="";
            String snidpel = "",snmeter="",setlement="", desc="";

            xml = xml.toUpperCase();

            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("XML");

            for (int i = 0; i < nodes.getLength(); i++) {
              Element element = (Element) nodes.item(i);

              try {
              NodeList name = element.getElementsByTagName("TRANSID");
              Element line = (Element) name.item(0);
              System.out.println("transid: " + getCharacterDataFromElement(line));
              transid = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList title = element.getElementsByTagName("PARTNERTID");
              Element line = (Element) title.item(0);
              System.out.println("partnertid: " + getCharacterDataFromElement(line));
              partnertid = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList rescodex = element.getElementsByTagName("RESCODE");
              Element line = (Element) rescodex.item(0);
              System.out.println("rescode: " + getCharacterDataFromElement(line));
              rescode = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList restypex = element.getElementsByTagName("RESTYPE");
              Element line = (Element) restypex.item(0);
              System.out.println("restype: " + getCharacterDataFromElement(line));
              restype = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList idpelx = element.getElementsByTagName("IDPEL");
              Element line = (Element) idpelx.item(0);
              System.out.println("idpel: " + getCharacterDataFromElement(line));
              idpel = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("NAMA");
              Element line = (Element) namax.item(0);
              System.out.println("nama: " + getCharacterDataFromElement(line));
              nama = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SNTOKEN");
              Element line = (Element) namax.item(0);
              System.out.println("sntoken: " + getCharacterDataFromElement(line));
              sntoken = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("PLNREF");
              Element line = (Element) namax.item(0);
              System.out.println("plnref: " + getCharacterDataFromElement(line));
              plnref = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("REFNBR");
              Element line = (Element) namax.item(0);
              System.out.println("plnref: " + getCharacterDataFromElement(line));
              refnbr = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList kwhx = element.getElementsByTagName("KWH");
              Element line = (Element) kwhx.item(0);
              System.out.println("kwh: " + getCharacterDataFromElement(line));
              kwh = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("TAGIHAN");
              Element line = (Element) namax.item(0);
              System.out.println("tagihan: " + getCharacterDataFromElement(line));
              tagihan = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("ADMIN");
              Element line = (Element) namax.item(0);
              System.out.println("admin: " + getCharacterDataFromElement(line));
              admin = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("METERAI");
              Element line = (Element) namax.item(0);
              System.out.println("meterai: " + getCharacterDataFromElement(line));
              meterai = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("PPN");
              Element line = (Element) namax.item(0);
              System.out.println("ppn: " + getCharacterDataFromElement(line));
              ppn = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("PPJ");
              Element line = (Element) namax.item(0);
              System.out.println("ppj: " + getCharacterDataFromElement(line));
              ppj = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("ANGSURAN");
              Element line = (Element) namax.item(0);
              System.out.println("angsuran: " + getCharacterDataFromElement(line));
              angsuran = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("RPTOKEN");
              Element line = (Element) namax.item(0);
              System.out.println("RPTOKEN: " + getCharacterDataFromElement(line));
              rptoken = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("JMKWH");
              Element line = (Element) namax.item(0);
              System.out.println("jmkwh: " + getCharacterDataFromElement(line));
              jmkwh = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("INFO");
              Element line = (Element) namax.item(0);
              System.out.println("info: " + getCharacterDataFromElement(line));
              info = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SALDO");
              Element line = (Element) namax.item(0);
              System.out.println("saldo: " + getCharacterDataFromElement(line));
              saldo = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SNIDPEL");
              Element line = (Element) namax.item(0);
              System.out.println("snidpel: " + getCharacterDataFromElement(line));
              snidpel = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SNMETER");
              Element line = (Element) namax.item(0);
              System.out.println("snmeter: " + getCharacterDataFromElement(line));
              snmeter = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SETLEMENT");
              Element line = (Element) namax.item(0);
              System.out.println("settlement: " + getCharacterDataFromElement(line));
              setlement = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList ket = element.getElementsByTagName("DESC");
              Element line = (Element) ket.item(0);
              System.out.println("ket: " + getCharacterDataFromElement(line));
              desc = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

        }

                this.conx = setting.getConnection();
                Statement st   = conx.createStatement();
                String format = "";

                String sql = " select * from format_balasans with(nolock) "+
                             " where user_id = "+user_id+" and name = 'PLN-PRE-SUCCESS'";

                //System.out.println(sql);
                //logger.log(Level.INFO,sql);

                ResultSet rs = st.executeQuery(sql);
                if (rs.next()) {
                    format = rs.getString("format");
                }

                Calendar currentDate = Calendar.getInstance();
                SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                String dateNow = formatter.format(currentDate.getTime());

                NumberFormat formatter2 = new DecimalFormat("#,###,###");

                sql = "select price from users "
                        + " left outer join product_prices on product_prices.price_template_id = users.price_template_id "
                        + " where users.id = '"+user_id+"' and product_prices.product_id ='80'";

                double fee = 0;

                rs = st.executeQuery(sql);
                if (rs.next()) {
                    fee = rs.getInt("price");
                }
                else {
                    fee = 0;
                }

                Saldo mutasi = new Saldo();
                saldo = String.valueOf(mutasi.getBalance(user_id));

                format = format.replace("[Keterangan]",desc);
                format = format.replace("[TrxId]",trxid);
                format = format.replace("[TRANSID]",transid);
                format = format.replace("[PARTNERTID]",partnertid);
                format = format.replace("[RESTYPE]",restype);
                format = format.replace("[RESCODE]",rescode);
                format = format.replace("[IDPEL]",idpel);
                format = format.replace("[NAMA]",nama);
                format = format.replace("[SNTOKEN]",sntoken);
                format = format.replace("[PLNREF]",plnref);
                format = format.replace("[REFNBR]",refnbr);
                format = format.replace("[KWH]",kwh);
                format = format.replace("[TAGIHAN]",tagihan);
                format = format.replace("[ADMIN]",admin);
                format = format.replace("[METERAI]",meterai);
                format = format.replace("[PPN]",ppn);
                format = format.replace("[PPJ]",ppj);
                format = format.replace("[ANGSURAN]",angsuran);
                format = format.replace("[RPTOKEN]",rptoken);
                format = format.replace("[JMKWH]",jmkwh);
                format = format.replace("[INFO]",info);
                //format = format.replace("[SALDO]",saldo);
                format = format.replace("[SNIDPEL]",snidpel);
                format = format.replace("[SNMETER]",snmeter);
                format = format.replace("[SETLEMENT]",setlement);
                format = format.replace("[SETTLEMENT]",setlement);

                format = format.replace("[Harga]",formatter2.format(Double.parseDouble(tagihan)-fee));
                format = format.replace("[Saldo]",formatter2.format(Double.parseDouble(saldo)-(Double.parseDouble(tagihan)-fee)));
                format = format.replace("[SALDO]",formatter2.format(Double.parseDouble(saldo)-(Double.parseDouble(tagihan)-fee)));
                format = format.replace("[Saldo Sebelumnya]",formatter2.format(Double.parseDouble(saldo)));
                format = format.replace("[Status]","SUKSES");
                format = format.replace("[Tgl]",dateNow);

                st.close();
                rs.close();
                conx.close();

                return format;

            }
            catch(Exception e) {
                logger.log(Level.FATAL, "Error @getFormatBalasan SMS : {0}"+ e.getMessage());

            }

        return xml;
    }

    public String getFormatBalasanPLNBuy_XMPP(String xml, String user_id, String trxid) {

        try {

            String transid="",partnertid="",restype="",rescode="",idpel="",nama="",sntoken="",plnref="",refnbr="";
            String kwh="",tagihan="",admin="",meterai="",ppn="",ppj="",angsuran="",rptoken="",jmkwh="",info="",saldo="";
            String snidpel = "",snmeter="",setlement="", desc="";

            xml = xml.toUpperCase();

            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("XML");

            for (int i = 0; i < nodes.getLength(); i++) {
              Element element = (Element) nodes.item(i);

              try {
              NodeList name = element.getElementsByTagName("TRANSID");
              Element line = (Element) name.item(0);
              System.out.println("transid: " + getCharacterDataFromElement(line));
              transid = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList title = element.getElementsByTagName("PARTNERTID");
              Element line = (Element) title.item(0);
              System.out.println("partnertid: " + getCharacterDataFromElement(line));
              partnertid = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList rescodex = element.getElementsByTagName("RESCODE");
              Element line = (Element) rescodex.item(0);
              System.out.println("rescode: " + getCharacterDataFromElement(line));
              rescode = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList restypex = element.getElementsByTagName("RESTYPE");
              Element line = (Element) restypex.item(0);
              System.out.println("restype: " + getCharacterDataFromElement(line));
              restype = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList idpelx = element.getElementsByTagName("IDPEL");
              Element line = (Element) idpelx.item(0);
              System.out.println("idpel: " + getCharacterDataFromElement(line));
              idpel = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("NAMA");
              Element line = (Element) namax.item(0);
              System.out.println("nama: " + getCharacterDataFromElement(line));
              nama = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SNTOKEN");
              Element line = (Element) namax.item(0);
              System.out.println("sntoken: " + getCharacterDataFromElement(line));
              sntoken = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("PLNREF");
              Element line = (Element) namax.item(0);
              System.out.println("plnref: " + getCharacterDataFromElement(line));
              plnref = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("REFNBR");
              Element line = (Element) namax.item(0);
              System.out.println("plnref: " + getCharacterDataFromElement(line));
              refnbr = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList kwhx = element.getElementsByTagName("KWH");
              Element line = (Element) kwhx.item(0);
              System.out.println("kwh: " + getCharacterDataFromElement(line));
              kwh = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("TAGIHAN");
              Element line = (Element) namax.item(0);
              System.out.println("tagihan: " + getCharacterDataFromElement(line));
              tagihan = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("ADMIN");
              Element line = (Element) namax.item(0);
              System.out.println("admin: " + getCharacterDataFromElement(line));
              admin = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("METERAI");
              Element line = (Element) namax.item(0);
              System.out.println("meterai: " + getCharacterDataFromElement(line));
              meterai = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("PPN");
              Element line = (Element) namax.item(0);
              System.out.println("ppn: " + getCharacterDataFromElement(line));
              ppn = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("PPJ");
              Element line = (Element) namax.item(0);
              System.out.println("ppj: " + getCharacterDataFromElement(line));
              ppj = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("ANGSURAN");
              Element line = (Element) namax.item(0);
              System.out.println("angsuran: " + getCharacterDataFromElement(line));
              angsuran = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("RPTOKEN");
              Element line = (Element) namax.item(0);
              System.out.println("RPTOKEN: " + getCharacterDataFromElement(line));
              rptoken = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("JMKWH");
              Element line = (Element) namax.item(0);
              System.out.println("jmkwh: " + getCharacterDataFromElement(line));
              jmkwh = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("INFO");
              Element line = (Element) namax.item(0);
              System.out.println("info: " + getCharacterDataFromElement(line));
              info = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SALDO");
              Element line = (Element) namax.item(0);
              System.out.println("saldo: " + getCharacterDataFromElement(line));
              saldo = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SNIDPEL");
              Element line = (Element) namax.item(0);
              System.out.println("snidpel: " + getCharacterDataFromElement(line));
              snidpel = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SNMETER");
              Element line = (Element) namax.item(0);
              System.out.println("snmeter: " + getCharacterDataFromElement(line));
              snmeter = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SETLEMENT");
              Element line = (Element) namax.item(0);
              System.out.println("settlement: " + getCharacterDataFromElement(line));
              setlement = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList ket = element.getElementsByTagName("DESC");
              Element line = (Element) ket.item(0);
              System.out.println("ket: " + getCharacterDataFromElement(line));
              desc = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

        }

                this.conx = setting.getConnection();
                Statement st   = conx.createStatement();
                String format = "";

                Calendar currentDate = Calendar.getInstance();
                SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                String dateNow = formatter.format(currentDate.getTime());

                NumberFormat formatter2 = new DecimalFormat("#,###,###");

                String sql = "select price from users "
                        + " left outer join product_prices on product_prices.price_template_id = users.price_template_id "
                        + " where users.id = '"+user_id+"' and product_prices.product_id ='80'";

                double fee = 0;

                ResultSet rs = st.executeQuery(sql);
                if (rs.next()) {
                    fee = rs.getInt("price");
                }
                else {
                    fee = 0;
                }

                Saldo mutasi = new Saldo();
                saldo = String.valueOf(mutasi.getBalance(user_id));

                format = format.replace("[Keterangan]",desc);
                format = format.replace("[TrxId]",trxid);
                format = format.replace("[TRANSID]",transid);
                format = format.replace("[PARTNERTID]",partnertid);
                format = format.replace("[RESTYPE]",restype);
                format = format.replace("[RESCODE]",rescode);
                format = format.replace("[IDPEL]",idpel);
                format = format.replace("[NAMA]",nama);
                format = format.replace("[SNTOKEN]",sntoken);
                format = format.replace("[PLNREF]",plnref);
                format = format.replace("[REFNBR]",refnbr);
                format = format.replace("[KWH]",kwh);
                format = format.replace("[TAGIHAN]",tagihan);
                format = format.replace("[ADMIN]",admin);
                format = format.replace("[METERAI]",meterai);
                format = format.replace("[PPN]",ppn);
                format = format.replace("[PPJ]",ppj);
                format = format.replace("[ANGSURAN]",angsuran);
                format = format.replace("[RPTOKEN]",rptoken);
                format = format.replace("[JMKWH]",jmkwh);
                format = format.replace("[INFO]",info);
                //format = format.replace("[SALDO]",saldo);
                format = format.replace("[SNIDPEL]",snidpel);
                format = format.replace("[SNMETER]",snmeter);
                format = format.replace("[SETLEMENT]",setlement);
                format = format.replace("[SETTLEMENT]",setlement);

                format = format.replace("[Harga]",formatter2.format(Double.parseDouble(tagihan)-fee));
                format = format.replace("[Saldo]",formatter2.format(Double.parseDouble(saldo)-(Double.parseDouble(tagihan)-fee)));
                format = format.replace("[SALDO]",formatter2.format(Double.parseDouble(saldo)-(Double.parseDouble(tagihan)-fee)));
                format = format.replace("[Saldo Sebelumnya]",formatter2.format(Double.parseDouble(saldo)));
                format = format.replace("[Status]","SUKSES");
                format = format.replace("[Tgl]",dateNow);

                format  = "STRUK PEMBELIAN LISTRIK PRABAYAR\n\n";
                format += "TANGGAL\t\t: "+dateNow+"\n";
                format += "NO RESI\t\t: "+refnbr+"\n";
                format += "NO METER\t: "+idpel+"\n";
                format += "NAMA\t\t: "+nama+"\n";
                format += "TARIF/DAYA\t: "+kwh+"\n";
                format += "GSP REF\t\t: "+plnref+"\n";
                format += "JML KWH\t\t: "+jmkwh+"\n";
                format += "METERAI\t\t: Rp. "+formatter2.format(Double.parseDouble(meterai))+"\n";
                format += "PPN\t\t: Rp. "+formatter2.format(Double.parseDouble(ppn))+"\n";
                format += "PPJ\t\t: Rp. "+formatter2.format(Double.parseDouble(ppj))+"\n";
                format += "ANGSURAN\t: Rp. "+formatter2.format(Double.parseDouble(angsuran))+"\n";
                format += "RP TOKEN\t: Rp. "+formatter2.format(Double.parseDouble(rptoken))+"\n";
                format += "ADMIN BANK\t: Rp. "+formatter2.format(Double.parseDouble(admin))+"\n";
                format += "======================\n";
                format += "RP BAYAR\t: Rp. "+formatter2.format(Double.parseDouble(tagihan))+"\n\n";
                format += "TOKEN\t\t: "+sntoken+"\n\n";
                format += "TERIMA KASIH\n";
                format += ""+info;

                st.close();
                rs.close();
                conx.close();

                return format;

            }
            catch(Exception e) {
                logger.log(Level.FATAL, "Error @getFormatBalasan SMS : {0}"+ e.getMessage());

            }

        return xml;
    }

    public String getFormatBalasanPLNBuyGanda(String xml, String user_id, String trxid) {

        try {

            String transid="",partnertid="",restype="",rescode="",idpel="",nama="",sntoken="",plnref="",refnbr="";
            String kwh="",tagihan="",admin="",meterai="",ppn="",ppj="",angsuran="",rptoken="",jmkwh="",info="",saldo="";
            String snidpel = "",snmeter="",setlement="", desc="";

            xml = xml.toUpperCase();

            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("XML");

            for (int i = 0; i < nodes.getLength(); i++) {
              Element element = (Element) nodes.item(i);

              try {
              NodeList name = element.getElementsByTagName("TRANSID");
              Element line = (Element) name.item(0);
              System.out.println("transid: " + getCharacterDataFromElement(line));
              transid = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList title = element.getElementsByTagName("PARTNERTID");
              Element line = (Element) title.item(0);
              System.out.println("partnertid: " + getCharacterDataFromElement(line));
              partnertid = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList rescodex = element.getElementsByTagName("RESCODE");
              Element line = (Element) rescodex.item(0);
              System.out.println("rescode: " + getCharacterDataFromElement(line));
              rescode = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList restypex = element.getElementsByTagName("RESTYPE");
              Element line = (Element) restypex.item(0);
              System.out.println("restype: " + getCharacterDataFromElement(line));
              restype = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList idpelx = element.getElementsByTagName("IDPEL");
              Element line = (Element) idpelx.item(0);
              System.out.println("idpel: " + getCharacterDataFromElement(line));
              idpel = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("NAMA");
              Element line = (Element) namax.item(0);
              System.out.println("nama: " + getCharacterDataFromElement(line));
              nama = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SNTOKEN");
              Element line = (Element) namax.item(0);
              System.out.println("sntoken: " + getCharacterDataFromElement(line));
              sntoken = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("PLNREF");
              Element line = (Element) namax.item(0);
              System.out.println("plnref: " + getCharacterDataFromElement(line));
              plnref = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("REFNBR");
              Element line = (Element) namax.item(0);
              System.out.println("plnref: " + getCharacterDataFromElement(line));
              refnbr = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList kwhx = element.getElementsByTagName("KWH");
              Element line = (Element) kwhx.item(0);
              System.out.println("kwh: " + getCharacterDataFromElement(line));
              kwh = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("TAGIHAN");
              Element line = (Element) namax.item(0);
              System.out.println("tagihan: " + getCharacterDataFromElement(line));
              tagihan = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("ADMIN");
              Element line = (Element) namax.item(0);
              System.out.println("admin: " + getCharacterDataFromElement(line));
              admin = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("METERAI");
              Element line = (Element) namax.item(0);
              System.out.println("meterai: " + getCharacterDataFromElement(line));
              meterai = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("PPN");
              Element line = (Element) namax.item(0);
              System.out.println("ppn: " + getCharacterDataFromElement(line));
              ppn = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("PPJ");
              Element line = (Element) namax.item(0);
              System.out.println("ppj: " + getCharacterDataFromElement(line));
              ppj = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("ANGSURAN");
              Element line = (Element) namax.item(0);
              System.out.println("angsuran: " + getCharacterDataFromElement(line));
              angsuran = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("RPTOKEN");
              Element line = (Element) namax.item(0);
              System.out.println("RPTOKEN: " + getCharacterDataFromElement(line));
              rptoken = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("JMKWH");
              Element line = (Element) namax.item(0);
              System.out.println("jmkwh: " + getCharacterDataFromElement(line));
              jmkwh = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("INFO");
              Element line = (Element) namax.item(0);
              System.out.println("info: " + getCharacterDataFromElement(line));
              info = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SALDO");
              Element line = (Element) namax.item(0);
              System.out.println("saldo: " + getCharacterDataFromElement(line));
              saldo = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SNIDPEL");
              Element line = (Element) namax.item(0);
              System.out.println("snidpel: " + getCharacterDataFromElement(line));
              snidpel = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SNMETER");
              Element line = (Element) namax.item(0);
              System.out.println("snmeter: " + getCharacterDataFromElement(line));
              snmeter = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SETLEMENT");
              Element line = (Element) namax.item(0);
              System.out.println("settlement: " + getCharacterDataFromElement(line));
              setlement = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList ket = element.getElementsByTagName("DESC");
              Element line = (Element) ket.item(0);
              System.out.println("ket: " + getCharacterDataFromElement(line));
              desc = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

        }

                this.conx = setting.getConnection();
                Statement st   = conx.createStatement();
                String format = "";

                String sql = " select * from format_balasans with(nolock) "+
                             " where user_id = "+user_id+" and name = 'PLN-PRE-SUCCESS'";

                //System.out.println(sql);
                //logger.log(Level.INFO,sql);

                ResultSet rs = st.executeQuery(sql);
                if (rs.next()) {
                    format = rs.getString("format");
                }

                Calendar currentDate = Calendar.getInstance();
                SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                String dateNow = formatter.format(currentDate.getTime());

                NumberFormat formatter2 = new DecimalFormat("#,###,###");

                sql = "select price from users "
                        + " left outer join product_prices on product_prices.price_template_id = users.price_template_id "
                        + " where users.id = '"+user_id+"' and product_prices.product_id ='80'";

                double fee = 0;

                rs = st.executeQuery(sql);
                if (rs.next()) {
                    fee = rs.getInt("price");
                }
                else {
                    fee = 0;
                }

                Saldo mutasi = new Saldo();
                saldo = String.valueOf(mutasi.getBalance(user_id));

                format = format.replace("[Keterangan]",desc);
                format = format.replace("[TrxId]",trxid);
                format = format.replace("[TRANSID]",transid);
                format = format.replace("[PARTNERTID]",partnertid);
                format = format.replace("[RESTYPE]",restype);
                format = format.replace("[RESCODE]",rescode);
                format = format.replace("[IDPEL]",idpel);
                format = format.replace("[NAMA]",nama);
                format = format.replace("[SNTOKEN]",sntoken);
                format = format.replace("[PLNREF]",plnref);
                format = format.replace("[REFNBR]",refnbr);
                format = format.replace("[KWH]",kwh);
                format = format.replace("[TAGIHAN]",tagihan);
                format = format.replace("[ADMIN]",admin);
                format = format.replace("[METERAI]",meterai);
                format = format.replace("[PPN]",ppn);
                format = format.replace("[PPJ]",ppj);
                format = format.replace("[ANGSURAN]",angsuran);
                format = format.replace("[RPTOKEN]",rptoken);
                format = format.replace("[JMKWH]",jmkwh);
                format = format.replace("[INFO]",info);
                //format = format.replace("[SALDO]",saldo);
                format = format.replace("[SNIDPEL]",snidpel);
                format = format.replace("[SNMETER]",snmeter);
                format = format.replace("[SETLEMENT]",setlement);
                format = format.replace("[SETTLEMENT]",setlement);

                format = format.replace("[Harga]","");
                format = format.replace("[Saldo]","");
                format = format.replace("[SALDO]","");
                format = format.replace("[Saldo Sebelumnya]","");
                format = format.replace("[Status]","SUKSES");
                format = format.replace("[Tgl]",dateNow);

                st.close();
                rs.close();
                conx.close();

                return format;

            }
            catch(Exception e) {
                logger.log(Level.FATAL, "Error @getFormatBalasan SMS : {0}"+ e.getMessage());

            }

        return xml;
    }

    public String getFormatBalasanTelkomPay(String xml, String user_id) {

        try {

            String transid="",partnertid="",restype="",rescode="",idpel="",nama="",jumlahrek="",plnref="",refnbr="";
            String kwh="",tagihan="",admin="",meterai="",ppn="",ppj="",angsuran="",outstanding="",jmkwh="",info="",saldo="";
            String snidpel = "",snmeter="",setlement="", desc="", denda="", insentive="", bulan="", mlalu="";

            xml = xml.toUpperCase();

            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("XML");

            for (int i = 0; i < nodes.getLength(); i++) {
              Element element = (Element) nodes.item(i);

              try {
              NodeList name = element.getElementsByTagName("TRANSID");
              Element line = (Element) name.item(0);
              System.out.println("transid: " + getCharacterDataFromElement(line));
              transid = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList title = element.getElementsByTagName("PARTNERTID");
              Element line = (Element) title.item(0);
              System.out.println("partnertid: " + getCharacterDataFromElement(line));
              partnertid = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList rescodex = element.getElementsByTagName("RESCODE");
              Element line = (Element) rescodex.item(0);
              System.out.println("rescode: " + getCharacterDataFromElement(line));
              rescode = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList restypex = element.getElementsByTagName("RESTYPE");
              Element line = (Element) restypex.item(0);
              System.out.println("restype: " + getCharacterDataFromElement(line));
              restype = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList idpelx = element.getElementsByTagName("IDPEL");
              Element line = (Element) idpelx.item(0);
              System.out.println("idpel: " + getCharacterDataFromElement(line));
              idpel = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("NAMA");
              Element line = (Element) namax.item(0);
              System.out.println("nama: " + getCharacterDataFromElement(line));
              nama = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList kwhx = element.getElementsByTagName("KWH");
              Element line = (Element) kwhx.item(0);
              System.out.println("kwh: " + getCharacterDataFromElement(line));
              kwh = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("JUMLAHREK");
              Element line = (Element) namax.item(0);
              System.out.println("JUMLAH REK: " + getCharacterDataFromElement(line));
              jumlahrek = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList kwhx = element.getElementsByTagName("OUTSTANDING");
              Element line = (Element) kwhx.item(0);
              System.out.println("OUTSTANDING: " + getCharacterDataFromElement(line));
              outstanding = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("PLNREF");
              Element line = (Element) namax.item(0);
              System.out.println("plnref: " + getCharacterDataFromElement(line));
              plnref = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("REFNBR");
              Element line = (Element) namax.item(0);
              System.out.println("plnref: " + getCharacterDataFromElement(line));
              refnbr = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList kwhx = element.getElementsByTagName("DENDA");
              Element line = (Element) kwhx.item(0);
              System.out.println("denda: " + getCharacterDataFromElement(line));
              denda = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("ADMIN");
              Element line = (Element) namax.item(0);
              System.out.println("admin: " + getCharacterDataFromElement(line));
              admin = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("INSENTIF");
              Element line = (Element) namax.item(0);
              System.out.println("insentif: " + getCharacterDataFromElement(line));
              insentive = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("TAGIHAN");
              Element line = (Element) namax.item(0);
              System.out.println("tagihan: " + getCharacterDataFromElement(line));
              tagihan = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("BULAN");
              Element line = (Element) namax.item(0);
              System.out.println("BULAN: " + getCharacterDataFromElement(line));
              bulan = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("MLALU");
              Element line = (Element) namax.item(0);
              System.out.println("Mlalu: " + getCharacterDataFromElement(line));
              mlalu = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SALDO");
              Element line = (Element) namax.item(0);
              System.out.println("saldo: " + getCharacterDataFromElement(line));
              saldo = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}


              try {
              NodeList namax = element.getElementsByTagName("INFO");
              Element line = (Element) namax.item(0);
              System.out.println("info: " + getCharacterDataFromElement(line));
              info = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}


              try {
              NodeList ket = element.getElementsByTagName("DESC");
              Element line = (Element) ket.item(0);
              System.out.println("ket: " + getCharacterDataFromElement(line));
              desc = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

        }

                this.conx = setting.getConnection();
                Statement st   = conx.createStatement();
                String format = "";

                String sql = " select * from format_balasans with(nolock) "+
                             " where user_id = '"+user_id+"' and name = 'PLN-POST-SUCCESS'";

                //System.out.println(sql);
                //logger.log(Level.INFO,sql);

                ResultSet rs = st.executeQuery(sql);
                if (rs.next()) {
                    format = rs.getString("format");
                }

                Calendar currentDate = Calendar.getInstance();
                SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                String dateNow = formatter.format(currentDate.getTime());

                NumberFormat formatter2 = new DecimalFormat("#,###,###");

                format = format.replace("[Keterangan]",desc);
                format = format.replace("[TrxId]",transid);
                format = format.replace("[TRANSID]",transid);
                format = format.replace("[PARTNERTID]",partnertid);
                format = format.replace("[RESTYPE]",restype);
                format = format.replace("[RESCODE]",rescode);
                format = format.replace("[IDPEL]",idpel);
                format = format.replace("[NAMA]",nama);
                format = format.replace("[INSENTIF]",insentive);
                format = format.replace("[PLNREF]",plnref);
                format = format.replace("[REFNBR]",refnbr);
                format = format.replace("[KWH]",kwh);
                format = format.replace("[TAGIHAN]",tagihan);
                format = format.replace("[ADMIN]",admin);
                format = format.replace("[METERAI]",meterai);
                format = format.replace("[PPN]",ppn);
                format = format.replace("[PPJ]",ppj);
                format = format.replace("[ANGSURAN]",angsuran);
                format = format.replace("[MLALU]",mlalu);
                format = format.replace("[BULAN]",bulan);
                format = format.replace("[JMKWH]",jmkwh);
                format = format.replace("[INFO]",info);
                format = format.replace("[SALDO]",saldo);
                format = format.replace("[SNIDPEL]",snidpel);
                format = format.replace("[SNMETER]",snmeter);
                format = format.replace("[SETLEMENT]",setlement);
                format = format.replace("[SETTLEMENT]",setlement);
                format = format.replace("[DENDA]",denda);

                st.close();
                rs.close();
                conx.close();

                return format;

            }
            catch(Exception e) {
                logger.log(Level.FATAL, e);
            }

        return xml;
    }

    public String getFormatBalasanPLNPay(String xml, String user_id) {

        try {

            String transid="",partnertid="",restype="",rescode="",idpel="",nama="",jumlahrek="",plnref="",refnbr="";
            String kwh="",tagihan="",admin="",meterai="",ppn="",ppj="",angsuran="",outstanding="",jmkwh="",info="",saldo="";
            String snidpel = "",snmeter="",setlement="", desc="", denda="", insentive="", bulan="", mlalu="";

            xml = xml.toUpperCase();

            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("XML");

            for (int i = 0; i < nodes.getLength(); i++) {
              Element element = (Element) nodes.item(i);

              try {
              NodeList name = element.getElementsByTagName("TRANSID");
              Element line = (Element) name.item(0);
              System.out.println("transid: " + getCharacterDataFromElement(line));
              transid = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList title = element.getElementsByTagName("PARTNERTID");
              Element line = (Element) title.item(0);
              System.out.println("partnertid: " + getCharacterDataFromElement(line));
              partnertid = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList rescodex = element.getElementsByTagName("RESCODE");
              Element line = (Element) rescodex.item(0);
              System.out.println("rescode: " + getCharacterDataFromElement(line));
              rescode = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList restypex = element.getElementsByTagName("RESTYPE");
              Element line = (Element) restypex.item(0);
              System.out.println("restype: " + getCharacterDataFromElement(line));
              restype = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList idpelx = element.getElementsByTagName("IDPEL");
              Element line = (Element) idpelx.item(0);
              System.out.println("idpel: " + getCharacterDataFromElement(line));
              idpel = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("NAMA");
              Element line = (Element) namax.item(0);
              System.out.println("nama: " + getCharacterDataFromElement(line));
              nama = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList kwhx = element.getElementsByTagName("KWH");
              Element line = (Element) kwhx.item(0);
              System.out.println("kwh: " + getCharacterDataFromElement(line));
              kwh = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("JUMLAHREK");
              Element line = (Element) namax.item(0);
              System.out.println("JUMLAH REK: " + getCharacterDataFromElement(line));
              jumlahrek = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList kwhx = element.getElementsByTagName("OUTSTANDING");
              Element line = (Element) kwhx.item(0);
              System.out.println("OUTSTANDING: " + getCharacterDataFromElement(line));
              outstanding = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}



              try {
              NodeList namax = element.getElementsByTagName("PLNREF");
              Element line = (Element) namax.item(0);
              System.out.println("plnref: " + getCharacterDataFromElement(line));
              plnref = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("REFNBR");
              Element line = (Element) namax.item(0);
              System.out.println("plnref: " + getCharacterDataFromElement(line));
              refnbr = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList kwhx = element.getElementsByTagName("DENDA");
              Element line = (Element) kwhx.item(0);
              System.out.println("denda: " + getCharacterDataFromElement(line));
              denda = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("ADMIN");
              Element line = (Element) namax.item(0);
              System.out.println("admin: " + getCharacterDataFromElement(line));
              admin = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("INSENTIF");
              Element line = (Element) namax.item(0);
              System.out.println("insentif: " + getCharacterDataFromElement(line));
              insentive = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("TAGIHAN");
              Element line = (Element) namax.item(0);
              System.out.println("tagihan: " + getCharacterDataFromElement(line));
              tagihan = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("BULAN");
              Element line = (Element) namax.item(0);
              System.out.println("BULAN: " + getCharacterDataFromElement(line));
              bulan = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("MLALU");
              Element line = (Element) namax.item(0);
              System.out.println("Mlalu: " + getCharacterDataFromElement(line));
              mlalu = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("SALDO");
              Element line = (Element) namax.item(0);
              System.out.println("saldo: " + getCharacterDataFromElement(line));
              saldo = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}


              try {
              NodeList namax = element.getElementsByTagName("INFO");
              Element line = (Element) namax.item(0);
              System.out.println("info: " + getCharacterDataFromElement(line));
              info = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}


              try {
              NodeList ket = element.getElementsByTagName("DESC");
              Element line = (Element) ket.item(0);
              System.out.println("ket: " + getCharacterDataFromElement(line));
              desc = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

        }

                this.conx = setting.getConnection();
                Statement st   = conx.createStatement();
                String format = "";

                String sql = " select * from format_balasans with(nolock) "+
                             " where user_id = "+user_id+" and name = 'PLN-POST-SUCCESS'";

                //System.out.println(sql);
                //logger.log(Level.INFO,sql);

                ResultSet rs = st.executeQuery(sql);
                if (rs.next()) {
                    format = rs.getString("format");
                }

                Calendar currentDate = Calendar.getInstance();
                SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                String dateNow = formatter.format(currentDate.getTime());

                NumberFormat formatter2 = new DecimalFormat("#,###,###");

                format = format.replace("[Keterangan]",desc);
                format = format.replace("[TrxId]",transid);
                format = format.replace("[TRANSID]",transid);
                format = format.replace("[PARTNERTID]",partnertid);
                format = format.replace("[RESTYPE]",restype);
                format = format.replace("[RESCODE]",rescode);
                format = format.replace("[IDPEL]",idpel);
                format = format.replace("[NAMA]",nama);
                format = format.replace("[INSENTIF]",insentive);
                format = format.replace("[PLNREF]",plnref);
                format = format.replace("[REFNBR]",refnbr);
                format = format.replace("[KWH]",kwh);
                format = format.replace("[TAGIHAN]",tagihan);
                format = format.replace("[ADMIN]",admin);
                format = format.replace("[METERAI]",meterai);
                format = format.replace("[PPN]",ppn);
                format = format.replace("[PPJ]",ppj);
                format = format.replace("[ANGSURAN]",angsuran);
                format = format.replace("[MLALU]",mlalu);
                format = format.replace("[BULAN]",bulan);
                format = format.replace("[JMKWH]",jmkwh);
                format = format.replace("[INFO]",info);
                format = format.replace("[SALDO]",saldo);
                format = format.replace("[SNIDPEL]",snidpel);
                format = format.replace("[SNMETER]",snmeter);
                format = format.replace("[SETLEMENT]",setlement);
                format = format.replace("[SETTLEMENT]",setlement);
                format = format.replace("[DENDA]",denda);


                st.close();
                rs.close();
                conx.close();

                return format;

            }
            catch(Exception e) {
                logger.log(Level.FATAL, e.getMessage());
            }

        return xml;
    }

    public String getFormatBalasanPLN(String xml, String user_id) {

        try {

            String transid="",partnertid="",restype="",rescode="",idpel="",nama="",sntoken="",plnref="",refnbr="";
            String kwh="",tagihan="",admin="",meterai="",ppn="",ppj="",angsuran="",rptoken="",jmkwh="",info="",saldo="";
            String snidpel = "",snmeter="",setlement="", desc="";

            xml = xml.toUpperCase();

            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("XML");

            for (int i = 0; i < nodes.getLength(); i++) {
              Element element = (Element) nodes.item(i);

              try {
              NodeList name = element.getElementsByTagName("TRANSID");
              Element line = (Element) name.item(0);
              System.out.println("transid: " + getCharacterDataFromElement(line));
              transid = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList title = element.getElementsByTagName("PARTNERTID");
              Element line = (Element) title.item(0);
              System.out.println("partnertid: " + getCharacterDataFromElement(line));
              partnertid = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList rescodex = element.getElementsByTagName("RESCODE");
              Element line = (Element) rescodex.item(0);
              System.out.println("rescode: " + getCharacterDataFromElement(line));
              rescode = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList restypex = element.getElementsByTagName("RESTYPE");
              Element line = (Element) restypex.item(0);
              System.out.println("restype: " + getCharacterDataFromElement(line));
              restype = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList idpelx = element.getElementsByTagName("IDPEL");
              Element line = (Element) idpelx.item(0);
              System.out.println("idpel: " + getCharacterDataFromElement(line));
              idpel = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("NAMA");
              Element line = (Element) namax.item(0);
              System.out.println("nama: " + getCharacterDataFromElement(line));
              nama = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList kwhx = element.getElementsByTagName("KWH");
              Element line = (Element) kwhx.item(0);
              System.out.println("kwh: " + getCharacterDataFromElement(line));
              kwh = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList ket = element.getElementsByTagName("DESC");
              Element line = (Element) ket.item(0);
              System.out.println("ket: " + getCharacterDataFromElement(line));
              desc = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

        }

                this.conx = setting.getConnection();
                Statement st   = conx.createStatement();
                String format = "";

                String sql = " select * from format_balasans with(nolock) "+
                             " where user_id = "+user_id+" and name = 'PLN-PRE-INQUIRY'";

                //System.out.println(sql);
                //logger.log(Level.INFO,sql);

                ResultSet rs = st.executeQuery(sql);
                if (rs.next()) {
                    format = rs.getString("format");
                }

                Calendar currentDate = Calendar.getInstance();
                SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                String dateNow = formatter.format(currentDate.getTime());

                NumberFormat formatter2 = new DecimalFormat("#,###,###");

                format = format.replace("[Keterangan]",desc);
                format = format.replace("[TrxId]",transid);
                format = format.replace("[TRANSID]",transid);
                format = format.replace("[PARTNERTID]",partnertid);
                format = format.replace("[RESTYPE]",restype);
                format = format.replace("[RESCODE]",rescode);
                format = format.replace("[IDPEL]",idpel);
                format = format.replace("[NAMA]",nama);
                format = format.replace("[SNTOKEN]",sntoken);
                format = format.replace("[PLNREF]",plnref);
                format = format.replace("[REFNBR]",refnbr);
                format = format.replace("[KWH]",kwh);
                format = format.replace("[TAGIHAN]",tagihan);
                format = format.replace("[ADMIN]",admin);
                format = format.replace("[METERAI]",meterai);
                format = format.replace("[PPN]",ppn);
                format = format.replace("[PPJ]",ppj);
                format = format.replace("[ANGSURAN]",angsuran);
                format = format.replace("[RPTOKEN]",rptoken);
                format = format.replace("[JMKWH]",jmkwh);
                format = format.replace("[INFO]",info);
                format = format.replace("[SALDO]",saldo);
                format = format.replace("[SNIDPEL]",snidpel);
                format = format.replace("[SNMETER]",snmeter);
                format = format.replace("[SETLEMENT]",setlement);

                st.close();
                rs.close();
                conx.close();

                return format;

            }
            catch(Exception e) {
                logger.log(Level.FATAL, "Error @getFormatBalasan SMS : "+ e.getMessage());

            }

        return xml;
    }

    public String getFormatBalasanPLN_XMPP(String xml, String user_id) {

        try {

            String transid="",partnertid="",restype="",rescode="",idpel="",nama="",sntoken="",plnref="",refnbr="";
            String kwh="",tagihan="",admin="",meterai="",ppn="",ppj="",angsuran="",rptoken="",jmkwh="",info="",saldo="";
            String snidpel = "",snmeter="",setlement="", desc="";

            xml = xml.toUpperCase();

            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("XML");

            for (int i = 0; i < nodes.getLength(); i++) {
              Element element = (Element) nodes.item(i);

              try {
              NodeList name = element.getElementsByTagName("TRANSID");
              Element line = (Element) name.item(0);
              System.out.println("transid: " + getCharacterDataFromElement(line));
              transid = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList title = element.getElementsByTagName("PARTNERTID");
              Element line = (Element) title.item(0);
              System.out.println("partnertid: " + getCharacterDataFromElement(line));
              partnertid = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList rescodex = element.getElementsByTagName("RESCODE");
              Element line = (Element) rescodex.item(0);
              System.out.println("rescode: " + getCharacterDataFromElement(line));
              rescode = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList restypex = element.getElementsByTagName("RESTYPE");
              Element line = (Element) restypex.item(0);
              System.out.println("restype: " + getCharacterDataFromElement(line));
              restype = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList idpelx = element.getElementsByTagName("IDPEL");
              Element line = (Element) idpelx.item(0);
              System.out.println("idpel: " + getCharacterDataFromElement(line));
              idpel = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList namax = element.getElementsByTagName("NAMA");
              Element line = (Element) namax.item(0);
              System.out.println("nama: " + getCharacterDataFromElement(line));
              nama = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList kwhx = element.getElementsByTagName("KWH");
              Element line = (Element) kwhx.item(0);
              System.out.println("kwh: " + getCharacterDataFromElement(line));
              kwh = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

              try {
              NodeList ket = element.getElementsByTagName("DESC");
              Element line = (Element) ket.item(0);
              System.out.println("ket: " + getCharacterDataFromElement(line));
              desc = getCharacterDataFromElement(line);
              }
              catch(Exception e) {}

             }

                String format = "";
                
                Calendar currentDate = Calendar.getInstance();
                SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                String dateNow = formatter.format(currentDate.getTime());

                format  = "INQUIRY LISTRIK PRABAYAR\n\n";
                format += "TANGGAL\t\t:"+dateNow+"\n";                
                format += "NO METER\t:"+idpel+"\n";
                format += "NAMA\t\t:"+nama+"\n";
                format += "TARIF/DAYA\t:"+kwh+"\n";                
                format += "TERIMA KASIH\n";
                format += ""+info;

                return format;

            }
            catch(Exception e) {
                logger.log(Level.FATAL, "Error : "+ e);

            }

        return xml;
    }

    public String getFormatBalasanPLNPost(String xml, String user_id) {

        try {

            String transid="",partnertid="",restype="",rescode="",idpel="",nama="",sntoken="",plnref="",refnbr="";
            String kwh="",tagihan="",admin="",meterai="",ppn="",ppj="",angsuran="",rptoken="",jmkwh="",info="",saldo="";
            String snidpel = "",snmeter="",setlement="", desc="";

            xml = xml.toUpperCase();

            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("XML");

            for (int i = 0; i < nodes.getLength(); i++) {
              Element element = (Element) nodes.item(i);

              NodeList name = element.getElementsByTagName("TRANSID");
              Element line = (Element) name.item(0);
              System.out.println("transid: " + getCharacterDataFromElement(line));
              transid = getCharacterDataFromElement(line);

              NodeList title = element.getElementsByTagName("PARTNERTID");
              line = (Element) title.item(0);
              System.out.println("partnertid: " + getCharacterDataFromElement(line));
              partnertid = getCharacterDataFromElement(line);

              NodeList rescodex = element.getElementsByTagName("RESCODE");
              line = (Element) rescodex.item(0);
              System.out.println("rescode: " + getCharacterDataFromElement(line));
              rescode = getCharacterDataFromElement(line);

              NodeList restypex = element.getElementsByTagName("RESTYPE");
              line = (Element) restypex.item(0);
              System.out.println("restype: " + getCharacterDataFromElement(line));
              restype = getCharacterDataFromElement(line);


              NodeList ket = element.getElementsByTagName("DESC");
              line = (Element) ket.item(0);
              System.out.println("ket: " + getCharacterDataFromElement(line));
              desc = getCharacterDataFromElement(line);

        }

                this.conx = setting.getConnection();
                Statement st   = conx.createStatement();
                String format = "";

                String sql = " select * from format_balasans with(nolock) "+
                             " where user_id = "+user_id+" and name = 'PLN-POST-INQUIRY'";

                //System.out.println(sql);
                //logger.log(Level.INFO,sql);

                ResultSet rs = st.executeQuery(sql);
                if (rs.next()) {
                    format = rs.getString("format");
                }

                Calendar currentDate = Calendar.getInstance();
                SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                String dateNow = formatter.format(currentDate.getTime());

                NumberFormat formatter2 = new DecimalFormat("#,###,###");

                format = format.replace("[Keterangan]",desc);
                format = format.replace("[TrxId]",transid);
                format = format.replace("[TRANSID]",transid);
                format = format.replace("[PARTNERTID]",partnertid);
                format = format.replace("[RESTYPE]",restype);
                format = format.replace("[RESCODE]",rescode);
                format = format.replace("[IDPEL]",idpel);
                format = format.replace("[NAMA]",nama);
                format = format.replace("[SNTOKEN]",sntoken);
                format = format.replace("[PLNREF]",plnref);
                format = format.replace("[REFNBR]",refnbr);
                format = format.replace("[KWH]",kwh);
                format = format.replace("[TAGIHAN]",tagihan);
                format = format.replace("[ADMIN]",admin);
                format = format.replace("[METERAI]",meterai);
                format = format.replace("[PPN]",ppn);
                format = format.replace("[PPJ]",ppj);
                format = format.replace("[ANGSURAN]",angsuran);
                format = format.replace("[RPTOKEN]",rptoken);
                format = format.replace("[JMKWH]",jmkwh);
                format = format.replace("[INFO]",info);
                format = format.replace("[SALDO]",saldo);
                format = format.replace("[SNIDPEL]",snidpel);
                format = format.replace("[SNMETER]",snmeter);
                format = format.replace("[SETLEMENT]",setlement);

                st.close();
                rs.close();
                conx.close();

                return format;

            }
            catch(Exception e) {
                logger.log(Level.FATAL, "Error @getFormatBalasan SMS : "+ e.getMessage());

            }

        return xml;
    }

    public String getFormatBalasanSMS2(String idVoc, String pin
            , String password, String sn, String kode_voucher, String management_code
            , String trx, String status, String hp, String saldo
            , String prevSaldo, String harga, String nominal, String vCode, String user_id, String format_balasan_id) {


        try {

                this.conx = setting.getConnection();
                Statement st   = conx.createStatement();
                String format = "";

                String sql = " select * from format_balasans with(nolock) "+                             
                             " where format_balasans.id = "+format_balasan_id+"";

                format = "Bukti TagList: Ref #REFNBR# #NAMA# #KWH# Rp."+
                         "#TAGIHAN# ";

                //System.out.println(sql);
                //logger.log(Level.INFO,sql);

                ResultSet rs = st.executeQuery(sql);
                if (rs.next()) {
                    format = rs.getString("format_sms");
                }

                Calendar currentDate = Calendar.getInstance();
                SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                String dateNow = formatter.format(currentDate.getTime());

                NumberFormat formatter2 = new DecimalFormat("#,###,###");

                if (harga.equalsIgnoreCase("")) harga = "0";
                if (saldo.equalsIgnoreCase("")) saldo = "0";

                format = format.replace("[SN]",sn);
                format = format.replace("[Pass]",password);
                format = format.replace("[PIN]",pin);
                format = format.replace("[Status]",status);
                format = format.replace("[Harga]",formatter2.format(Double.parseDouble(harga)));
                format = format.replace("[Saldo]",formatter2.format(Double.parseDouble(saldo)-Double.parseDouble(harga)));
                format = format.replace("[Saldo Sebelumnya]",formatter2.format(Double.parseDouble(saldo)));//formatter2.format(Integer.parseInt(saldo)+Integer.parseInt(harga)));
                format = format.replace("[Kode Produk]",vCode);
                format = format.replace("[Nama Produk]",prod.getProductDescription(vCode+nominal));
                format = format.replace("[HP]",hp);
                format = format.replace("[Tujuan]",hp);
                format = format.replace("[SN]",sn);
                format = format.replace("[Tgl]",dateNow);
                format = format.replace("[Code]",kode_voucher);
                format = format.replace("[Nominal]",nominal);
                format = format.replace("[TrxId]",trx);

                st.close();
                rs.close();
                conx.close();

                return format;

            }
            catch(Exception e) {
                logger.log(Level.FATAL, "Error @getFormatBalasan SMS : "+ e.getMessage());

            }

        return "System Error";
    }

    public String getFormatBalasanSMS(String idVoc, String pin
            , String password, String sn, String kode_voucher, String management_code
            , String trx, String status, String hp, String saldo
            , String prevSaldo, String harga, String nominal, String vCode, String user_id) {


        try {

                this.conx = setting.getConnection();
                Statement st   = conx.createStatement();
                String format = "";

                String sql = " select * from format_balasans with(nolock) "+
                             " left outer join response_stats on response_stats.id = format_balasans.response_stat_id "+
                             " where user_id = "+user_id+" and response_stats.name = '"+status+"'";

                if (status.equalsIgnoreCase("SUCCESS")) {
                    sql = " select * from format_balasans with(nolock) "+
                          " left outer join response_stats on response_stats.id = format_balasans.response_stat_id "+
                          " where user_id = "+user_id+" and response_stats.name = 'SUCCESS'"+
                          " and format_balasans.id="+String.valueOf(prodCat.getFormatBalasanId( String.valueOf(prodCat.getProductCategoryId(vCode)),user_id));
                }
                else {
                    sql = " select * from format_balasans with(nolock) "+
                          " left outer join response_stats on response_stats.id = format_balasans.response_stat_id "+
                          " where user_id = "+user_id+" and response_stats.name = '"+status+"'";
                }

                //System.out.println(sql);
                //logger.log(Level.INFO,sql);

                ResultSet rs = st.executeQuery(sql);
                if (rs.next()) {
                    format = rs.getString("format_sms");
                }

                Calendar currentDate = Calendar.getInstance();
                SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                String dateNow = formatter.format(currentDate.getTime());

                NumberFormat formatter2 = new DecimalFormat("#,###,###");

                if (harga.equalsIgnoreCase("")) harga = "0";
                if (saldo.equalsIgnoreCase("")) saldo = "0";

                format = format.replace("[SN]",sn);
                format = format.replace("[Pass]",password);
                format = format.replace("[PIN]",pin);
                format = format.replace("[Status]",status);
                format = format.replace("[Harga]",formatter2.format(Integer.parseInt(harga)));
                format = format.replace("[Saldo]",formatter2.format(Integer.parseInt(saldo)-Integer.parseInt(harga)));
                format = format.replace("[Saldo Sebelumnya]",formatter2.format(Integer.parseInt(saldo)));//formatter2.format(Integer.parseInt(saldo)+Integer.parseInt(harga)));
                format = format.replace("[Kode Produk]",vCode);
                format = format.replace("[HP]",hp);
                format = format.replace("[Tujuan]",hp);
                format = format.replace("[TrxId]",trx);
                format = format.replace("[SN]",sn);
                format = format.replace("[Tgl]",dateNow);
                format = format.replace("[Code]",kode_voucher);
                format = format.replace("[Nominal]",nominal);

                st.close();
                rs.close();
                conx.close();

                return format;

            }
            catch(Exception e) {
                logger.log(Level.FATAL, "Error @getFormatBalasan SMS : "+ e.getMessage());
                
            }

        return "System Error";
    }

    public String getFormatBalasan(String idVoc, String pin
            , String password, String sn, String kode_voucher, String management_code
            , String trx, String status, String hp, String saldo
            , String prevSaldo, String harga, String nominal, String vCode, String user_id) {

        try {

                this.conx = setting.getConnection();
                Statement st   = conx.createStatement();
                String format = "";
                String sql = "";

                if (status.equalsIgnoreCase("SUCCESS")) {
                    sql = " select * from format_balasans with(nolock) "+
                          " left outer join response_stats on response_stats.id = format_balasans.response_stat_id "+
                          " where user_id = "+user_id+" and response_stats.name = '"+status+"'"+
                          " and format_balasans.id="+String.valueOf(prodCat.getFormatBalasanId( String.valueOf(prodCat.getProductCategoryId(vCode)),user_id));
                }
                else {
                    sql = " select * from format_balasans with(nolock) "+
                          " left outer join response_stats on response_stats.id = format_balasans.response_stat_id "+
                          " where user_id = "+user_id+" and response_stats.name = '"+status+"'";
                }

                //logger.log(Level.INFO,sql);

                //System.out.println(sql);
                ResultSet rs = st.executeQuery(sql);
                if (rs.next()) {
                    format = rs.getString("format");
                    
                }

                Calendar currentDate = Calendar.getInstance();
                SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                String dateNow = formatter.format(currentDate.getTime());

                NumberFormat formatter2 = new DecimalFormat("#,###,###");

                if (harga.equalsIgnoreCase("")) harga = "0";
                if (saldo.equalsIgnoreCase("")) saldo = "0";

                format = format.replace("[SN]",sn);
                format = format.replace("[Pass]",password);
                format = format.replace("[PIN]",pin);
                format = format.replace("[Status]",status);
                format = format.replace("[Harga]",formatter2.format(Integer.parseInt(harga)));
                format = format.replace("[Saldo]",formatter2.format(Integer.parseInt(saldo)-Integer.parseInt(harga)));
                format = format.replace("[Saldo Sebelumnya]",formatter2.format(Integer.parseInt(saldo)));//formatter2.format(Integer.parseInt(saldo)+Integer.parseInt(harga)));
                format = format.replace("[Kode Produk]",vCode);
                format = format.replace("[HP]",hp);
                format = format.replace("[Tujuan]",hp);
                format = format.replace("[TrxId]",trx);
                format = format.replace("[SN]",sn);
                format = format.replace("[Tgl]",dateNow);
                format = format.replace("[Code]",kode_voucher);
                format = format.replace("[Nominal]",nominal);

                rs.close();
                st.close();
                conx.close();

                return format;

            }
            catch(Exception e) {
                logger.log(Level.FATAL, "Error @getFormatBalasan  : "+ e.getMessage());
            }

        return "System Error";

    }

   
    public String getFormatBalasanGame(String idVoc, String pin
            , String password, String sn, String kode_voucher, String management_code
            , String trx, String status, String hp, String saldo
            , String prevSaldo, String harga, String nominal, String vCode, String user_id) {

        try {

                this.conx = setting.getConnection();
                Statement st   = conx.createStatement();
                String format = "";
                String sql = "";

                if (status.equalsIgnoreCase("200")) {
                    sql = " select * from format_balasans with(nolock) "+
                          " left outer join response_stats on response_stats.id = format_balasans.response_stat_id "+
                          " where user_id = "+user_id+" and response_stats.code = '"+status+"'"+
                          " and format_balasans.id="+String.valueOf(prodCat.getFormatBalasanId( String.valueOf(prodCat.getProductCategoryId(vCode+nominal)),user_id));
                }
                else {
                    sql = " select * from format_balasans with(nolock) "+
                          " left outer join response_stats on response_stats.id = format_balasans.response_stat_id "+
                          " where user_id = "+user_id+" and response_stats.code = '"+status+"'";
                }

                //logger.log(Level.INFO,sql);

                //System.out.println(sql);
                ResultSet rs = st.executeQuery(sql);
                if (rs.next()) {
                    format = rs.getString("format");
                }

                Calendar currentDate = Calendar.getInstance();
                SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                String dateNow = formatter.format(currentDate.getTime());

                NumberFormat formatter2 = new DecimalFormat("#,###,###");

                if (harga.equalsIgnoreCase("")) harga = "0";
                if (saldo.equalsIgnoreCase("")) saldo = "0";

                System.out.println("harga : "+harga);
                System.out.println("saldo : "+saldo);

                format = format.replace("[SN]",sn);
                format = format.replace("[Pass]",password);
                format = format.replace("[PIN]",pin);                
                format = format.replace("[Harga]",formatter2.format(Double.parseDouble(harga)));
                format = format.replace("[Saldo]",formatter2.format(Double.parseDouble(saldo)-Double.parseDouble(harga)));
                format = format.replace("[Saldo Sebelumnya]",formatter2.format(Double.parseDouble(saldo)));//formatter2.format(Integer.parseInt(saldo)+Integer.parseInt(harga)));
                format = format.replace("[Kode Produk]",vCode+nominal);
                format = format.replace("[HP]",hp);
                format = format.replace("[Tujuan]",hp);
                format = format.replace("[TrxId]",trx);
                format = format.replace("[SN]",sn);
                format = format.replace("[Tgl]",dateNow);
                format = format.replace("[Code]",kode_voucher);
                format = format.replace("[Nominal]",nominal);
                format = format.replace("[Status]",getStatusName(status));

                st.close();
                rs.close();
                conx.close();

                return format;

            }
            catch(Exception e) {
                logger.log(Level.FATAL, "Error @getFormatBalasan  : "+ e.getMessage());
            }

        return "909";

    }


}
