
package utils;

import ppob_biller.Settings;
import java.sql.*;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author ngonar
 */
public class Users {

    Connection conx = null;
    Connection conOtomax = null;
    Settings setting = new Settings();

    private static final Logger logger = Logger.getLogger(Users.class);

    public Users() {
        //this.conx = setting.getConnection();
        //this.conOtomax = setting.getConnectionOtomax();
    }

    public int getPriceTemplateId(String user_id) {

        try {
            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            String sql = "select price_template_id from users where id='"+user_id+"'";
            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            int price_template_id = 0;

            if (rs.next())
                price_template_id = rs.getInt("price_template_id");
            else
                price_template_id = 0;

            st.close();
            rs.close();
            conx.close();

            return price_template_id;

        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return 0;
    }

    

    public int getIdFromMedia(String media, String media_type) {

        try {
            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            String sql = "select user_id from user_medias where media_type_id = '"+media_type+"' and name='"+media+"'";
            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            int user_id = 0;

            if (rs.next())
                user_id = rs.getInt("user_id");
            else
                user_id = 0;

            st.close();
            rs.close();
            conx.close();

            return user_id;

        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return 0;
    }

    public boolean isRegisteredGtalk(String sender) {

        String pengirim[] = sender.split("@");
        sender = pengirim[0];

        try {
            this.conx = setting.getConnection();
            Statement st = conx.createStatement();
            ResultSet rs = st.executeQuery("select * from user_medias where media_type_id = 4");

            if (rs.next())
                return true;

            st.close();
            rs.close();
            conx.close();
        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage());
        }

        return false;
    }

    public boolean isHth(int id) {
        return false;
    }

    public boolean isRetail(int id) {
        return false;
    }

    public boolean pinIsValid(String pin, String id) {

        String cekPin = "";

        try {
            this.conx = setting.getConnection();
            Statement st   = conx.createStatement();
            String sql = "select pin from users with(nolock) where id = '"+id+"'";
            ResultSet rs = st.executeQuery(sql);

            //logger.log(Level.INFO,sql);

            boolean pinVal = false;

            if (rs.next())
                cekPin = rs.getString("pin");

            //cek
            if (cekPin.equalsIgnoreCase(pin)) {
                pinVal = true;
                logger.log(Level.INFO,"PIN valid : "+cekPin+" - "+pin);
            }
            else {
                pinVal = false;
                logger.log(Level.INFO,"PIN invalid : "+cekPin+" - "+pin);
            }

            st.close();
            rs.close();
            conx.close();

            return pinVal;

        }
        catch(Exception e) {
            logger.log(Level.FATAL,e.getMessage() );
        }

        return false;
    }

}
