/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ngonar
 */
@Entity
@Table(name = "product_prices")
@NamedQueries({
    @NamedQuery(name = "ProductPrices.findAll", query = "SELECT p FROM ProductPrices p"),
    @NamedQuery(name = "ProductPrices.findById", query = "SELECT p FROM ProductPrices p WHERE p.id = :id"),
    @NamedQuery(name = "ProductPrices.findByPriceTemplateId", query = "SELECT p FROM ProductPrices p WHERE p.priceTemplateId = :priceTemplateId"),
    @NamedQuery(name = "ProductPrices.findByProductId", query = "SELECT p FROM ProductPrices p WHERE p.productId = :productId"),
    @NamedQuery(name = "ProductPrices.findByPrice", query = "SELECT p FROM ProductPrices p WHERE p.price = :price"),
    @NamedQuery(name = "ProductPrices.findByCreateDate", query = "SELECT p FROM ProductPrices p WHERE p.createDate = :createDate"),
    @NamedQuery(name = "ProductPrices.findByCreateBy", query = "SELECT p FROM ProductPrices p WHERE p.createBy = :createBy"),
    @NamedQuery(name = "ProductPrices.findByUpdateDate", query = "SELECT p FROM ProductPrices p WHERE p.updateDate = :updateDate"),
    @NamedQuery(name = "ProductPrices.findByUpdateBy", query = "SELECT p FROM ProductPrices p WHERE p.updateBy = :updateBy")})
public class ProductPrices implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "price_template_id")
    private Integer priceTemplateId;
    @Column(name = "product_id")
    private Integer productId;
    @Column(name = "price")
    private Long price;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "create_by")
    private String createBy;
    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "update_by")
    private String updateBy;

    public ProductPrices() {
    }

    public ProductPrices(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPriceTemplateId() {
        return priceTemplateId;
    }

    public void setPriceTemplateId(Integer priceTemplateId) {
        this.priceTemplateId = priceTemplateId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductPrices)) {
            return false;
        }
        ProductPrices other = (ProductPrices) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.ProductPrices[id=" + id + "]";
    }

}
