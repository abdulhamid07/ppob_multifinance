/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author root
 */
@Entity
@Table(name = "partner_codes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PartnerCodes.findById", query = "SELECT p FROM PartnerCodes p WHERE p.id = :id"),
    @NamedQuery(name = "PartnerCodes.findByName", query = "SELECT p FROM PartnerCodes p WHERE p.name = :name"),
    @NamedQuery(name = "PartnerCodes.findByInitial", query = "SELECT p FROM PartnerCodes p WHERE p.initial = :initial"),
    @NamedQuery(name = "PartnerCodes.findByDescription", query = "SELECT p FROM PartnerCodes p WHERE p.description = :description"),
    @NamedQuery(name = "PartnerCodes.findByCid", query = "SELECT p FROM PartnerCodes p WHERE p.cid = :cid")
})

@SequenceGenerator(sequenceName = "partner_codes_id_seq", name = "partnercode_gen", allocationSize = 1)
public class PartnerCodes implements Serializable {

    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "partnercode_gen")
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Size(max = 50)
    @Column(name = "name")
    private String name;

    @Size(max = 5)
    @Column(name = "initial")
    private String initial;

    @Size(max = 7)
    @Column(name = "cid")
    private String cid;

    @Size(max = 255)
    @Column(name = "description")
    private String description;


    public PartnerCodes() {
    }

    public PartnerCodes(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PartnerCodes)) {
            return false;
        }
        PartnerCodes other = (PartnerCodes) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.PartnerCode[ id=" + getId() + " ]";
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the initial
     */
    public String getInitial() {
        return initial;
    }

    /**
     * @param initial the initial to set
     */
    public void setInitial(String initial) {
        this.initial = initial;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
