
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ngonar
 */
@Entity
@Table(name = "pln_prepaids")
@NamedQueries({
    @NamedQuery(name = "PlnPrepaids.findAll", query = "SELECT p FROM PlnPrepaids p"),
    @NamedQuery(name = "PlnPrepaids.findById", query = "SELECT p FROM PlnPrepaids p WHERE p.id = :id"),
    @NamedQuery(name = "PlnPrepaids.findByInboxId", query = "SELECT p FROM PlnPrepaids p WHERE p.inboxId = :inboxId"),
    @NamedQuery(name = "PlnPrepaids.findByTransid", query = "SELECT p FROM PlnPrepaids p WHERE p.transid = :transid"),
    @NamedQuery(name = "PlnPrepaids.findByPartnertid", query = "SELECT p FROM PlnPrepaids p WHERE p.partnertid = :partnertid"),
    @NamedQuery(name = "PlnPrepaids.findByRestype", query = "SELECT p FROM PlnPrepaids p WHERE p.restype = :restype"),
    @NamedQuery(name = "PlnPrepaids.findByRescode", query = "SELECT p FROM PlnPrepaids p WHERE p.rescode = :rescode"),
    @NamedQuery(name = "PlnPrepaids.findByIdpel", query = "SELECT p FROM PlnPrepaids p WHERE p.idpel = :idpel"),
    @NamedQuery(name = "PlnPrepaids.findByNama", query = "SELECT p FROM PlnPrepaids p WHERE p.nama = :nama"),
    @NamedQuery(name = "PlnPrepaids.findBySntoken", query = "SELECT p FROM PlnPrepaids p WHERE p.sntoken = :sntoken"),
    @NamedQuery(name = "PlnPrepaids.findByPlnref", query = "SELECT p FROM PlnPrepaids p WHERE p.plnref = :plnref"),
    @NamedQuery(name = "PlnPrepaids.findByRefnbr", query = "SELECT p FROM PlnPrepaids p WHERE p.refnbr = :refnbr"),
    @NamedQuery(name = "PlnPrepaids.findByKwh", query = "SELECT p FROM PlnPrepaids p WHERE p.kwh = :kwh"),
    @NamedQuery(name = "PlnPrepaids.findByTagihan", query = "SELECT p FROM PlnPrepaids p WHERE p.tagihan = :tagihan"),
    @NamedQuery(name = "PlnPrepaids.findByAdmin", query = "SELECT p FROM PlnPrepaids p WHERE p.admin = :admin"),
    @NamedQuery(name = "PlnPrepaids.findByMeterai", query = "SELECT p FROM PlnPrepaids p WHERE p.meterai = :meterai"),
    @NamedQuery(name = "PlnPrepaids.findByPpn", query = "SELECT p FROM PlnPrepaids p WHERE p.ppn = :ppn"),
    @NamedQuery(name = "PlnPrepaids.findByPpj", query = "SELECT p FROM PlnPrepaids p WHERE p.ppj = :ppj"),
    @NamedQuery(name = "PlnPrepaids.findByAngsuran", query = "SELECT p FROM PlnPrepaids p WHERE p.angsuran = :angsuran"),
    @NamedQuery(name = "PlnPrepaids.findByRptoken", query = "SELECT p FROM PlnPrepaids p WHERE p.rptoken = :rptoken"),
    @NamedQuery(name = "PlnPrepaids.findByJmkwh", query = "SELECT p FROM PlnPrepaids p WHERE p.jmkwh = :jmkwh"),
    @NamedQuery(name = "PlnPrepaids.findByInfo", query = "SELECT p FROM PlnPrepaids p WHERE p.info = :info"),
    @NamedQuery(name = "PlnPrepaids.findBySaldo", query = "SELECT p FROM PlnPrepaids p WHERE p.saldo = :saldo"),
    @NamedQuery(name = "PlnPrepaids.findBySnidpel", query = "SELECT p FROM PlnPrepaids p WHERE p.snidpel = :snidpel"),
    @NamedQuery(name = "PlnPrepaids.findBySnmeter", query = "SELECT p FROM PlnPrepaids p WHERE p.snmeter = :snmeter"),
    @NamedQuery(name = "PlnPrepaids.findBySettlement", query = "SELECT p FROM PlnPrepaids p WHERE p.settlement = :settlement"),
    @NamedQuery(name = "PlnPrepaids.findByXml", query = "SELECT p FROM PlnPrepaids p WHERE p.xml = :xml"),
    @NamedQuery(name = "PlnPrepaids.findByCreateDate", query = "SELECT p FROM PlnPrepaids p WHERE p.createDate = :createDate"),
    @NamedQuery(name = "PlnPrepaids.findByCreateBy", query = "SELECT p FROM PlnPrepaids p WHERE p.createBy = :createBy"),
    @NamedQuery(name = "PlnPrepaids.findByDestination", query = "SELECT p FROM PlnPrepaids p WHERE p.destination = :destination"),
    @NamedQuery(name = "PlnPrepaids.findByUser", query = "SELECT p FROM PlnPrepaids p WHERE p.user = :user"),
    @NamedQuery(name = "PlnPrepaids.findByPass", query = "SELECT p FROM PlnPrepaids p WHERE p.pass = :pass"),
    @NamedQuery(name = "PlnPrepaids.findByIso", query = "SELECT p FROM PlnPrepaids p WHERE p.iso = :iso")})

@SequenceGenerator(sequenceName="pln_prepaids_id_seq",name="plnprepaid_gen", allocationSize=1)
public class PlnPrepaids implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "plnprepaid_gen")
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "inbox_id")
    private Integer inboxId;
    @Column(name = "transid")
    private String transid;
    @Column(name = "partnertid")
    private String partnertid;
    @Column(name = "restype")
    private String restype;
    @Column(name = "rescode")
    private String rescode;
    @Column(name = "idpel")
    private String idpel;
    @Column(name = "nama")
    private String nama;
    @Column(name = "sntoken")
    private String sntoken;
    @Column(name = "plnref")
    private String plnref;
    @Column(name = "refnbr")
    private String refnbr;
    @Column(name = "kwh")
    private String kwh;
    @Column(name = "tagihan")
    private String tagihan;
    @Column(name = "admin")
    private String admin;
    @Column(name = "meterai")
    private String meterai;
    @Column(name = "ppn")
    private String ppn;
    @Column(name = "ppj")
    private String ppj;
    @Column(name = "angsuran")
    private String angsuran;
    @Column(name = "rptoken")
    private String rptoken;
    @Column(name = "jmkwh")
    private String jmkwh;
    @Column(name = "info")
    private String info;
    @Column(name = "saldo")
    private String saldo;
    @Column(name = "snidpel")
    private String snidpel;
    @Column(name = "snmeter")
    private String snmeter;
    @Column(name = "settlement")
    private String settlement;
    @Column(name = "xml")
    private String xml;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "create_by")
    private String createBy;
    @Column(name = "destination")
    private String destination;
    @Column(name = "user_")
    private String user;
    @Column(name = "pass_")
    private String pass;
    @Column(name = "iso")
    private String iso;
    @Column(name = "telpon")
    private String telpon;

    @Column(name = "merchant_cat_code")
    private String merchant_cat_code;
    @Column(name = "bank_code")
    private String bank_code;
    @Column(name = "terminal_id")
    private String terminal_id;
    @Column(name = "dt_trx")
    private String dt_trx;
    @Column(name = "reprint")
    private int reprint;

    public int getReprint() {
        return reprint;
    }

    public void setReprint(int inboxId) {
        this.reprint = inboxId;
    }

    public String getDtTrx() {
        return dt_trx;
    }

    public void setDtTrx(String inboxId) {
        this.dt_trx = inboxId;
    }

    public String getTerminalId() {
        return terminal_id;
    }

    public void setTerminalId(String inboxId) {
        this.terminal_id = inboxId;
    }

    public String getBankCode() {
        return bank_code;
    }

    public void setBankCode(String inboxId) {
        this.bank_code = inboxId;
    }

    public String getMerchantCatCode() {
        return merchant_cat_code;
    }

    public void setMerchantCatCode(String inboxId) {
        this.merchant_cat_code = inboxId;
    }

    public PlnPrepaids() {
    }

    public PlnPrepaids(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getInboxId() {
        return inboxId;
    }

    public void setInboxId(Integer inboxId) {
        this.inboxId = inboxId;
    }

    public String getTransid() {
        return transid;
    }

    public void setTransid(String transid) {
        this.transid = transid;
    }

    public String getPartnertid() {
        return partnertid;
    }

    public void setPartnertid(String partnertid) {
        this.partnertid = partnertid;
    }

    public String getRestype() {
        return restype;
    }

    public void setRestype(String restype) {
        this.restype = restype;
    }

    public String getRescode() {
        return rescode;
    }

    public void setRescode(String rescode) {
        this.rescode = rescode;
    }

    public String getIdpel() {
        return idpel;
    }

    public void setIdpel(String idpel) {
        this.idpel = idpel;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getSntoken() {
        return sntoken;
    }

    public void setSntoken(String sntoken) {
        this.sntoken = sntoken;
    }

    public String getPlnref() {
        return plnref;
    }

    public void setPlnref(String plnref) {
        this.plnref = plnref;
    }

    public String getRefnbr() {
        return refnbr;
    }

    public void setRefnbr(String refnbr) {
        this.refnbr = refnbr;
    }

    public String getKwh() {
        return kwh;
    }

    public void setKwh(String kwh) {
        this.kwh = kwh;
    }

    public String getTagihan() {
        return tagihan;
    }

    public void setTagihan(String tagihan) {
        this.tagihan = tagihan;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getMeterai() {
        return meterai;
    }

    public void setMeterai(String meterai) {
        this.meterai = meterai;
    }

    public String getPpn() {
        return ppn;
    }

    public void setPpn(String ppn) {
        this.ppn = ppn;
    }

    public String getPpj() {
        return ppj;
    }

    public void setPpj(String ppj) {
        this.ppj = ppj;
    }

    public String getAngsuran() {
        return angsuran;
    }

    public void setAngsuran(String angsuran) {
        this.angsuran = angsuran;
    }

    public String getRptoken() {
        return rptoken;
    }

    public void setRptoken(String rptoken) {
        this.rptoken = rptoken;
    }

    public String getJmkwh() {
        return jmkwh;
    }

    public void setJmkwh(String jmkwh) {
        this.jmkwh = jmkwh;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getSnidpel() {
        return snidpel;
    }

    public void setSnidpel(String snidpel) {
        this.snidpel = snidpel;
    }

    public String getSnmeter() {
        return snmeter;
    }

    public void setSnmeter(String snmeter) {
        this.snmeter = snmeter;
    }

    public String getSettlement() {
        return settlement;
    }

    public void setSettlement(String settlement) {
        this.settlement = settlement;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getTelpon() {
        return telpon;
    }

    public void setTelpon(String telpon) {
        this.telpon = telpon;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlnPrepaids)) {
            return false;
        }
        PlnPrepaids other = (PlnPrepaids) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.PlnPrepaids[id=" + id + "]";
    }

}
