/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ngonar
 */
@Entity
@Table(name = "pln_postpaids")
@NamedQueries({
    @NamedQuery(name = "PlnPostpaids.findAll", query = "SELECT p FROM PlnPostpaids p"),
    @NamedQuery(name = "PlnPostpaids.findById", query = "SELECT p FROM PlnPostpaids p WHERE p.id = :id"),
    @NamedQuery(name = "PlnPostpaids.findByInboxId", query = "SELECT p FROM PlnPostpaids p WHERE p.inboxId = :inboxId"),
    @NamedQuery(name = "PlnPostpaids.findByTransid", query = "SELECT p FROM PlnPostpaids p WHERE p.transid = :transid"),
    @NamedQuery(name = "PlnPostpaids.findByPartnertid", query = "SELECT p FROM PlnPostpaids p WHERE p.partnertid = :partnertid"),
    @NamedQuery(name = "PlnPostpaids.findByRestype", query = "SELECT p FROM PlnPostpaids p WHERE p.restype = :restype"),
    @NamedQuery(name = "PlnPostpaids.findByRescode", query = "SELECT p FROM PlnPostpaids p WHERE p.rescode = :rescode"),
    @NamedQuery(name = "PlnPostpaids.findByIdpel", query = "SELECT p FROM PlnPostpaids p WHERE p.idpel = :idpel"),
    @NamedQuery(name = "PlnPostpaids.findByNama", query = "SELECT p FROM PlnPostpaids p WHERE p.nama = :nama"),
    @NamedQuery(name = "PlnPostpaids.findByKwh", query = "SELECT p FROM PlnPostpaids p WHERE p.kwh = :kwh"),
    @NamedQuery(name = "PlnPostpaids.findByJumlahrek", query = "SELECT p FROM PlnPostpaids p WHERE p.jumlahrek = :jumlahrek"),
    @NamedQuery(name = "PlnPostpaids.findByOutstanding", query = "SELECT p FROM PlnPostpaids p WHERE p.outstanding = :outstanding"),
    @NamedQuery(name = "PlnPostpaids.findByTagihan", query = "SELECT p FROM PlnPostpaids p WHERE p.tagihan = :tagihan"),
    @NamedQuery(name = "PlnPostpaids.findByDenda", query = "SELECT p FROM PlnPostpaids p WHERE p.denda = :denda"),
    @NamedQuery(name = "PlnPostpaids.findByAdmin", query = "SELECT p FROM PlnPostpaids p WHERE p.admin = :admin"),
    @NamedQuery(name = "PlnPostpaids.findByInsentive", query = "SELECT p FROM PlnPostpaids p WHERE p.insentive = :insentive"),
    @NamedQuery(name = "PlnPostpaids.findByBulan", query = "SELECT p FROM PlnPostpaids p WHERE p.bulan = :bulan"),
    @NamedQuery(name = "PlnPostpaids.findByMlalu", query = "SELECT p FROM PlnPostpaids p WHERE p.mlalu = :mlalu"),
    @NamedQuery(name = "PlnPostpaids.findByRefnbr", query = "SELECT p FROM PlnPostpaids p WHERE p.refnbr = :refnbr"),
    @NamedQuery(name = "PlnPostpaids.findByInfo", query = "SELECT p FROM PlnPostpaids p WHERE p.info = :info"),
    @NamedQuery(name = "PlnPostpaids.findBySaldo", query = "SELECT p FROM PlnPostpaids p WHERE p.saldo = :saldo"),
    @NamedQuery(name = "PlnPostpaids.findByCreateDate", query = "SELECT p FROM PlnPostpaids p WHERE p.createDate = :createDate"),
    @NamedQuery(name = "PlnPostpaids.findByDestination", query = "SELECT p FROM PlnPostpaids p WHERE p.destination = :destination"),
    @NamedQuery(name = "PlnPostpaids.findByUser", query = "SELECT p FROM PlnPostpaids p WHERE p.user = :user"),
    @NamedQuery(name = "PlnPostpaids.findByPass", query = "SELECT p FROM PlnPostpaids p WHERE p.pass = :pass"),
    @NamedQuery(name = "PlnPostpaids.findByXml", query = "SELECT p FROM PlnPostpaids p WHERE p.xml = :xml"),
    @NamedQuery(name = "PlnPostpaids.findByIso", query = "SELECT p FROM PlnPostpaids p WHERE p.iso = :iso")})

@SequenceGenerator(sequenceName="pln_postpaids_id_seq",name="plnpostpaid_gen", allocationSize=1)
public class PlnPostpaids implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "plnpostpaid_gen")
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "inbox_id")
    private Integer inboxId;
    @Column(name = "transid")
    private String transid;
    @Column(name = "partnertid")
    private String partnertid;
    @Column(name = "restype")
    private String restype;
    @Column(name = "rescode")
    private String rescode;
    @Column(name = "idpel")
    private String idpel;
    @Column(name = "nama")
    private String nama;
    @Column(name = "kwh")
    private String kwh;
    @Column(name = "jumlahrek")
    private String jumlahrek;
    @Column(name = "outstanding")
    private String outstanding;
    @Column(name = "tagihan")
    private String tagihan;
    @Column(name = "denda")
    private String denda;
    @Column(name = "admin")
    private String admin;
    @Column(name = "insentive")
    private String insentive;
    @Column(name = "bulan")
    private String bulan;
    @Column(name = "mlalu")
    private String mlalu;
    @Column(name = "refnbr")
    private String refnbr;
    @Column(name = "info")
    private String info;
    @Column(name = "telpon")
    private String telpon;
    @Column(name = "saldo")
    private String saldo;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "destination")
    private String destination;
    @Column(name = "user_")
    private String user;
    @Column(name = "pass_")
    private String pass;
    @Column(name = "xml")
    private String xml;
    @Column(name = "iso")
    private String iso;
    @Column(name = "ca_id")
    private String ca_id;
    @Column(name = "merchant")
    private String merchant;
    @Column(name = "bill_period")
    private String bill_period;
    @Column(name = "total_electricity_bill")
    private String total_electricity_bill;
    @Column(name = "ppn")
    private String ppn;
    @Column(name = "penalty")
    private String penalty;
    @Column(name = "terminal_id")
    private String terminal_id;
    @Column(name = "dt_trx")
    private String dt_trx;
    @Column(name = "reprint")
    private int reprint;
    @Column(name = "plnref")
    private String plnref;

    public String getPlnref() {
        return plnref;
    }

    public void setPlnref(String transid) {
        this.plnref = transid;
    }

    public String getCaid() {
        return ca_id;
    }

    public void setCaid(String transid) {
        this.ca_id = transid;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String transid) {
        this.merchant = transid;
    }

    public String getBillPeriod() {
        return bill_period;
    }

    public void setBillPeriod(String transid) {
        this.bill_period = transid;
    }

    public String getTotalElectricityBill() {
        return total_electricity_bill;
    }

    public void setTotalElectricityBill(String transid) {
        this.total_electricity_bill = transid;
    }

    public String getPpn() {
        return ppn;
    }

    public void setPpn(String transid) {
        this.ppn = transid;
    }

    public String getPenalty() {
        return penalty;
    }

    public void setPenalty(String transid) {
        this.penalty = transid;
    }

    public String getTerminalId() {
        return terminal_id;
    }

    public void setTerminalId(String transid) {
        this.terminal_id = transid;
    }

    public String getDtTrx() {
        return dt_trx;
    }

    public void setDtTrx(String transid) {
        this.dt_trx = transid;
    }

    public int getReprint() {
        return reprint;
    }

    public void setReprint(int transid) {
        this.reprint = transid;
    }

    public PlnPostpaids() {
    }

    public PlnPostpaids(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getInboxId() {
        return inboxId;
    }

    public void setInboxId(Integer inboxId) {
        this.inboxId = inboxId;
    }

    public String getTransid() {
        return transid;
    }

    public void setTransid(String transid) {
        this.transid = transid;
    }

    public String getPartnertid() {
        return partnertid;
    }

    public void setPartnertid(String partnertid) {
        this.partnertid = partnertid;
    }

    public String getRestype() {
        return restype;
    }

    public void setRestype(String restype) {
        this.restype = restype;
    }

    public String getRescode() {
        return rescode;
    }

    public void setRescode(String rescode) {
        this.rescode = rescode;
    }

    public String getIdpel() {
        return idpel;
    }

    public void setIdpel(String idpel) {
        this.idpel = idpel;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKwh() {
        return kwh;
    }

    public void setKwh(String kwh) {
        this.kwh = kwh;
    }

    public String getJumlahrek() {
        return jumlahrek;
    }

    public void setJumlahrek(String jumlahrek) {
        this.jumlahrek = jumlahrek;
    }

    public String getOutstanding() {
        return outstanding;
    }

    public void setOutstanding(String outstanding) {
        this.outstanding = outstanding;
    }

    public String getTagihan() {
        return tagihan;
    }

    public void setTagihan(String tagihan) {
        this.tagihan = tagihan;
    }

    public String getDenda() {
        return denda;
    }

    public void setDenda(String denda) {
        this.denda = denda;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getInsentive() {
        return insentive;
    }

    public void setInsentive(String insentive) {
        this.insentive = insentive;
    }

    public String getBulan() {
        return bulan;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public String getMlalu() {
        return mlalu;
    }

    public void setMlalu(String mlalu) {
        this.mlalu = mlalu;
    }

    public String getRefnbr() {
        return refnbr;
    }

    public void setRefnbr(String refnbr) {
        this.refnbr = refnbr;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getTelpon() {
        return telpon;
    }

    public void setTelpon(String telpon) {
        this.telpon = telpon;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlnPostpaids)) {
            return false;
        }
        PlnPostpaids other = (PlnPostpaids) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.PlnPostpaids[id=" + id + "]";
    }

}
