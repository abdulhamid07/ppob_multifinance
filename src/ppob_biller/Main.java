package ppob_biller;

import BILLER.SW.BILLER;
import com.rabbitmq.client.*;
import java.util.concurrent.*;
//import javax.swing.JFrame;
import org.apache.log4j.*;

public class Main {

//    private static final String EXCHANGE_NAME = "BPJSKES_JPA_exchange";
    private static final String EXCHANGE_NAME = "";
    private static Logger logger = Logger.getLogger("PPOB_MULTIFINANCE");

    Settings setting = new Settings();

    ExecutorService executor = null;
    //Executors.newFixedThreadPool(setting.getNthread());    
    //ExecutorService executor = Executors.newCachedThreadPool();
    //ExecutorService executor = Executors.newSingleThreadExecutor();

    public static void main(String[] args) {
        
        try {
            
            Main main = new Main();
            main.consumer();
        }
        catch(Exception e) {
            logger.log(Level.FATAL, e.getMessage());
        }

    }

    public void consumer() {
        setting.setConnections();
        
        executor = Executors.newFixedThreadPool(setting.getNthread()); 
        Connection connection = null;
        Channel channel = null;
        try {
            String EXCHANGE_NAME = setting.getRabbit_exchange();
            String queueName = setting.getRabbit_queueName();
            String bindingKey = setting.getRabbit_bindingKey();
            String rabbitHost = setting.getRabbitHost();
            
            System.out.println(EXCHANGE_NAME);
            System.out.println(queueName);
            System.out.println(bindingKey);
            System.out.println(rabbitHost);
            
            
            
          ConnectionFactory factory = new ConnectionFactory();
          factory.setHost(setting.getRabbitHost());

          connection = factory.newConnection();
          channel = connection.createChannel();

          channel.exchangeDeclare(EXCHANGE_NAME, "topic",true);
//          String queueName = "bpjskes_jpa_queue";

//          String bindingKey = "BPJSKES.JPA";
          channel.queueDeclare(queueName, true, false, false, null);
          channel.queueBind(queueName, EXCHANGE_NAME, bindingKey);

          logger.log(Level.INFO, "Waiting for messages");

          QueueingConsumer consumer = new QueueingConsumer(channel);
          channel.basicConsume(queueName, true, consumer);

          while (true) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            String message = new String(delivery.getBody());
            String routingKey = delivery.getEnvelope().getRoutingKey();

            ////////////////
            //executor = Executors.newFixedThreadPool(setting.getNthread()); 
            //executor = Executors.newSingleThreadExecutor();

            //message = "763955;token2000.pln99600.543101937419.1234;cekidot_trx@pelangi.co.id/OtomaX.;6;pelangi/NONXML;40;XMPP";
            try {
                if (message.trim().length()>1 && !message.isEmpty()) {
                    Runnable worker = new BILLER(message);
                    executor.execute(worker);
                    //executor.shutdown();
                    ////////////////
                }
            }
            catch(Exception e) {
                logger.log(Level.FATAL,e);
            }
            
            logger.log(Level.INFO, " [x] Received '" + routingKey + "':'" + message + "'");
          }
          
        }
        catch  (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e);
        }
        finally {
          if (connection != null) {
            try {
              connection.close();
            }
            catch (Exception ignore) {ignore.printStackTrace();}
          }
          
          if (channel != null) {
            try {
              channel.close();
            }
            catch (Exception ignore) {ignore.printStackTrace();}
          }
          
          if (!executor.isShutdown()) {
            executor.shutdown();
          }
          
        }        
    }

}
