package ppob_biller;

import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import org.apache.log4j.WriterAppender;
import org.apache.log4j.spi.LoggingEvent;
/**
 * Simple example of creating a Log4j appender that will
 * write to a JTextArea. 
 */
public class TextAreaAppender extends WriterAppender {
	
	static private JTextArea jTextArea = null;
	
	static public void setTextArea(JTextArea jTextArea) {
		TextAreaAppender.jTextArea = jTextArea;
	}

	@Override	
	public void append(LoggingEvent loggingEvent) {
		final String message = this.layout.format(loggingEvent);

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				jTextArea.append(message);
			}
		});
	}
         
}
