
package BILLER.SW;

/**
 *
 * @author ngonar
 */
public class PrepaidResponseCode {

    public PrepaidResponseCode() {

    }

    public String getPurchaseMsgResponseName(String code) {
        if (code.equalsIgnoreCase("0000")) {
            return "SUCCESSFUL";
        }
        else if(code.equalsIgnoreCase("0005")) {
            return "ERROR-OTHER";
        }
        else if(code.equalsIgnoreCase("0006")) {
            return "BLOCKED CID";
        }
        else if(code.equalsIgnoreCase("0009")) {
            return "ERROR-Unknown subscriber";
            //return "ERROR-Transaksi ganda, silahkan tunggu 2 menit untuk transaksi kembali";
        }
        else if(code.equalsIgnoreCase("0011")) {
            return "ERROR-Need to sign on";
        }
        else if(code.equalsIgnoreCase("0012")) {
            return "ERROR-Reversal melebihi batas waktu";
        }
        else if(code.equalsIgnoreCase("0013")) {
            return "ERROR-Invalid Transaction Amount";
        }
        else if(code.equalsIgnoreCase("0014")) {
            return "ERROR-Unknown subscriber";
        }
        else if(code.equalsIgnoreCase("0015")) {
            return "ERROR-Unknown registration number";
        }        
        else if(code.equalsIgnoreCase("0030")) {
            return "ERROR-Invalid message";
        }
        else if(code.equalsIgnoreCase("0031")) {
            return "ERROR-Unregistered Bank Code";
        }
        else if(code.equalsIgnoreCase("0032")) {
            return "ERROR-Unregistered Switching";
        }
        else if(code.equalsIgnoreCase("0033")) {
            return "ERROR-Unregistered Product";
        }
        else if(code.equalsIgnoreCase("0034")) {
            return "ERROR-Unregistered Terminal";
        }
        else if(code.equalsIgnoreCase("0041")) {
            return "ERROR-Transaction Amount below minimum purchase amount";
        }
        else if(code.equalsIgnoreCase("0042")) {
            return "ERROR-Transaction Amount exceed maximum purchase amount";
        }
        else if(code.equalsIgnoreCase("0045")) {
            return "ERROR-Invalid admin charge";
        }
        else if(code.equalsIgnoreCase("0046")) {
            return "ERROR-Insufficient Deposit";
        }
        else if(code.equalsIgnoreCase("0047")) {
            return "ERROR-Total KWH is over the limit";
        }
        else if(code.equalsIgnoreCase("0063")) {
            return "ERROR-No Payment";
        }
        else if(code.equalsIgnoreCase("0068")) {
            return "ERROR-Timeout";
        }
        else if(code.equalsIgnoreCase("0077")) {
            return "ERROR-Subscriber suspended";
        }
        else if(code.equalsIgnoreCase("0088")) {
            return "ERROR-Bills already paid";
        }
        else if(code.equalsIgnoreCase("0089")) {
            return "ERROR-Current Bill is not available";
        }
        else if(code.equalsIgnoreCase("0090")) {
            return "ERROR-Cut-off is in progress";
        }
        else if(code.equalsIgnoreCase("0092")) {
            return "ERROR-Switcher Receipt";
        }
        else if(code.equalsIgnoreCase("0093")) {
            return "ERROR-Invalid Switcher Reference Number";
        }
        else if(code.equalsIgnoreCase("0095")) {
            return "ERROR-Unregistered Merchant";
        }
        else if(code.equalsIgnoreCase("0097")) {
            return "ERROR-Switching ID and / or Bank Code is not identical with inquiry";
        }
        else if(code.equalsIgnoreCase("0098")) {
            return "ERROR-PLN Ref Number is not valid";
        }
        

        return "";
    }

    public String getInquiryMsgResponseName(String code) {
        if (code.equalsIgnoreCase("0000")) {
            return "SUCCESSFUL";
        }
        else if(code.equalsIgnoreCase("0005")) {
            return "ERROR-OTHER";
        }
        else if(code.equalsIgnoreCase("0006")) {
            return "BLOCKED CID";
        }
        else if(code.equalsIgnoreCase("0011")) {
            return "ERROR-Need to sign on";
        }
        else if(code.equalsIgnoreCase("0014")) {
            return "ERROR-Unknown subscriber";
        }
        else if(code.equalsIgnoreCase("0015")) {
            return "ERROR-Unknown registration number";
        }
        else if(code.equalsIgnoreCase("0030")) {
            return "ERROR-Invalid message";
        }
        else if(code.equalsIgnoreCase("0031")) {
            return "ERROR-Unregistered Bank Code";
        }
        else if(code.equalsIgnoreCase("0032")) {
            return "ERROR-Unregistered Switching";
        }
        else if(code.equalsIgnoreCase("0034")) {
            return "ERROR-Unregistered Terminal";
        }
        else if(code.equalsIgnoreCase("0033")) {
            return "ERROR-Unregistered Product";
        }
        else if(code.equalsIgnoreCase("0047")) {
            return "ERROR-Total KWH is over the limit";
        }
        else if(code.equalsIgnoreCase("0068")) {
            return "ERROR-Timeout";
        }
        else if(code.equalsIgnoreCase("0077")) {
            return "ERROR-Subscriber suspended";
        }
        else if(code.equalsIgnoreCase("0090")) {
            return "ERROR-Cut-off is in progress";
        }

        return "";
    }

    public String getNetworkMsgResponseName(String code) {
        if (code.equalsIgnoreCase("0000")) {
            return "SUCCESSFUL";
        }
        else if(code.equalsIgnoreCase("0005")) {
            return "ERROR-OTHER";
        }
        else if(code.equalsIgnoreCase("0006")) {
            return "BLOCKED CID";
        }
        else if(code.equalsIgnoreCase("0011")) {
            return "ERROR-NEED TO SIGN ON";
        }
        else if(code.equalsIgnoreCase("0015")) {
            return "ERROR-Unknown registration number";
        }
        else if(code.equalsIgnoreCase("0030")) {
            return "ERROR-INVALID MESSAGE";
        }
        else if(code.equalsIgnoreCase("0032")) {
            return "ERROR-UNREGISTERED SWITCHING";
        }
        else if(code.equalsIgnoreCase("0047")) {
            return "ERROR-Total KWH is over the limit";
        }
        else if(code.equalsIgnoreCase("0068")) {
            return "ERROR-TIMEOUT";
        }
        else if(code.equalsIgnoreCase("0090")) {
            return "ERROR-CUT-OFF IS IN PROGRESS";
        }

        return "";
    }

}
