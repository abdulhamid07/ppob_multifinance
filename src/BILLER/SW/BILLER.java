package BILLER.SW;

import ppob_biller.Settings;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.persistence.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
//import pelangi_utils.*;
import org.apache.log4j.*;
import model.Inboxes;
import model.Outboxes;
import model.TokenUnsolds;
import model.Users;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import model.*;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.packager.GenericPackager;
import utils.Saldo;

public class BILLER implements Runnable {

    private static final Logger logger = Logger.getLogger("BILLER");
    Saldo mutasi = new Saldo();
    Connection conx = null;
    Connection conOtomax = null;
    Settings setting = new Settings();
    double pln_price = 0;
    double initial_nominal = 0;
    long curr_saldo = 0;
    int inbox_id = 0;
    int inbox_id_pelangi = 0;
    String serializeQueue = "";
    String[] arrayQueue = new String[9];

//    public BILLER(int id) {
    public BILLER(String serializeQueue) {
        this.serializeQueue = serializeQueue;
        //arrayQueue = serializeQueue.split(";");

        ////////////////////the magic///////////////////////////////
        String msg = serializeQueue;
        logger.log(Level.INFO, "Msg init : " + msg);

        String msgArray[] = new String[9];

        if (msg.split(";").length < 9) {
            String tempA[] = msg.split(";");
            msg = msg + tempA[0];
        }

        logger.log(Level.INFO, "Msg fin : " + msg);

        msgArray = msg.split(";");

        logger.log(Level.INFO, "number array : " + msgArray.length);
        for (int i = 0; i < msgArray.length; i++) {
            logger.log(Level.INFO, "msg[" + i + "] : " + msgArray[i]);
        }

        //rearrangement
        logger.log(Level.INFO, "#################################");
        //stan awal
        logger.log(Level.INFO, "arr[0] : " + msgArray[0]);
        //pattern
        logger.log(Level.INFO, "arr[1] : " + msgArray[1]);
        //user
        logger.log(Level.INFO, "arr[2] : " + msgArray[2]);
        
        logger.log(Level.INFO, "arr[3] : " + msgArray[3]);
        logger.log(Level.INFO, "arr[4] : " + msgArray[4]);
        //user id
        logger.log(Level.INFO, "arr[5] : " + msgArray[5]);
        //stan akhir
        logger.log(Level.INFO, "arr[6] : " + msgArray[6]);
//        logger.log(Level.INFO, "arr[7] : " + msgArray[7]);
//        logger.log(Level.INFO, "arr[8] : " + msgArray[8]);
//        logger.log(Level.INFO, "arr[9] : " + msgArray[9]);
     
        //get the command
        logger.log(Level.INFO, ";" + msgArray[2] + " # " + msg.indexOf(";" + msgArray[2]));
        logger.log(Level.INFO, ";" + msgArray[0] + " # " + msg.indexOf(msgArray[0] + ";"));

        String cmd = msg.substring(msg.indexOf(msgArray[0] + ";") + msgArray[0].length() + 1, msg.indexOf(";" + msgArray[2]));
        logger.log(Level.INFO, "command : " + cmd);
        msgArray[1] = cmd;
        
        arrayQueue = msgArray;

        ///////////////////////////////////////////the magic///////////////////////////////
        this.inbox_id = Integer.parseInt(arrayQueue[0]);
        String stanAkhir = arrayQueue[6].replaceAll("WEB", "");
        
        this.inbox_id_pelangi = Integer.parseInt(stanAkhir);

        setting.setConnections();
    }

//    public String rebuildISO(int inbox_id, String[] dettail, int kodeProdukDiPelangi) {
    public String rebuildISO(int inbox_id) {

        String iso = "";
        String xml = "";
        String stan = "", rc = "", dt = "", terminal = "", produk = "", bit48 = "", bit62 = "", bank_code = "", merchant_code = "", pan = "", bit4 = "0";

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        try {

            //checking balance
            String sql = "select m from Outboxes m where m.inboxId=:uid";
            Query qq = em.createQuery(sql);
            qq.setParameter("uid", inbox_id);
            qq.setMaxResults(1);

            for (Outboxes m : (List<Outboxes>) qq.getResultList()) {
                xml = m.getMessage();
            }

            logger.log(Level.INFO, "REBUILD ISO FOR XML : " + xml);
            iso = xml;

            //parse the xml
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc;
            InputStream xmlAkhir = new ByteArrayInputStream(xml.toString().getBytes());
            doc = docBuilder.parse(xmlAkhir);
            doc.getDocumentElement().normalize();

            Map<String, String> tagMap = new HashMap<String, String>();

            try {
                NodeList nodeRc = doc.getElementsByTagName("rc");
                Element eRc = (Element) nodeRc.item(0);
                NodeList rcx = eRc.getChildNodes();
                rc = ((Node) rcx.item(0)).getNodeValue().trim();
                logger.log(Level.INFO, "RC : " + rc);

                if (rc.equalsIgnoreCase("00")) {

                    String[] tagList = {"trx_type", "product_type", "merchant_type", "bank_code", "terminal_id", "product_code", "amount", "stan", "date_time", "rc", "no_va", "jumlah_bulan", "nama", "kode_cabang", "nama_cabang", "biaya_premi", "biaya_admin", "sisa","private_data_48"};
                    for (int i = 0; i < tagList.length; i++) {
                        String tagList1 = tagList[i];
                        NodeList nodeTag = doc.getElementsByTagName(tagList[i]);
                        Element eTag = (Element) nodeTag.item(0);
                        NodeList tagx = eTag.getChildNodes();
                        tagMap.put(tagList[i], ((Node) tagx.item(0)).getNodeValue());
                        logger.log(Level.INFO, tagList[i] + " : " + tagMap.get(tagList[i]));
                    }
                    //build the ISO 
                    // membuat sebuah packager
                    ISOPackager packager = new GenericPackager("packager/iso87ascii.xml");
                    ISOMsg isoMsg = new ISOMsg();

                    Settings setting = new Settings();
                    setting.setConnections();
                    String cid = setting.getSwitchingCID();

                    bit48 = tagMap.get("private_data_48");

                    // Create ISO Message
                    isoMsg.setPackager(packager);
                    isoMsg.setMTI("2110");
                    isoMsg.set(2, "00339");
                    isoMsg.set(4, "360" + String.format("%13s", tagMap.get("amount")).replace(' ', '0'));
                    isoMsg.set(11, tagMap.get("stan"));
                    isoMsg.set(12, tagMap.get("date_time"));
                    isoMsg.set(26, tagMap.get("merchant_type"));
                    isoMsg.set(32, tagMap.get("bank_code"));
                    isoMsg.set(33, cid);
                    isoMsg.set(39, tagMap.get("rc"));
                    isoMsg.set(41, tagMap.get("terminal_id"));
                    isoMsg.set(48, bit48);
                    byte[] datax = isoMsg.pack();
                    logger.log(Level.INFO, "The rebuilt ISO : " + new String(datax));
                    iso = new String(datax);
                }

            } catch (Exception e) {
                e.printStackTrace();
                logger.log(Level.INFO, e.toString());
            }

            em.close();
            factory.close();

        } catch (Exception e) {

            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        return iso;
    }

    public String rebuildResponseByTrxId(int trx_id) {

        String iso = "";
        String xml = "";
        String stan = "", rc = "", dt = "", terminal = "", produk = "", bit48 = "", bit62 = "", bank_code = "", merchant_code = "", pan = "", bit4 = "0";

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        try {

            //cek respon berdasarkan trx id
            String sql = "select o.message from Inboxes i join Outboxes o "
                    + "where m.trxId =:trxId "
                    + "and ";
            Query qq = em.createQuery(sql);
            qq.setParameter("trxId", trx_id);

            for (Outboxes m : (List<Outboxes>) qq.getResultList()) {
                xml = m.getMessage();
            }

            logger.log(Level.INFO, "REBUILD ISO FOR XML : " + xml);
        } catch (Exception e) {

            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        return xml;
    }

    public String[] getProductDetail(String product_code) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        String[] output = new String[9];

        try {
            String sql = "SELECT o FROM Products o "
                    + " where o.code =:code";
            Query query = em.createQuery(sql);
            query.setParameter("code", Long.parseLong(product_code));
            query.setMaxResults(1);

            for (Products m : (List<Products>) query.getResultList()) {
                output[0] = String.valueOf(m.getId());
                output[1] = m.getCode();
                output[2] = m.getName();
                output[3] = m.getDescription();
                output[4] = String.valueOf(m.getNominal());
                output[5] = "0000";
                output[6] = String.valueOf(m.getActive());
                output[7] = String.valueOf(m.getKosong());
                output[8] = String.valueOf(m.getGangguan());
            }

        } catch (Exception e) {
            output[3] = "Deskripsi tidak tersedia";
            output[5] = "0005";
            logger.log(Level.FATAL, e.getMessage());
        } finally {
            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
        }

        return output;
    }

    public void run() {
        process();
    }

    public boolean itung(double nominal, int inbox_id, int user_id, String msg, int product_id, int price_template_id, int jumlah_rekening) {

        boolean boleh = false;
        RainbowCaller core = null;

        try {
            core = new RainbowCaller();

            logger.log(Level.INFO, "itung." + (int) nominal + "." + inbox_id + "." + user_id + "." + "MULTI-FINANCE" + "." + product_id + "." + price_template_id + "." + jumlah_rekening);
            String itungan = core.call("itung." + (int) nominal + "." + inbox_id + "." + user_id + "." + "MULTI-FINANCE" + "." + product_id + "." + price_template_id + "." + jumlah_rekening);
            logger.log(Level.INFO, "Hasil itungan : " + itungan);
            String rex[] = itungan.split("\\|");
            logger.log(Level.INFO, "hasil : " + rex[0]);
            if (rex[0].equalsIgnoreCase("true")) {
                boleh = true;
                pln_price = Double.parseDouble(rex[1]);
                logger.log(Level.INFO, "price : " + rex[1]);
                curr_saldo = Long.parseLong(rex[2]);
                logger.log(Level.INFO, "saldo : " + rex[2]);
            }

            core.close();

        } catch (Exception e) {
            boleh = false;
            logger.log(Level.FATAL, e.getMessage() + " < " + inbox_id + " > " + e.toString());
            try {
                core.close();
            } catch (Exception ex) {
            }

        } finally {
        }

        return boleh;
    }

    public long getSaldo(int user_id) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();
        long saldo = 0;

        try {

            //checking balance
            String sql = "select SUM(m.amount) from Mutations m where m.userId=:uid";
            Query qq = em.createQuery(sql);
            qq.setParameter("uid", user_id);

            Number saldoq = (Number) qq.getSingleResult();

            saldo = saldoq.longValue();

            em.close();
            factory.close();

        } catch (Exception e) {

            logger.log(Level.FATAL, e.getMessage() + " " + e.toString());

            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        return saldo;
    }

    public int getPriceTemplateId(int user_id) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        int price_template_id = 0;
        System.out.println("User id : " + user_id);
        try {

            Users user = em.find(Users.class, user_id);
            price_template_id = user.getPriceTemplateId();

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.toString());
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        return price_template_id;
    }

    public String[] getLoket(int user_id) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        String[] userData = new String[8];
        System.out.println("User id : " + user_id);
        try {

            Users user = em.find(Users.class, user_id);
            userData[0] = user.getUsername();
            userData[1] = user.getCompany();
            userData[2] = user.getAddress();
            userData[3] = user.getPhone();
            userData[4] = user.getFirstName();
            userData[5] = user.getKabKota().toUpperCase();
            userData[6] = user.getKodeKabKota().toUpperCase();
            userData[7] = user.getKodePos().toUpperCase();

            if (userData[1].length() > 30) {
                userData[1] = userData[1].substring(0, 30);
            }
            if (userData[2].length() > 50) {
                userData[2] = userData[2].substring(0, 50);
            }
            if (userData[3].length() > 18) {
                userData[3] = userData[3].substring(0, 18);
            }

            if (userData[5].length() > 30) {
                userData[5] = userData[5].substring(0, 10);
            }

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.toString());
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        return userData;
    }

    public String[] getLoketParameter(String dataLoket) {


        String[] dataLoketX = new String[7];
        int[] sequence = {32, 30, 50, 18, 0, 4, 15};
        String[] title = {"kode", "company", "alamat", "telp", "nama", "kab_kota", "cust phone"};
        int from = 0;
        int to = 0;
        for (int i = 0; i < sequence.length; i++) {
            int t = sequence[i];
            if (t == 0) {
                dataLoketX[i] = "";
            } else {
                to = from + t;
                dataLoketX[i] = dataLoket.substring(from, to);
                from = to;
            }
            logger.info(title[i] + " :: " + dataLoketX[i]);
        }

        return dataLoketX;
    }

    public String getLImitEdc(int user_id) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        String limit_edc = "";
        System.out.println("User id : " + user_id);
        
        try {

            Users user = em.find(Users.class, user_id);
            limit_edc = user.getLimitEdc();

            em.close();
            factory.close();

            if (limit_edc == null) {
                limit_edc = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.toString());
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        return limit_edc;
    }

    public long getProductPrice(int price_template_id, int product_id) {

        long price = 0;

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        try {
            //get Product price
            String sq = "select m.price from ProductPrices m where m.priceTemplateId=:ptid and m.productId=:pid";
            Query qqq = em.createQuery(sq);
            qqq.setParameter("pid", product_id);
            qqq.setParameter("ptid", price_template_id);
            Number priceq = (Number) qqq.getSingleResult();
            logger.log(Level.INFO, "Harga < " + inbox_id + " > :" + (priceq.longValue()) + ", fee : " + priceq.longValue());

            price = priceq.longValue();
        } catch (Exception e) {

            logger.log(Level.FATAL, e.getMessage() + " < " + inbox_id + " > " + e.toString());

            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
        } finally {
            if (em.isOpen()) {
                em.close();
            }
            if (factory.isOpen()) {
                factory.close();
            }
        }

        return price;
    }

    public synchronized void process() {

        int postpaid_pid = Integer.parseInt(setting.getKodeProduk());
        int price_template_id = 0;

        PrePaid pre = null;//new PrePaid();
        PrepaidResponseCode preRC = new PrepaidResponseCode();
        PrepaidProcessing prePro = new PrepaidProcessing();

        PostPaid post = null;//new PostPaid();
        PostpaidResponseCode postRC = new PostpaidResponseCode();
        PostPaidProcessing postPro = new PostPaidProcessing();

        NonTagihan nonTagList = null;//new NonTagihan();
        NonTagihanResponseCode nonRC = new NonTagihanResponseCode();

        String hasil = "";
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        String detail[] = new String[12];
        String product_detail[] = new String[10];
        int admin_charge_setting = 1600;

        RainbowCaller core = null;
        String id_pelanggan2 = "";
        try {

            core = new RainbowCaller();

                        //inbox message
            //inbox sender
            detail[1] = arrayQueue[2];
            //inbox media type id
            detail[2] = arrayQueue[3];
            //inbox id
            detail[3] = String.valueOf(inbox_id);
            //inbox receiver
            detail[4] = arrayQueue[4];//length array
            //inbox user id
            detail[5] = arrayQueue[5];//inbox id #208957316
            //inbox sender type
            detail[6] = arrayQueue[6]; //plg12110004/XML
            
            detail[8] = "0"; //harga

            detail[9] = "0"; //nominal admin charge
            detail[10] = ""; //curr saldo

            //detail[11] = arrayQueue[8]; //inbox id origin #WEB208957316
            //split for parameter get detail product 
            detail[0] = arrayQueue[1];
            
            String codeProd[] = new String[9];
            codeProd = detail[0].split("\\.");
            detail[7] = codeProd[2]; // product id
            detail[11] = codeProd[2];
   
            //////////////////////////////////////////////////
            //get Product Detail
            //System.out.println("=================== PRODUCT CODE ======== "+detail[7]);
            //System.exit(0);
            
            product_detail = this.getProductDetail(detail[7]);
            
            post = new PostPaid(Integer.parseInt(detail[5]));

            //minus amount with nominal
            admin_charge_setting = Integer.parseInt(product_detail[4]);
            logger.log(Level.INFO, "Admin charge setting : " + admin_charge_setting);

            detail[9] = product_detail[4];
            logger.log(Level.INFO, "detail[9] : " + detail[9]);
            /////////////////////////////////////////////////

            // diganti iwan 20140411
            price_template_id = this.getPriceTemplateId(Integer.parseInt(detail[5]));

            /////////////////UPDATE STATUS INBOX/////////////////
            setStatus(902, Integer.parseInt(detail[11]));
            /////////////////////////////////////////////////////

            ////////////////get saldo/////////////////
            logger.log(Level.INFO, "Retrieving Saldo");

            /// diganti biar cepetan dikit - iwan - 20140411
            String theBalance = String.valueOf(this.getSaldo(Integer.parseInt(detail[5])));

            double balq = Double.parseDouble(theBalance);
            curr_saldo = (long) balq;

            logger.log(Level.INFO, "Saldo : " + curr_saldo);

            ///////////////////////////////////////
            ///////////////get limit EDC///////////////
            String limit_edc = "0";

            if (detail[6].equalsIgnoreCase("edc")) {
                limit_edc = this.getLImitEdc(Integer.parseInt(detail[5]));
            }
            logger.log(Level.INFO, "Limit EDC : " + limit_edc);

            //////////////end of get limit edc////////////////////
            logger.log(Level.INFO, "message : " + detail[0]);

            //Delivery Channel
            //String merchant_code = "6012"; //teller
            logger.log(Level.INFO, "Merchant : -" + detail[6] + "-");
            String merchant_code = "6012";

            if (detail[6].equalsIgnoreCase("teller")) {
                merchant_code = "6010";
            } else if (detail[6].equalsIgnoreCase("autodebet")) {
                merchant_code = "6013";
            } else if (detail[6].equalsIgnoreCase("edc")) {
                merchant_code = "6018";
            } else if (detail[6].equalsIgnoreCase("sms")) {
                merchant_code = "6023";
            } else if (detail[6].equalsIgnoreCase("mobile")) {
                merchant_code = "6023";
            } else if (detail[6].toLowerCase().equalsIgnoreCase("xmpp")) {
                merchant_code = "6012";
            }

            logger.log(Level.INFO, "Merchant Code : -" + merchant_code + "-");

            String terminal_id = String.format("%16s", detail[5]).replace(' ', '0');

            String trxFrom = "retail";

            //validate idpel
            String dataLoket = "";

            if (detail[0].contains("||")) {
                trxFrom = "hth";
                String[] dataLoketX = detail[0].split("\\|\\|");

                detail[0] = dataLoketX[0];
                dataLoket = dataLoketX[1].substring(5);
                if (dataLoketX[1].substring(0,5).toLowerCase() == "loket") {
                    dataLoket = "";
                }
            }

            String rex[] = detail[0].split("\\.");
            String id_pelanggan = rex[2].substring(2);
            String lembarBayar = rex[2].substring(0, 2);
            id_pelanggan2 = id_pelanggan;
            
            //cek.mf.mf_ff.0000010917408836
            if (Pattern.matches("cek.mf.mf_ff.[0-9]+.[0-9]+", detail[0].toLowerCase())) {
                System.out.println("========MULTIFINANCE INQUIRY START==========");
                String[] userData = new String[8];
                userData = getLoket(Integer.parseInt(detail[5]));

                hasil = post.request_inquiry2(detail[0], detail[5], String.valueOf(inbox_id), merchant_code, terminal_id, id_pelanggan2, lembarBayar, userData);
                //System.out.println("=============== Hasil 1 ======= "+hasil);
                //System.exit(0);
                
                System.out.println("masuk");
                hasil = postPro.processInquiryBpjs(hasil, detail[3], postpaid_pid, dataLoket);
                
                System.out.println("=============== Hasil 2 ======= "+hasil);
                System.exit(0);
                
                System.out.println("========MULTIFINANCE XML : " + hasil + "==========");
                System.out.println("========MULTIFINANCE INQUIRY DONE==========");

            } else if (Pattern.matches("advice.pln[0-9]+.[a-z0-9]+.[0-9]+.[\\w\\s\\.'-;&]+", detail[0].toLowerCase())) {
            } else if (Pattern.matches("byr.bpjs.[0-9]+.[0-9]+", detail[0].toLowerCase())) { //pay BPJS
                String prehasil = rebuildISO(inbox_id);

                if (prehasil.toLowerCase().contains("xml")) { //note: artinya inquiry gagal

                    hasil = prehasil;

                    /////////////////UPDATE STATUS INBOX/////////////////
                    setStatus(901, inbox_id_pelangi);
                    /////////////////////////////////////////////////////
                } else {

                    String[] result2 = post.parseInquiryMsgResponse(prehasil, false);

                    System.out.append("result2 : " + result2[8]);

                    if (result2[8].equalsIgnoreCase("0000")) {

                        String nominal = result2[2].substring(4, result2[2].length());
                        String[] reqPost = detail[0].split("\\.");

                        int jumlah_rek = 1;

                        try {
                            //note: parsing berdasarkan respon inquiry yang diterima dari symphoni
                            String[] bit48 = post.parseBit48InquiryResponseSA_BPJS(result2[10], result2[8]);
                            admin_charge_setting = Integer.parseInt(bit48[7]);
                        } catch (Exception e) {
                            logger.log(Level.FATAL, e.toString());
                        }

                        /////////////nominal dikurangi admin///////////////
                        Long hargax = Long.parseLong(nominal);
                        nominal = hargax.toString();
                        ///////////////////////////////////////////////////
                        //kalau cukup saldonya                
//                        if (true) {
                        if (itung(Double.parseDouble(nominal), Integer.parseInt(detail[11]),
                                Integer.parseInt(detail[5]),
                                detail[0].toLowerCase(),
                                postpaid_pid,
                                price_template_id, jumlah_rek)) {
                            String[] userData = new String[8];
                                userData = getLoket(Integer.parseInt(detail[5]));

                            hasil = post.bill_payment_bpjs(detail[0], String.valueOf(inbox_id), detail[5],
                                    merchant_code, terminal_id, prehasil, admin_charge_setting,
                                    Integer.parseInt(detail[7]), userData);

                            logger.log(Level.INFO, "hasil billpayment non edc : " + hasil);
                            logger.log(Level.INFO, "curr saldo : " + curr_saldo + " - pln price : " + pln_price);
                            detail[10] = String.valueOf(curr_saldo);
                            detail[8] = String.valueOf(pln_price);

                            hasil = postPro.processPaymentBpjs(hasil, String.valueOf(inbox_id), pln_price,
                                    detail, postpaid_pid, String.valueOf(inbox_id_pelangi), userData);
                            logger.log(Level.INFO, "hasil process payment : " + hasil);

                        } else {
                            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                                    + "<response>"
                                    + "<stan>" + inbox_id_pelangi + "</stan>"
                                    + "<trx_id></trx_id>"
                                    + "<produk>BULTI-FINANCE</produk>"
                                    + "<code>0046</code>"
                                    + "<desc>Saldo tidak cukup</desc>"
                                    + "<saldo>" + curr_saldo + "</saldo>"
                                    + "</response>";

                            /////////////////UPDATE STATUS INBOX/////////////////                                    
                            setStatus(400, inbox_id_pelangi);
                            /////////////////////////////////////////////////////
                        }

                    }//gangguan
                    else {

                        hasil = postPro.processInquiry(prehasil, detail);

                        /////////////////UPDATE STATUS INBOX/////////////////
                        setStatus(901, inbox_id_pelangi);
                        /////////////////////////////////////////////////////
                    }
                }
////////////                }
            } else if (Pattern.matches("adv.bpjs.[0-9]+.[0-9]+", detail[0].toLowerCase())) {
                String prehasil = rebuildISO(inbox_id);

                if (prehasil.toLowerCase().contains("xml")) { //note: artinya inquiry gagal
                    hasil = prehasil;

                    /////////////////UPDATE STATUS INBOX/////////////////
                    setStatus(901, inbox_id_pelangi);
                    /////////////////////////////////////////////////////
                } else {

                    String[] result2 = post.parseInquiryMsgResponse(prehasil, false);

                    System.out.append("result2 : " + result2[8]);

                    if (result2[8].equalsIgnoreCase("0000")) {

                        String nominal = result2[2].substring(4, result2[2].length());
                        String[] reqPost = detail[0].split("\\.");

                        int jumlah_rek = 1;

                        try {
                            //note: parsing berdasarkan respon inquiry yang diterima dari symphoni
                            String[] bit48 = post.parseBit48InquiryResponseSA_BPJS(result2[10], result2[8]);
                            admin_charge_setting = Integer.parseInt(bit48[7]);
                        } catch (Exception e) {
                            logger.log(Level.FATAL, e.toString());
                        }

                        /////////////nominal dikurangi admin///////////////
                        Long hargax = Long.parseLong(nominal);
                        nominal = hargax.toString();
                        ///////////////////////////////////////////////////
                        //kalau cukup saldonya                

                        String[] userData = new String[8];
                        userData = getLoket(Integer.parseInt(detail[5]));

                        hasil = post.bill_advice_bpjs(detail[0], String.valueOf(inbox_id), detail[5],
                                merchant_code, terminal_id, prehasil, admin_charge_setting,
                                Integer.parseInt(detail[7]), userData);

                        logger.log(Level.INFO, "hasil bill advice non edc : " + hasil);
                        logger.log(Level.INFO, "curr saldo : " + curr_saldo + " - pln price : " + pln_price);
                        detail[10] = String.valueOf(curr_saldo);
                        detail[8] = String.valueOf(pln_price);

                        hasil = postPro.processPaymentBpjs(hasil, String.valueOf(inbox_id), pln_price,
                                detail, postpaid_pid, String.valueOf(inbox_id_pelangi), userData);
                        logger.log(Level.INFO, "hasil process advice : " + hasil);

                    }//gangguan
                    else {

                        hasil = postPro.processInquiry(prehasil, detail);

                        /////////////////UPDATE STATUS INBOX/////////////////
                        setStatus(901, inbox_id_pelangi);
                        /////////////////////////////////////////////////////
                    }
                }
////////////                }
            }//pay BPJS
            else if (Pattern.matches("byr.bpjs.[0-9]+.[0-9]+.[0-9]+", detail[0].toLowerCase())) {

                String prehasil = rebuildISO(inbox_id);

                if (prehasil.toLowerCase().contains("xml")) { //note: artinya inquiry gagal

                    hasil = prehasil;

                    /////////////////UPDATE STATUS INBOX/////////////////
                    setStatus(901, inbox_id);
                    /////////////////////////////////////////////////////
                } else {

                    String[] result2 = post.parseInquiryMsgResponse(prehasil, false);

                    System.out.append("result2 : " + result2[8]);

                    if (result2[8].equalsIgnoreCase("0000")) {

                        String nominal = result2[2].substring(4, result2[2].length());
                        String[] reqPost = detail[0].split("\\.");

                        int jumlah_rek = 1;

                        try {
                            //note: parsing berdasarkan respon inquiry yang diterima dari symphoni
                            String[] bit48 = post.parseBit48InquiryResponseSA_BPJS(result2[10], result2[8]);

                            admin_charge_setting = Integer.parseInt(bit48[7]);
                        } catch (Exception e) {
                            logger.log(Level.FATAL, e.toString());
                        }

                        /////////////nominal dikurangi admin///////////////
                        Long hargax = Long.parseLong(nominal);
                        nominal = hargax.toString();
                        ///////////////////////////////////////////////////
                        //kalau cukup saldonya                
                        if (itung(Double.parseDouble(nominal), Integer.parseInt(detail[11]),
                                Integer.parseInt(detail[5]),
                                detail[0].toLowerCase(),
                                postpaid_pid,
                                price_template_id, jumlah_rek)) {

                            String[] userData = new String[8];
                            userData = getLoket(Integer.parseInt(detail[5]));

                            hasil = post.bill_payment_bpjs(detail[0], String.valueOf(inbox_id), detail[5], merchant_code, terminal_id, prehasil, admin_charge_setting, Integer.parseInt(detail[7]), userData);

                            logger.log(Level.INFO, "hasil billpayment non edc : " + hasil);
                            logger.log(Level.INFO, "curr saldo : " + curr_saldo + " - pln price : " + pln_price);
                            detail[10] = String.valueOf(curr_saldo);
                            detail[8] = String.valueOf(pln_price);

                            hasil = postPro.processPaymentBpjs(hasil, String.valueOf(inbox_id), pln_price, detail, postpaid_pid, String.valueOf(inbox_id_pelangi), userData);
                            logger.log(Level.INFO, "hasil process payment : " + hasil);

                        } else {
                            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                                    + "<response>"
                                    + "<stan>" + inbox_id_pelangi + "</stan>"
                                    + "<trx_id></trx_id>"
                                    + "<produk>MULTI-FINANCE</produk>"
                                    + "<code>0046</code>"
                                    + "<desc>Saldo tidak cukup</desc>"
                                    + "<saldo>" + curr_saldo + "</saldo>"
                                    + "</response>";

                            /////////////////UPDATE STATUS INBOX/////////////////                                    
                            setStatus(400, inbox_id_pelangi);
                            /////////////////////////////////////////////////////
                        }

                    }//gangguan
                    else {

                        hasil = postPro.processInquiry(prehasil, detail);

                        /////////////////UPDATE STATUS INBOX/////////////////
                        setStatus(901, inbox_id_pelangi);
                        /////////////////////////////////////////////////////
                    }
                }

            } else if (Pattern.matches("inqpay.bpjs.[0-9]+.[0-9]+", detail[0].toLowerCase())) {

                logger.info("========MULTI-FINANCE INQUIRY START==========");
                String[] userData = new String[6];
                if (dataLoket.equalsIgnoreCase("")) {
                    userData = getLoket(Integer.parseInt(detail[5]));
                } else {
                    userData = getLoketParameter(dataLoket);
                }
                String prehasil = post.request_inquiry2(detail[0], detail[5], String.valueOf(inbox_id), merchant_code, terminal_id, id_pelanggan2, lembarBayar, userData);
                logger.info("parsing dulu ");
                String[] result2 = post.parseInquiryMsgResponse(prehasil, false);
                logger.info("parsing selesai ");

                logger.info("build xml inquiry ");
                String prehasilXml = postPro.processInquiryBpjs(prehasil, detail[3], postpaid_pid, dataLoket);
                logger.info("build xml inquiry selesai");

                logger.info("RC INQUIRY : " + result2[8]);
                
                /* remark by iwan 20180827 */
                /*
                    insertToOutboxOnly(prehasilXml, detail[1],
                        detail[6], "" + inbox_id_pelangi, Integer.parseInt(detail[5]),
                        detail[4], Integer.parseInt(detail[2]), result2[8], id_pelanggan);
                */
                
                logger.info("========MULTIFINANCE XML : " + prehasil + "==========");
                logger.info("========MULTIFINANCE INQUIRY DONE==========");

                if (result2[8].equalsIgnoreCase("0000")) {

                    String nominal = result2[2].substring(4, result2[2].length());
                    String[] reqPost = detail[0].split("\\.");

                    int jumlah_rek = 1;

                    try {
                        //note: parsing berdasarkan respon inquiry yang diterima dari symphoni
                        String[] bit48 = post.parseBit48InquiryResponseSA_BPJS(result2[10], result2[8]);

                        admin_charge_setting = Integer.parseInt(bit48[7]);
                    } catch (Exception e) {
                        logger.log(Level.FATAL, e.toString());
                    }

                    Long hargax = Long.parseLong(nominal);
                    nominal = hargax.toString();

                    //kalau cukup saldonya                
                    if (itung(Double.parseDouble(nominal), Integer.parseInt(detail[11]),
                            Integer.parseInt(detail[5]),
                            detail[0].toLowerCase(),
                            postpaid_pid,
                            price_template_id, jumlah_rek)) {

                        hasil = post.bill_payment_bpjs(detail[0], String.valueOf(inbox_id), detail[5], merchant_code, terminal_id, prehasil, admin_charge_setting, Integer.parseInt(detail[7]), userData);

                        logger.log(Level.INFO, "hasil billpayment non edc : " + hasil);
                        logger.log(Level.INFO, "curr saldo : " + curr_saldo + " - pln price : " + pln_price);
                        detail[10] = String.valueOf(curr_saldo);
                        detail[8] = String.valueOf(pln_price);

                        hasil = postPro.processPaymentBpjs(hasil, String.valueOf(inbox_id), pln_price, detail,
                                postpaid_pid, String.valueOf(inbox_id_pelangi), userData);
                        logger.log(Level.INFO, "hasil process payment : " + hasil);

                    } else {
                        hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                                + "<response>"
                                + "<stan>" + inbox_id_pelangi + "</stan>"
                                + "<trx_id></trx_id>"
                                + "<produk>MULTI-FINANCE</produk>"
                                + "<code>0046</code>"
                                + "<desc>Saldo tidak cukup</desc>"
                                + "<saldo>" + curr_saldo + "</saldo>"
                                + "</response>";

                        /////////////////UPDATE STATUS INBOX/////////////////                                    
                        setStatus(400, inbox_id_pelangi);
                        /////////////////////////////////////////////////////
                    }
                }//gangguan
                else {
                    hasil = postPro.processInquiry(prehasil, detail);

                    /////////////////UPDATE STATUS INBOX/////////////////                    
                    setStatus(901, inbox_id);
                    /////////////////////////////////////////////////////
                }
            }//manual advice BPJS
            else if (Pattern.matches("aaaadv.bpjs.[0-9]+.[0-9]+", detail[0].toLowerCase())) {

            } //PLN Postpaid with HP & TRX_id
            else if (Pattern.matches("pln.pay.[0-9]+.[0-9]+.[a-z0-9]+.[a-z0-9]+", detail[0].toLowerCase())) {

            } else {
                hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                        + "<response>"
                        + "<stan>" + inbox_id_pelangi + "</stan>"
                        + "<product_type>MULTI-FINANCE</product_type>"
                        + "<code>0030</code>"
                        + "<desc>Format Salah</desc>"
                        + "<saldo>" + curr_saldo + "</saldo>"
                        + "</response>";

                /////////////////UPDATE STATUS INBOX/////////////////
                setStatus(907, inbox_id_pelangi);
                /////////////////////////////////////////////////////
            }

            logger.log(Level.INFO, "TRX PLN RESULT : " + hasil);

            hasil = hasil.replaceAll("\\P{Print}", "");

            //////cek hasil akhir ///////////////
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc;
            InputStream xmlAkhir = new ByteArrayInputStream(hasil.toString().getBytes());
            doc = docBuilder.parse(xmlAkhir);
            doc.getDocumentElement().normalize();
            logger.info(doc.getDocumentElement().getNodeName());

            String rc = "";
            try {
                NodeList nodeRc = doc.getElementsByTagName("code");
                Element eRc = (Element) nodeRc.item(0);
                NodeList rcx = eRc.getChildNodes();

                rc = ((Node) rcx.item(0)).getNodeValue().trim();

                if (!rc.equalsIgnoreCase("0000")) {
                    //setInboxStatus = core.call("status.901." + detail[3]);
                    setStatus(901, inbox_id_pelangi);
                }

            } catch (Exception e) {
                rc = "0000";
                logger.log(Level.FATAL, "CODE ra ono : " + e.getMessage());
                //setInboxStatus = core.call("status.200." + detail[3]);
                setStatus(200, inbox_id_pelangi);
            }
            //////cek hasil akhir ///////////////

            if (hasil.length() > 0) {

                insertToOutbox(hasil, detail[1],
                        detail[6], "" + inbox_id_pelangi, Integer.parseInt(detail[5]),
                        detail[4], Integer.parseInt(detail[2]), rc, id_pelanggan);
            }

            core.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.toString());

            try {
                core.close();
            } catch (Exception ex) {
            }

            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"
                    + "<stan>" + String.valueOf(inbox_id_pelangi) + "</stan>"
                    + "<produk>MULTI-FINANCE</produk>"
                    + "<code>0005</code>"
                    + "<desc>SYSTEM ERROR</desc>"
                    + "<saldo>" + curr_saldo + "</saldo>"
                    + "</response>";

            insertToOutbox(hasil, detail[1],
                    detail[6], "" + inbox_id_pelangi, Integer.parseInt(detail[5]),
                    detail[4], Integer.parseInt(detail[2]), "0005", id_pelanggan2);

        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }
    }

    public String getMessageOutboxByInboxId(int inboxId) {
        String result = "";
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();
        try {

            String sql = "SELECT o FROM Outboxes o "
                    + " where o.inboxId =:inbox_id order by o.inboxId asc";
            Query query = em.createQuery(sql);
            query.setParameter("inbox_id", inboxId);
            query.setMaxResults(1);

            //if (query.getResultList().size()>0) {
            for (Outboxes m : (List<Outboxes>) query.getResultList()) {
                logger.log(Level.INFO, "id : " + m.getId());
                //resendMessageOutbox(m.getId());
                result = m.getMessage();
                logger.log(Level.INFO, result);
            }
            //}

            em.close();
            factory.close();
        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
        } finally {

            if (em.isOpen()) {
                em.close();
            }
            if (factory.isOpen()) {
                factory.close();
            }

        }

        return result;
    }

    public int getInboxIdByTrxId(String trx_id, String user_id, int inbox_id, int prod_id, String trxType) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();
        int hasil = 0;
        logger.log(Level.INFO, "cek duplikasi..." + trx_id + "#" + user_id + "#" + inbox_id);
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            //get current date time with Date()
            java.util.Date date = new java.util.Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            
            String sql = " SELECT o FROM Inboxes o "
                    + " where o.trxId =:trx_id "
                    + " and o.trxId <> '0'"
                    + " and o.id not in (:inbox_id) "
                    + " and o.userId =:user_id "
                    + " and o.prodId =:prod_id "
                    + " and o.trxType =:trx_type "
                    + " order by o.id asc";

            Query query = em.createQuery(sql);
            query.setParameter("user_id", Integer.parseInt(user_id));
            query.setParameter("inbox_id", inbox_id);
            query.setParameter("trx_id", trx_id);
            query.setParameter("prod_id", prod_id);
            query.setParameter("trx_type", trxType);
            
            query.setMaxResults(1);

            for (Inboxes m : (List<Inboxes>) query.getResultList()) {
                hasil = m.getId().intValue();
                logger.log(Level.INFO, "hasil cari duplikat inbox id : " + hasil);
            }

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }
        System.out.println("hasil duplikasi : " + hasil);
        return hasil;
    }

    public int isDuplicateTrxIdPostpaid(String trx_id, String user_id, int inbox_id) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();
        int hasil = 0;
        logger.log(Level.INFO, "cek duplikasi..." + trx_id + "#" + user_id + "#" + inbox_id);
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            //get current date time with Date()
            java.util.Date date = new java.util.Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            //System.out.println(dateFormat.format(date));

            String sql = " SELECT o FROM Inboxes o "
                    + " where o.trxId =:trx_id "
                    + " and o.trxId <> '0'"
                    + " and o.id not in (:inbox_id) "
                    + " and o.userId =:user_id "
                    + " and o.prodId =:prod_id "
                    + " and o.trxType =:trx_type "
                    + " order by o.id asc";

            Query query = em.createQuery(sql);
            query.setParameter("user_id", Integer.parseInt(user_id));
            query.setParameter("inbox_id", inbox_id);
            query.setParameter("trx_id", trx_id);
            query.setParameter("prod_id", 339);
            query.setParameter("trx_type", "2200");
            
            query.setMaxResults(1);

            for (Inboxes m : (List<Inboxes>) query.getResultList()) {
                hasil = m.getId().intValue();
                logger.log(Level.INFO, "hasil cari duplikat inbox id : " + hasil);
            }

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }
        System.out.println("hasil duplikasi : " + hasil);
        return hasil;
    }

    protected void resendMessageOutbox(long id) {
        String result = "";
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        logger.log(Level.INFO, "Resending... " + id);
        try {
            em.getTransaction().begin();
            Outboxes outbox = em.find(Outboxes.class, id);
            outbox.setStatus(0);
            em.getTransaction().commit();

            em.close();
            factory.close();

            logger.log(Level.INFO, "Resend ok " + id);
        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        } finally {

            if (em.isOpen()) {
                em.close();
            }
            if (factory.isOpen()) {
                factory.close();
            }

        }
    }

    public boolean checkExpiryOutbox(int id) {
        boolean result = false;
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();
        try {

            String sql = "SELECT o FROM Outboxes o "
                    + " where o.inboxId =:inbox_id "
                    + " order by o.inboxId asc";

            Query query = em.createQuery(sql);
            query.setParameter("inbox_id", id);
            query.setMaxResults(1);

            if (query.getResultList().size() > 0) {
                for (Outboxes m : (List<Outboxes>) query.getResultList()) {
                    logger.log(Level.INFO, "id : " + m.getId());

                    java.util.Date dtOutCreated = (java.util.Date) m.getCreateDate();
                    java.util.Date dtNow = new java.util.Date();

                    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

                    long diff = dtNow.getTime() - dtOutCreated.getTime();

                    long diffSeconds = diff / 1000 % 60;
                    long diffMinutes = diff / (60 * 1000) % 60;
                    logger.log(Level.INFO, "<" + id + "> menit : " + diffMinutes);
                    long diffHours = diff / (60 * 60 * 1000) % 24;
                    logger.log(Level.INFO, "<" + id + "> jam : " + diffHours);
                    long diffDays = diff / (24 * 60 * 60 * 1000);
                    logger.log(Level.INFO, "<" + id + "> hari : " + diffDays);

                    if (diffMinutes > 5 && diffDays >= 0 && diffHours >= 0) {
                        result = true;
                    }

                    logger.log(Level.INFO, "hasil expiry : " + result);
                }
            } else {
                logger.log(Level.INFO, "Outbox kosong : " + inbox_id);

                // cek date inbox_id
                sql = "SELECT o FROM Inboxes o "
                        + " where o.id =:inbox_id ";

                query = em.createQuery(sql);
                query.setParameter("inbox_id", id);
                query.setMaxResults(1);

                if (query.getResultList().size() > 0) {
                    for (Inboxes m : (List<Inboxes>) query.getResultList()) {

                        // compare dgn timestamp sekarang
                        java.util.Date dtInbox = (java.util.Date) m.getCreateDate();
                        java.util.Date dtNow = new java.util.Date();

                        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

                        long diff = dtNow.getTime() - dtInbox.getTime();

                        long diffSeconds = diff / 1000 % 60;
                        logger.log(Level.INFO, "detik : " + diffSeconds);
                        long diffMinutes = diff / (60 * 1000) % 60;
                        logger.log(Level.INFO, "menit : " + diffMinutes);
                        long diffHours = diff / (60 * 60 * 1000) % 24;
                        logger.log(Level.INFO, "jam : " + diffHours);
                        long diffDays = diff / (24 * 60 * 60 * 1000);
                        logger.log(Level.INFO, "hari : " + diffDays);

                        if (diffMinutes > 5 && diffDays >= 0 && diffHours >= 0) {
                            System.out.println("oioiio");
                            result = true;
                        }

                    }
                }
            }

            em.close();
            factory.close();
        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
        } finally {

            if (em.isOpen()) {
                em.close();
            }
            if (factory.isOpen()) {
                factory.close();
            }

        }

        return result;
    }

    public String getMessageOutbox(int id) {
        String result = "";
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();
        try {

            String sql = "SELECT o FROM Outboxes o "
                    + " where o.inboxId =:inbox_id order by o.inboxId asc";
            Query query = em.createQuery(sql);
            query.setParameter("inbox_id", id);
            query.setMaxResults(1);

            //if (query.getResultList().size()>0) {
            for (Outboxes m : (List<Outboxes>) query.getResultList()) {
                logger.log(Level.INFO, "id : " + m.getId());
                //resendMessageOutbox(m.getId());
                result = m.getMessage();
                logger.log(Level.INFO, result);
            }
            //}

            em.close();
            factory.close();
        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
        } finally {

            if (em.isOpen()) {
                em.close();
            }
            if (factory.isOpen()) {
                factory.close();
            }

        }

        return result;
    }

    public void replaceOutboxMessage(int inbox_id, String msg) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        try {

            em.getTransaction().begin();
            Query query = em.createQuery("UPDATE Outboxes o SET o.message = :message where o.inboxId=:inbox_id");
            query.setParameter("message", msg);
            int x = query.setParameter("inbox_id", inbox_id).executeUpdate();

            System.out.println("update : " + x);
            em.getTransaction().commit();

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());

        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

    }

    public void insertToOutboxOnly(String msg, String receiver, String receiver_type, String transaction_id,
            int user_id, String sender, int media_type_id, String response_code, String id_pelanggan) {
        //note: hanya untuk sekedar pencatatan inquiry bila mitra cuma ingin langsung bayar saja tanpa melalui process inquiry

        msg = msg.replace("<response>", "<response><stan_cycle>" + inbox_id + "</stan_cycle>");
        boolean isSMS = false;
        int status = 1;
        if (media_type_id != 1) {

        } else if (media_type_id == 1) {
            isSMS = true;
            status = 2;
        }

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        try {
            Outboxes outbox = new Outboxes();
            em.getTransaction().begin();

            outbox.setMessage(msg);
            outbox.setStatus(status);
            outbox.setCreateDate(new java.util.Date());
            outbox.setReceiver(receiver);
            outbox.setReceiverType(receiver_type);

            outbox.setTransactionId(inbox_id_pelangi);
            outbox.setInboxId(inbox_id_pelangi);
            outbox.setUserId(user_id);
            outbox.setSender(sender);
            outbox.setResponseCode(response_code);
            outbox.setMediaTypeId(media_type_id);
            outbox.setSms(isSMS);
            em.persist(outbox);
            em.getTransaction().commit();

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }
    }

    public void insertToOutbox(String msg, String receiver, String receiver_type, String transaction_id,
            int user_id, String sender, int media_type_id, String response_code, String id_pelanggan) {

        msg = msg.replace("<response>", "<response><stan_cycle>" + inbox_id + "</stan_cycle>");
        boolean isSMS = false;
        int status = 1;
        if (media_type_id != 1) {
            insertToQueue(msg, receiver, receiver_type, transaction_id,
                    user_id, sender, media_type_id, response_code, id_pelanggan);
        } else if (media_type_id == 1) {
            isSMS = true;
            status = 0;
        }

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        try {
            Outboxes outbox = new Outboxes();
            em.getTransaction().begin();

            outbox.setMessage(msg);
            outbox.setStatus(status);
            outbox.setCreateDate(new java.util.Date());
            outbox.setReceiver(receiver);
            outbox.setReceiverType(receiver_type);

            outbox.setTransactionId(inbox_id_pelangi);
            outbox.setInboxId(inbox_id_pelangi);
            outbox.setUserId(user_id);
            outbox.setSender(sender);
            outbox.setResponseCode(response_code);
            outbox.setMediaTypeId(media_type_id);
            outbox.setSms(isSMS);
            em.persist(outbox);
            em.getTransaction().commit();

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }
    }

    private void insertToQueue(String msg, String receiver, String receiver_type, String transaction_id, int user_id, String sender, int media_type_id, String response_code, String id_pelanggan) {
//        String QUEUE_NAME = "outbox_dev";
//        String TASK_QUEUE_NAME = "outbox_queue_dev";
//        String EXCHANGE_NAME = "outbox_general_exchange_dev";
//        String ROUTING_KEY = "outbox.general_dev";

        String QUEUE_NAME = "outbox";
        String TASK_QUEUE_NAME = "outbox_queue";
        String EXCHANGE_NAME = "outbox_general_exchange";
        String ROUTING_KEY = "outbox.general";

        try {

            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(setting.getRabbitHost());
            com.rabbitmq.client.Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            BillerConverter plnConverter = new BillerConverter();
            String message = receiver + "#" + msg;
            String isXml = "";
            if (sender.contains("NONXML")) {
                message = receiver + "#" + plnConverter.convertPrepaid(msg, id_pelanggan);
                isXml = "NONXML";
            } else {
//                QUEUE_NAME = "outboxxml";
//                TASK_QUEUE_NAME = "outboxxml_queue";
//                EXCHANGE_NAME = "outboxxml_general_exchange";
//                ROUTING_KEY = "outboxxml.general";

                QUEUE_NAME = setting.getRabbit_hostOut();
                TASK_QUEUE_NAME = setting.getRabbit_queueNameOut();
                EXCHANGE_NAME = setting.getRabbit_exchangeOut();
                ROUTING_KEY = setting.getRabbit_bindingKeyOut();
                
                isXml = "XML";
            }

            message += "#" + media_type_id;

            channel.exchangeDeclare(EXCHANGE_NAME, "topic", true);
            channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);

            if (!message.equalsIgnoreCase("0")) {

                channel.basicPublish(EXCHANGE_NAME, ROUTING_KEY,
                        MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());

                //System.out.println(" [x] Sent '" + message + "'");
                logger.log(Level.INFO, " [x] Sent '" + message + "'  <==>  " + sender + " <==> " + receiver + " <==> " + isXml);
            }

            channel.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e);
        }
    }

    public boolean setStatus(int code, int id) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        boolean result = false;
        System.out.print("Set status>>");
        try {

            em.getTransaction().begin();
            Query query = em.createQuery("UPDATE Inboxes o SET o.status = :stat where o.id=:inbox_id");
            query.setParameter("stat", code);
            int x = query.setParameter("inbox_id", id).executeUpdate();

            System.out.println("update : " + x);
            em.getTransaction().commit();

            em.close();
            factory.close();

            result = true;
            System.out.println("<<set status ok");
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage() + " " + e.toString());

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

            result = setStatus(code, id);
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        return result;
    }

    public boolean cekTokenUnsold(String idpel, int nominal, int inbox_id) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        boolean result = false;
        logger.log(Level.INFO, "Checking unsold token...");
        try {
            String sql = "SELECT COUNT(o.id) FROM TokenUnsolds o "
                    + " where (o.idpel =:idpel or o.snmeter =:snmeter) and o.sold=false"
                    + " and o.nominal=:nominal";
            Query query = em.createQuery(sql);
            query.setParameter("idpel", idpel);
            query.setParameter("snmeter", idpel);
            query.setParameter("nominal", nominal);
            //query.setMaxResults(1);
            Number qty = (Number) query.getSingleResult();
            logger.log(Level.INFO, "qty : " + qty.intValue());
            if (qty.intValue() > 0) {
                result = true;
                logger.log(Level.INFO, "ambil stok cuy " + idpel + "#" + nominal);
            } else {
                result = false;
            }

            em.close();
            factory.close();

        } catch (Exception e) {
            result = false;
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage() + " " + e.toString());

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        return result;
    }

    public String getTokenUnsold(String idpel, int nominal, int inbox_id) {
        String result = "";
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        try {

            long idx = 0;

            String sql = "";

            sql = "select o from TokenUnsolds o where (o.idpel =:idpel or o.snmeter =:snmeter) and o.sold=false "
                    + "and o.nominal=:nominal";

            em.getTransaction().begin();

            try {

                Query query = em.createQuery(sql);
                query.setParameter("idpel", idpel);
                query.setParameter("snmeter", idpel);
                query.setParameter("nominal", nominal);

                query.setMaxResults(1);

                for (TokenUnsolds voc : (List<TokenUnsolds>) query.getResultList()) {

                    idx = voc.getId();
                    result = voc.getIso();
                    logger.log(Level.INFO, "iso unsold : " + result);

                    //replace stan in iso
                }

            } catch (NoResultException e) {
                System.out.println("Error no result voucher : " + e.getMessage());
                idx = 0;
            }

            if (idx != 0) {
                //   em.getTransaction().begin();

                TokenUnsolds voucher = em.find(TokenUnsolds.class, idx);
                
                voucher.setSoldDate(new java.util.Date());
                voucher.setSold(Boolean.TRUE);
                voucher.setInboxId(inbox_id);

                //   em.getTransaction().commit();
            }

            em.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        return result;
    }

}
