

package BILLER.SW;

import ppob_biller.Settings;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class SLSLogger {

    Settings setting = new Settings();

    /*public static void main(String x[]) {
        SLSLogger a = new SLSLogger();
        //a.logAction("lsdf","PPOB");
        a.recapPLN("ngonar", "99501");
    }*/
    

    public void logAction(String iso,String payment_type) {
        try{
           /* String data = iso+"\r\n";

            Calendar currentDate = Calendar.getInstance();
            SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
            String tgl = formatter.format(currentDate.getTime());

            File file =new File(setting.getSwitchingCID()+"-"+payment_type+"-"+tgl+".log");

            //if file doesnt exists, then create it
            if(!file.exists()){
                    file.createNewFile();
            }

            //true = append file
            FileWriter fileWritter = new FileWriter(file.getName(),true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.close();
            */


    	}catch(Exception e){
            e.printStackTrace();
    	}
    }

    public void recapPLNPrepaid(String iso,String payment_type) {
        try{
            
            String header = "ID|" +
                    //"CA_ID|" +
                    //"CENTRAL_ID|" +
                    "MERCHANT|"+
                    "REFNUM|" +
                    "SREFNUM|" +
                    "METERNUM|" +
                    "TRAN_AMOUNT|" +
                    "ADMIN|" +
                    "RP_STAMPDUTY|" +
                    "RP_VAT|" +
                    "RP_PLT|" +
                    "RP_CPI|" +
                    "PP|" +
                    "PU|" +
                    "TOKEN|" +
                    "BANKCODE|" +
                    "PP_ID\r\n";

            String data = iso+"\r\n";

            Calendar currentDate = Calendar.getInstance();
            SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
            String tgl = formatter.format(currentDate.getTime());

            File file =new File(setting.getSwitchingCID()+"-"+payment_type+"-"+tgl+".ftr");

            //if file doesnt exists, then create it
            if(!file.exists()){
                file.createNewFile();
                FileWriter fileWritter = new FileWriter(file.getName(),true);
                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
                bufferWritter.write(header);
                bufferWritter.close();
            }

            //true = append file
            FileWriter fileWritter = new FileWriter(file.getName(),true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.close();


    	}catch(IOException e){
            e.printStackTrace();
    	}
    }

    public void recapPLN(String iso,String payment_type) {
        try{
            setting.setConnections();
           
            String header = "ID|" +
                    "CA_ID|" +
                    "CENTRAL_ID|" +
                    "MERCHANT|"+
                    "SREFNUM|" +
                    "IDPEL|" +
                    "BILL_PERIOD|" +
                    "TRAN_AMOUNT|" +
                    "TOTAL_BILL|" +
                    "BILL|" +
                    "RP_INSENTIF|" +
                    "VAT|" +
                    "PENALTY|" +
                    "ADM_CHARGE|" +
                    "TERMINAL_ID\r\n";

            String data = iso+"\r\n";

            Calendar currentDate = Calendar.getInstance();
            SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
            String tgl = formatter.format(currentDate.getTime());

            File file =new File(setting.getSwitchingCID()+"-"+payment_type+"-"+tgl+".ftr");

            //if file doesnt exists, then create it
            if(!file.exists()){
                file.createNewFile();
                FileWriter fileWritter = new FileWriter(file.getName(),true);
                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
                bufferWritter.write(header);
                bufferWritter.close();
            }

            //true = append file
            FileWriter fileWritter = new FileWriter(file.getName(),true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.close();


    	}catch(IOException e){
            e.printStackTrace();
    	}
    }

    public void recapPLNNon(String iso,String payment_type) {
        try{
            setting.setConnections();

            String header = "DT"
                   + "|CENTRAL_ID"
                   + "|MERCHANT"
                   + "|REFNUM"
                   + "|SREFNUM"
                   + "|SUBID"
                   + "|REGNUM"
                   + "|REGD"
                   + "|TRAN_CODE"
                   + "|TOTAL_AMOUNT"
                   + "|TRAN_AMOUNT"
                   + "|ADMIN_CHARGES"
                   + "|BANKCODE"
                   + "|PPID"
                   + "\r\n";

            String data = iso+"\r\n";

            Calendar currentDate = Calendar.getInstance();
            SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
            String tgl = formatter.format(currentDate.getTime());

            File file =new File(setting.getSwitchingCID()+"-"+payment_type+"-"+tgl+".ftr");

            //if file doesnt exists, then create it
            if(!file.exists()){
                file.createNewFile();
                FileWriter fileWritter = new FileWriter(file.getName(),true);
                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
                bufferWritter.write(header);
                bufferWritter.close();
            }

            //true = append file
            FileWriter fileWritter = new FileWriter(file.getName(),true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.close();


    	}catch(IOException e){
            e.printStackTrace();
    	}
    }

}
