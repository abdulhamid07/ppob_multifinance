package BILLER.SW;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import ppob_biller.Settings;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import model.BpjsKesehatans;
import model.PlnPostpaids;
import model.Transactions;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.*;
import utils.ParseISOMessage;

/**
 *
 * @author ngonar
 */
public class PostPaidProcessing {

    private static final Logger logger = Logger.getLogger("PostPaidProcessing");

    PostPaid post = new PostPaid();
    PostpaidResponseCode postRC = new PostpaidResponseCode();
    Settings setting = new Settings();

    public PostPaidProcessing() {

    }

    public String processInquiryBpjs(String hasil, String inbox_id_asli, int product_code, String dataLoket) {
        //String[] result = post.parseInquiryMsgResponse(hasil, false);
        GenericResponseCode genericResponseCode = new GenericResponseCode();
        ParseISOMessage iSOMessage = new ParseISOMessage();
        Map<String, String> bitMapNyo = null;
        try {
            
            bitMapNyo = iSOMessage.getBitmap(hasil);
            
        } catch (Exception ex) {
            logger.log(Level.FATAL, ex.getMessage());
//            java.util.logging.Logger.getLogger(PDAM.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        String[] datas = new String[16];
        Map<String, String> tagMap = new HashMap<String, String>();
        
        tagMap.put("2", "pan");
        tagMap.put("4", "amount");
        tagMap.put("11", "stan");
        tagMap.put("12", "date_time");
        tagMap.put("18", "merchant_type");
        tagMap.put("32", "bank_code");
        tagMap.put("33", "cid");
        tagMap.put("39", "rc");
        tagMap.put("41", "terminal_id");
        tagMap.put("48", "private_data_48");

//        String[] activeBit = {"MTI", "2", "4", "11", "12", "26", "32", "33", "39", "41", "48"};
        String[] activeBit = {"2", "4", "11", "12", "18", "32", "39", "41", "48"};

        String xml = "<?xml version='1.0' encoding='UTF-8'?><response>";
        xml += "<trx_type>INQUIRY</trx_type>";
        xml += "<product_type>MULTI-FINANCE</product_type>";
        xml += "<product_code>" + product_code + "</product_code>";

//        String[] result = pdam.parseInquiryMsgResponse(bitMapNyo, false);
        if (bitMapNyo.get("39").equalsIgnoreCase("0000")) {

            for (int i = 0; i < activeBit.length; i++) {
//                String activeBit1 = activeBit[i];
                if (activeBit[i].equalsIgnoreCase("4")) {
                    xml += "<" + tagMap.get(activeBit[i]) + ">" + bitMapNyo.get(activeBit[i]).substring(4) + "</" + tagMap.get(activeBit[i]) + ">";
                } else {
                    xml += "<" + tagMap.get(activeBit[i]) + ">" + bitMapNyo.get(activeBit[i]) + "</" + tagMap.get(activeBit[i]) + ">";
                }
            }
            
            String[] bit48 = post.parseBit48InquiryResponseSA_BPJS(bitMapNyo.get("48"), bitMapNyo.get("39"));
            String[] titleMain = {
                "switcher_id",
                "no_va",
                "jumlah_bulan",
                "nama",
                "kode_cabang",
                "nama_cabang",
                "biaya_premi",
                "biaya_admin",
                "amount",
                "sisa"};

            int mainData = 7;
            int detailData = 4;
            int indexData = 0;
//            for (int i = 0; i < mainData; i++) {
            for (int i = 0; i < titleMain.length; i++) {
                indexData = i;
                if (!titleMain[i].equalsIgnoreCase("amount")
                        && !titleMain[i].equalsIgnoreCase("switcher_id") //                        && !titleMain[i].equalsIgnoreCase("private_data_48")
                        ) {
                    xml += "<" + titleMain[i] + ">" + bit48[i] + "</" + titleMain[i] + ">";
                }
            }
            xml += "<cmd>" + dataLoket + "</cmd>";
            System.out.println("xml response : ");
            logger.log(Level.INFO, xml);
            System.out.println(xml);
        } else {
            System.out.println("++++++++++++++kena dua++++++++++++++++++++");
//            xml = "<?xml version='1.0' encoding='UTF-8'?>"
            //String[] bit48 = pdam.parseBit48Inquiry(bitMapNyo.get("48"), bitMapNyo.get("39"));
            xml += "<stan>" + bitMapNyo.get("11") + "</stan>";
            xml += "<rc>" + bitMapNyo.get("39") + "</rc>";
            xml += "<code>" + bitMapNyo.get("39") + "</code>";
            xml += "<desc>" + genericResponseCode.getResponseDesctiprionBpjs(bitMapNyo.get("39")) + "</desc>";
//               String idpel = bit48[0];
        }

        xml += "</response>";

//                System.out.println("++++++++++++++kena tiga++++++++++++++++++++");
//        xml.replace("norek_val", bitMapNyo.get("48").substring(0,8));
        String nn = bitMapNyo.get("48").substring(0, 7);
        xml = xml.replace("norek_val", nn);
        return xml;
    }

    public String processPaymentBpjs(String hasil, String inbox_id_asli, double harga,
            String[] detail, int product_code, String inboxIdPelangi, String[] userData) {
        //String[] result = post.parseInquiryMsgResponse(hasil, false);
        GenericResponseCode genericResponseCode = new GenericResponseCode();
        ParseISOMessage iSOMessage = new ParseISOMessage();
        Map<String, String> bitMapNyo = null;
        try {
            bitMapNyo = iSOMessage.getBitmap(hasil);
        } catch (Exception ex) {
            logger.log(Level.FATAL, ex.getMessage());
//            java.util.logging.Logger.getLogger(PDAM.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        String[] datas = new String[16];
        Map<String, String> tagMap = new HashMap<String, String>();
        tagMap.put("MTI", "mti");
        tagMap.put("2", "pan");
//            tagMap.put("3", "processing_code");
        tagMap.put("4", "amount");
//            tagMap.put("7", "transmission_date_time");
        tagMap.put("11", "stan");
        tagMap.put("12", "date_time");
//            tagMap.put("13", "local_trx_date");
//            tagMap.put("15", "settlement_date");
        tagMap.put("26", "merchant_type");
        tagMap.put("32", "bank_code");
        tagMap.put("33", "cid");
//            tagMap.put("37", "retrieval_ref_no");
        tagMap.put("39", "rc");
        tagMap.put("41", "terminal_id");
//        tagMap.put("42", "acceptor_identification_code");
        tagMap.put("48", "private_data_48");
        tagMap.put("62", "info_text");
//        tagMap.put("63", "info_loket");

//        String[] activeBit = {"MTI", "2", "4", "11", "12", "26", "32", "33", "39", "41", "48"};
        String[] activeBit = {"4", "11", "12", "26", "32", "39", "41", "62", "48"};

        String xml = "<?xml version='1.0' encoding='UTF-8'?><response>";
        xml += "<trx_type>PAYMENT</trx_type>";
        xml += "<product_type>MULTI-FINANCE</product_type>";
        xml += "<product_code>" + product_code + "</product_code>";
        xml += "<stan>" + inboxIdPelangi + "</stan>";
//        xml += "<kode_loket>" + userData[0] + "</kode_loket>";

//        String[] result = pdam.parseInquiryMsgResponse(bitMapNyo, false);
        if (bitMapNyo.get("39").equalsIgnoreCase("0000")) {

            for (int i = 0; i < activeBit.length; i++) {
                String activeBit1 = activeBit[i];
//                xml += "<" + tagMap.get(activeBit[i]) + ">" + bitMapNyo.get(activeBit[i]) + "</" + tagMap.get(activeBit[i]) + ">";
                if (activeBit[i].equalsIgnoreCase("4")) {
                    xml += "<" + tagMap.get(activeBit[i]) + ">" + bitMapNyo.get(activeBit[i]).substring(4) + "</" + tagMap.get(activeBit[i]) + ">";
                } else {
                    xml += "<" + tagMap.get(activeBit[i]) + ">" + bitMapNyo.get(activeBit[i]) + "</" + tagMap.get(activeBit[i]) + ">";
                }

//                if (activeBit[i].equalsIgnoreCase("11")) {
//                    xml += "<" + tagMap.get(activeBit[i]) + ">" + inboxIdPelangi + "</" + tagMap.get(activeBit[i]) + ">";
//                }
            }

            String[] bit48 = post.parseBit48PaymentResponseSA_BPJS(bitMapNyo.get("48"), bitMapNyo.get("39"));
            String[] titleMain = {
                //                "idpel", "blth", "name", "bill_count", "bill_repeat_count", "rp_tag", "biaya_admin"
                "switcher_id",
                "no_va",
                "jumlah_bulan",
                "nama",
                "kode_cabang",
                "nama_cabang",
                "biaya_premi",
                "biaya_admin",
                "amount",
                "sisa",
                "reffno",
                "tgl_lunas"
            };
//            String[] titleDetail = {
//                "bill_date", "bill_amount", "penalty", "kubikasi"
//            };

            int mainData = 7;
            int detailData = 4;
            int indexData = 0;
            long biayaAdmin = 0;
            long biayaPremi = 0;
            long amount = 0;
//            for (int i = 0; i < mainData; i++) {
            for (int i = 0; i < titleMain.length; i++) {
                indexData = i;
                if (!titleMain[i].equalsIgnoreCase("amount")
                        && !titleMain[i].equalsIgnoreCase("switcher_id") //                        && !titleMain[i].equalsIgnoreCase("private_data_48")
                        ) {
                    if (!titleMain[i].equalsIgnoreCase("amount")) {
                        xml += "<" + titleMain[i] + ">" + bit48[i] + "</" + titleMain[i] + ">";

                        if (titleMain[i].equalsIgnoreCase("biaya_admin")) {
                            biayaAdmin = Long.parseLong(bit48[i]);
                        }
                        if (titleMain[i].equalsIgnoreCase("biaya_premi")) {
                            biayaPremi = Long.parseLong(bit48[i]);
                        }
                        
                    }
                }
                if (titleMain[i].equalsIgnoreCase("amount")) {
                    amount = Long.parseLong(bit48[i]);
                }

            }

            //
            String[] bit63 = post.parseBit63PaymentResponseSA_BPJS(bitMapNyo.get("63"), bitMapNyo.get("39"));
            String[] titleMain63 = {
                //                "idpel", "blth", "name", "bill_count", "bill_repeat_count", "rp_tag", "biaya_admin"
                "kode_loket",
                "nama_loket",
                "alamat_loket",
                "phone_loket",
                "kode_kab_kota",
                "kab_kota",
                "kode_pos"
            };

            mainData = 7;
            detailData = 4;
            indexData = 0;
//            for (int i = 0; i < mainData; i++) {
            for (int i = 0; i < titleMain63.length; i++) {
                indexData = i;
                if (!titleMain63[i].equalsIgnoreCase("amount")
                        && !titleMain63[i].equalsIgnoreCase("switcher_id") //                        && !titleMain[i].equalsIgnoreCase("private_data_48")
                        ) {
                    if (!titleMain63[i].equalsIgnoreCase("amount")) {
                        xml += "<" + titleMain63[i] + ">" + bit63[i] + "</" + titleMain63[i] + ">";
                    }
                }
            }

//            //indexData++;
//            xml += "<bills>";
//            for (int i = 0; i < Integer.parseInt(bit48[4]); i++) {
//                xml += "<bill>";
//                for (int j = 0; j < detailData; j++) {
//                    indexData++;
//                    xml += "<" + titleDetail[j] + ">" + bit48[indexData] + "</" + titleDetail[j] + ">";
//                }
//                xml += "</bill>";
//            }
//            xml += "</bills>";
            try {
                xml += "<cust_msisdn>" + userData[3] + "</cust_msisdn>";
            } catch (Exception e) {
                xml += "<cust_msisdn></cust_msisdn>";
            }
            xml += "<saldo>" + detail[10] + "</saldo>";
            xml += "<harga>" + (long) harga + "</harga>";
            xml += "</response>";

            String bill_status = "1";
            long idx = insertToTrx(
                    Integer.parseInt(detail[5]),
                    Integer.parseInt(detail[7]),
                    detail[4],
                    detail[1],
                    detail[6],
//                    Long.parseLong(bitMapNyo.get("4").substring(4)) + (Long.parseLong(detail[9]) * Integer.parseInt(bill_status)),
//                    Long.parseLong(bitMapNyo.get("4").substring(4)),
                    //                    Integer.parseInt(detail[11]),
                    amount,
                    biayaPremi,
                    Integer.parseInt(inboxIdPelangi),
                    bitMapNyo.get("39"),
                    "MULTI-FINANCE",
                    Long.parseLong("0"),
                    Short.parseShort("0"),
                    Short.parseShort("0"),
                    bit48[10],
                    detail[4],
                    Short.parseShort("0"));

            int inbox_id_bpjs = Integer.parseInt(detail[11]);
            String transid = String.valueOf(idx);
            String partnertid = detail[1];
            String restype = bitMapNyo.get("39");
            String rescode = bitMapNyo.get("39");
            String idpelBpjs = bit48[1];
            String jumlahrek = bit48[2];
            String nama = bit48[3];
            String kodeCabang = bit48[4];
            String namaCabang = bit48[5];
            String tagihan = bit48[6];
            String admin = bit48[7];
            String totalBayar = bit48[8];
            String sisaSebelumnya = bit48[9];
            String refnbr = bit48[10];
            String tglLunas = bit48[11];
            String infoBpjs = bitMapNyo.get(63);
            String saldo = "";
            String create_date = "";
//            String message = xml;

            insertToBpjsKesehatan(
                    inbox_id_bpjs,
                    transid,
                    partnertid,
                    restype,
                    rescode,
                    idpelBpjs,
                    nama,
                    jumlahrek,
                    tagihan,
                    admin,
                    refnbr,
                    saldo,
                    create_date,
                    xml,
                    infoBpjs,
                    namaCabang,
                    kodeCabang,
                    totalBayar,
                    sisaSebelumnya,
                    tglLunas
            );

//            insertToDB(
//                    Integer.parseInt(detail[11]), 
//                    String.valueOf(idx), 
//                    "", 
//                    "", 
//                    result[9], 
//                    subscriber_id,
//                    subscriber_name, subscriber_segmentation + " / " + power_consuming_category,
//                    bill_status, outstanding_bill, String.valueOf(Long.parseLong(result[2].substring(4)) + Long.parseLong(detail[9]) * Integer.parseInt(bill_status)),
//                    penalty_fee, total_admin_charge,
//                    incentive, blth_summary, stand_meter_summary + " - " + sm_last,
//                    switcher_ref, result[12], "", "", "", service_unit_phone, hasil, iso,
//                    result[7], result[6], String.valueOf(total_tag + total_bk),
//                    String.valueOf(total_vat),
//                    String.valueOf(total_penalty),
//                    result[10], result[4]
//            );
        } else {
            System.out.println("++++++++++++++kena dua++++++++++++++++++++");
//            xml = "<?xml version='1.0' encoding='UTF-8'?>"
            //String[] bit48 = pdam.parseBit48Inquiry(bitMapNyo.get("48"), bitMapNyo.get("39"));

//            String rc = bitMapNyo.get("39");
            if (bitMapNyo.get("39").equalsIgnoreCase("0046")) {
                bitMapNyo.put("39", "0076");
                logger.log(Priority.FATAL, "XXXXXXXXXXXXXXXXXX          SALDO HABIS          XXXXXXXXXXXXXXXXXX");
            }

            xml += "<rc>" + bitMapNyo.get("39") + "</rc>";
            xml += "<code>" + bitMapNyo.get("39") + "</code>"
                    + "<desc>" + genericResponseCode.getResponseDesctiprionBpjs(bitMapNyo.get("39")) + "</desc>";
//                String idpel = bit48[0];

            xml += "<saldo>" + detail[10] + "</saldo>";
            xml += "<harga>" + (long) harga + "</harga>";
            xml += "</response>";

        }

        System.out.println("xml response : ");
        logger.log(Level.INFO, xml);
        System.out.println(xml);

        String[] result = post.parsePaymentMsgResponse(hasil, false);
//    public Long insertToTrx(int user_id, int product_id, String destination, String sender,
//            String sender_type, long price_sell, long price_buy, int inbox_id,
//            String status, String remark, long saldo_awal, short counter,
//            short counter2, String sn, String receiver, short resend) {

//                System.out.println("++++++++++++++kena tiga++++++++++++++++++++");
//        xml.replace("norek_val", bitMapNyo.get("48").substring(0,8));
//        String nn = bitMapNyo.get("48").substring(0, 7);
//        xml = xml.replace("norek_val", nn);
        return xml;
    }

    public void insertToBpjsKesehatan(
            int inbox_id_bpjs,
            String transid,
            String partnertid,
            String restype,
            String rescode,
            String idpelBpjs,
            String nama,
            String jumlahrek,
            String tagihan,
            String admin,
            String refnbr,
            String saldo,
            String create_date,
            String xml,
            String info,
            String namaCabang,
            String kodeCabang,
            String totalBayar,
            String sisaSebelumnya,
            String tglLunas
    ) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();
//        hjgjh
        try {
            BpjsKesehatans post = new BpjsKesehatans();
            em.getTransaction().begin();

            post.setIdpel(idpelBpjs);
            post.setInboxId(inbox_id_bpjs);
            post.setInfo(info);
            post.setJumlahrek(jumlahrek);
            post.setNama(nama);
            post.setPartnertid(partnertid);
            post.setRefnbr(refnbr);
            post.setRescode(rescode);
            post.setRptotal(totalBayar);
            post.setBiayapremi(tagihan);
            post.setAdmin(admin);
            post.setTransid(transid);
//            post.setIso(iso);
            post.setXml(xml);
            post.setCreateDate(new Date());
            post.setKodecabang(kodeCabang);
            post.setNamacabang(namaCabang);
            post.setSisa(sisaSebelumnya);
            post.setTgllunas(tglLunas);
            post.setReprint(0);
//            post.setInfo(info);

            em.persist(post);
            em.getTransaction().commit();

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
        } finally {
            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
        }

        String mitraToRecap = setting.getMitraToRecap();

        if (mitraToRecap.toLowerCase().contains(partnertid)) {
            try {
                recapMitra(
                        inbox_id_bpjs,
                        transid,
                        partnertid,
                        restype,
                        rescode,
                        idpelBpjs,
                        nama,
                        jumlahrek,
                        tagihan,
                        admin,
                        refnbr,
                        saldo,
                        create_date,
                        xml,
                        info,
                        namaCabang,
                        kodeCabang,
                        totalBayar,
                        sisaSebelumnya,
                        tglLunas
                );
            } catch (Exception e) {
                logger.log(Priority.FATAL, "RECAP ");
                logger.log(Priority.FATAL, e.getMessage());
            }
        }
    
    }

    private static String getHeader() {
        String header = String.format("%-24s", "TANGGAL") + "|"
                + String.format("%-8s", "CID") + "|"
                + String.format("%-18s", "NO VA") + "|"
                + String.format("%-7s", "PERIODE") + "|"
                + String.format("%-32s", "REFF NO") + "|"
                + String.format("%-18s", "ADMIN") + "|"
                + String.format("%-18s", "PREMI") + "|"
                + String.format("%-18s", "TOTAL") + "|"
                //                + String.format("%-18s", "sisa") + "|"
                + String.format("%-24s", "TGL LUNAS");
//                + String.format("%-20s", "iso");

        return header;
    }

    public static void recapMitra(
            int inbox_id_bpjs,
            String transid,
            String partnertid,
            String restype,
            String rescode,
            String idpelBpjs,
            String nama,
            String jumlahrek,
            String tagihan,
            String admin,
            String refnbr,
            String saldo,
            String create_date,
            String iso,
            String info,
            String namaCabang,
            String kodeCabang,
            String totalBayar,
            String sisaSebelumnya,
            String tglLunas
    ) {
        try {

            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            DateFormat dateFormat3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DateFormat dateFormat2 = new SimpleDateFormat("yyyyMMdd");
            Calendar currentDate = Calendar.getInstance();
            String trxDateTime = dateFormat.format(currentDate.getTime());
            String trxDate = trxDateTime.substring(0, 8);
            String trxHour = trxDateTime.substring(9, 11);

            String fileDate = "";
            if (Integer.parseInt(trxHour) >= 12) {
                Calendar tomorrowDate = Calendar.getInstance();
                tomorrowDate.add(Calendar.DATE, 1);
                fileDate = dateFormat2.format(tomorrowDate.getTime());
            } else {
                fileDate = dateFormat2.format(currentDate.getTime());
            }
//            Calendar tomorrowDate = Calendar.getInstance();
//            tomorrowDate.add(Calendar.DATE, 1);

            String header = getHeader();
            String data = String.format("%-24s", dateFormat3.format(currentDate.getTime())) + "|"
                    + String.format("%-8s", partnertid) + "|"
                    + String.format("%-18s", idpelBpjs) + "|"
                    + String.format("%-7s", jumlahrek) + "|"
                    + String.format("%-32s", refnbr) + "|"
                    + String.format("%-18s", admin) + "|"
                    + String.format("%-18s", tagihan) + "|"
                    + String.format("%-18s", totalBayar) + "|"
                    //                + String.format("%-18s", sisaSebelumnya) + "|"
                    + String.format("%-24s", tglLunas);
//                + String.format("%-20s", "iso");

//            Calendar currentDate = Calendar.getInstance();
//            SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
//            String tgl = formatter.format(currentDate.getTime());
            //File file =new File(setting.getSwitchingCid()+"-"+payment_type+"-"+tgl+".ftr");
//            File file =new File(payment_type+"-"+tgl+".ftr");
            String fileName = "BPJS-" + partnertid + "-" + fileDate + ".ftr";
            File file = new File(fileName);

            //if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
                FileWriter fileWritter = new FileWriter(file.getName(), true);
                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
                bufferWritter.write(header);
                bufferWritter.close();
            }

            //true = append file
            FileWriter fileWritter = new FileWriter(file.getName(), true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);

            bufferWritter.write(
                    "\r\n" + data);
            bufferWritter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public String processPayment(String hasil, String detail[], double harga) {
//asdfads
        String iso = hasil;
        String[] result = post.parsePaymentMsgResponse(hasil, false);

        String blth_summary = "";
        String stand_meter_summary = "", sm_last = "";

        logger.log(Level.INFO, "detail[9]" + detail[9]);

        if (result[9].equalsIgnoreCase("0000") || result[9].equalsIgnoreCase("0012")) {

            String[] bit48 = post.parseBit48Payment(result[11], result[9]);

            String switcher_id = bit48[0];
            String subscriber_id = bit48[1];
            String bill_status = bit48[2];
            logger.log(Level.INFO, "Bill status : " + bill_status);
            String payment_status = bit48[3];
            String outstanding_bill = bit48[4];
            String switcher_ref = bit48[5];
            String subscriber_name = bit48[6];
            String service_unit = bit48[7];
            String service_unit_phone = bit48[8];
            String subscriber_segmentation = bit48[9];
            String power_consuming_category = bit48[10];
            String total_admin_charge = String.valueOf(Integer.parseInt(detail[9]) * Integer.parseInt(bill_status));

            double total_bk = 0, total_tag = 0;

            //1 bill
            String bill_period = bit48[11 + 1];
            String due_date = bit48[12 + 1];
            String meter_read_date = bit48[13 + 1];
            String total_electricity_bill = bit48[14 + 1];
            String incentive = bit48[15 + 1];
            String vat = bit48[16 + 1];
            String penalty_fee = bit48[17 + 1];
            String prev_meter_reading1 = bit48[18 + 1];
            String curr_meter_reading1 = bit48[19 + 1];
            String prev_meter_reading2 = bit48[20 + 1];
            String curr_meter_reading2 = bit48[21 + 1];
            String prev_meter_reading3 = bit48[22 + 1];
            String curr_meter_reading3 = bit48[23 + 1];

            String bill_period_ = "", bill_period__ = "", bill_period___ = "";
            String due_date_ = "", due_date__ = "", due_date___ = "";
            String meter_read_date_ = "", meter_read_date__ = "", meter_read_date___ = "";
            String total_electricity_bill_ = "0", total_electricity_bill__ = "0", total_electricity_bill___ = "0";
            String incentive_ = "0", incentive__ = "0", incentive___ = "0";
            String vat_ = "0", vat__ = "0", vat___ = "0";
            String penalty_fee_ = "0", penalty_fee__ = "0", penalty_fee___ = "0";
            String prev_meter_reading1_ = "", prev_meter_reading1__ = "", prev_meter_reading1___ = "";
            String curr_meter_reading1_ = "", curr_meter_reading1__ = "", curr_meter_reading1___ = "";
            String prev_meter_reading2_ = "", prev_meter_reading2__ = "", prev_meter_reading2___ = "";
            String curr_meter_reading2_ = "", curr_meter_reading2__ = "", curr_meter_reading2___ = "";
            String prev_meter_reading3_ = "", prev_meter_reading3__ = "", prev_meter_reading3___ = "";
            String curr_meter_reading3_ = "", curr_meter_reading3__ = "", curr_meter_reading3___ = "";

            if (Integer.parseInt(bill_status) > 1) {
                //2 bill
                bill_period_ = bit48[25];
                due_date_ = bit48[26];
                meter_read_date_ = bit48[27];
                total_electricity_bill_ = bit48[28];
                incentive_ = bit48[29];
                vat_ = bit48[30];
                penalty_fee_ = bit48[31];
                prev_meter_reading1_ = bit48[32];
                curr_meter_reading1_ = bit48[33];
                prev_meter_reading2_ = bit48[34];
                curr_meter_reading2_ = bit48[35];
                prev_meter_reading3_ = bit48[36];
                curr_meter_reading3_ = bit48[37];
            }

            if (Integer.parseInt(bill_status) > 2) {
                //3 bill
                bill_period__ = bit48[38];
                due_date__ = bit48[39];
                meter_read_date__ = bit48[40];
                total_electricity_bill__ = bit48[41];
                incentive__ = bit48[42];
                vat__ = bit48[43];
                penalty_fee__ = bit48[44];
                prev_meter_reading1__ = bit48[45];
                curr_meter_reading1__ = bit48[46];
                prev_meter_reading2__ = bit48[47];
                curr_meter_reading2__ = bit48[48];
                prev_meter_reading3__ = bit48[49];
                curr_meter_reading3__ = bit48[50];
            }

            if (Integer.parseInt(bill_status) > 3) {
                //4 bill
                bill_period___ = bit48[51];
                due_date___ = bit48[52];
                meter_read_date___ = bit48[53];
                total_electricity_bill___ = bit48[54];
                incentive___ = bit48[55];
                vat___ = bit48[56];
                penalty_fee___ = bit48[57];
                prev_meter_reading1___ = bit48[58];
                curr_meter_reading1___ = bit48[59];
                prev_meter_reading2___ = bit48[60];
                curr_meter_reading2___ = bit48[61];
                prev_meter_reading3___ = bit48[62];
                curr_meter_reading3___ = bit48[63];
            }

            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"
                    + "<produk>MULTI-FINANCE</produk>";

            if (result[0].equalsIgnoreCase("2410") || result[0].equalsIgnoreCase("2411")) {
                hasil += "<msg_type>REVERSAL</msg_type>";
            }

            //subscriber_name = subscriber_name.replaceAll("<", "&lt;");
            //subscriber_name = subscriber_name.replaceAll(">", "&gt;");
            subscriber_name = StringEscapeUtils.escapeXml(subscriber_name);

            hasil += "<amount>" + (Long.parseLong(result[2].substring(4)) + (Integer.parseInt(detail[9]) * Integer.parseInt(bill_status))) + "</amount>"
                    + "<stan>" + result[3] + "</stan>"
                    + "<datetime>" + result[4] + "</datetime>"
                    + "<date_settlement>" + result[5] + "</date_settlement>"
                    + "<merchant_code>" + result[6] + "</merchant_code>"
                    + "<bank_code>" + result[7] + "</bank_code>"
                    + "<rc>" + result[9] + "</rc>"
                    + "<terminal_id>" + result[10] + "</terminal_id>"
                    + "<switcher_id>" + switcher_id + "</switcher_id>"
                    + "<bill_status>" + bill_status + "</bill_status>"
                    + "<outstanding_bill>" + outstanding_bill + "</outstanding_bill>"
                    + "<payment_status>" + payment_status + "</payment_status>"
                    + "<subscriber_id>" + subscriber_id + "</subscriber_id>"
                    + "<switcher_refno>" + switcher_ref + "</switcher_refno>"
                    + "<subscriber_name>" + subscriber_name + "</subscriber_name>"
                    + "<service_unit>" + service_unit + "</service_unit>"
                    + "<service_unit_phone>" + service_unit_phone + "</service_unit_phone>"
                    + "<subscriber_segmentation>" + subscriber_segmentation + "</subscriber_segmentation>"
                    + "<power>" + power_consuming_category + "</power>"
                    + "<admin_charge>" + total_admin_charge + "</admin_charge>"
                    + "<info_text>" + setting.getPostInfo() + "</info_text>"
                    + "<bills>"
                    + "<bill>"
                    + "<bill_period>" + bill_period + "</bill_period>"
                    + "<due_date>" + due_date + "</due_date>"
                    + "<meter_read_date>" + meter_read_date + "</meter_read_date>"
                    + "<total_electricity_bill>" + total_electricity_bill + "</total_electricity_bill>"
                    + "<incentive>" + incentive + "</incentive>"
                    + "<value_added_tax>" + vat + "</value_added_tax>"
                    + "<penalty_fee>" + penalty_fee + "</penalty_fee>"
                    + "<previous_meter_reading1>" + prev_meter_reading1 + "</previous_meter_reading1>"
                    + "<current_meter_reading1>" + curr_meter_reading1 + "</current_meter_reading1>"
                    + "<previous_meter_reading2>" + prev_meter_reading2 + "</previous_meter_reading2>"
                    + "<current_meter_reading2>" + curr_meter_reading2 + "</current_meter_reading2>"
                    + "<previous_meter_reading3>" + prev_meter_reading3 + "</previous_meter_reading3>"
                    + "<current_meter_reading3>" + curr_meter_reading3 + "</current_meter_reading3>"
                    + "</bill>";

            blth_summary += getMonthName(bill_period.substring(4)) + bill_period.substring(2, 4);
            stand_meter_summary += prev_meter_reading1;
            sm_last = curr_meter_reading1;

            total_bk += Double.parseDouble(penalty_fee);
            total_tag += Double.parseDouble(total_electricity_bill);

            if (Integer.parseInt(bill_status) > 1) {

                hasil += "<bill>"
                        + "<bill_period>" + bill_period_ + "</bill_period>"
                        + "<due_date>" + due_date_ + "</due_date>"
                        + "<meter_read_date>" + meter_read_date_ + "</meter_read_date>"
                        + "<total_electricity_bill>" + total_electricity_bill_ + "</total_electricity_bill>"
                        + "<incentive>" + incentive_ + "</incentive>"
                        + "<value_added_tax>" + vat_ + "</value_added_tax>"
                        + "<penalty_fee>" + penalty_fee_ + "</penalty_fee>"
                        + "<previous_meter_reading1>" + prev_meter_reading1_ + "</previous_meter_reading1>"
                        + "<current_meter_reading1>" + curr_meter_reading1_ + "</current_meter_reading1>"
                        + "<previous_meter_reading2>" + prev_meter_reading2_ + "</previous_meter_reading2>"
                        + "<current_meter_reading2>" + curr_meter_reading2_ + "</current_meter_reading2>"
                        + "<previous_meter_reading3>" + prev_meter_reading3_ + "</previous_meter_reading3>"
                        + "<current_meter_reading3>" + curr_meter_reading3_ + "</current_meter_reading3>"
                        + "</bill>";

                blth_summary += ", " + getMonthName(bill_period_.substring(4)) + bill_period_.substring(2, 4);
                sm_last = curr_meter_reading1_;

                total_bk += Double.parseDouble(penalty_fee_);
                total_tag += Double.parseDouble(total_electricity_bill_);

            }

            if (Integer.parseInt(bill_status) > 2) {
                hasil += "<bill>"
                        + "<bill_period>" + bill_period__ + "</bill_period>"
                        + "<due_date>" + due_date__ + "</due_date>"
                        + "<meter_read_date>" + meter_read_date__ + "</meter_read_date>"
                        + "<total_electricity_bill>" + total_electricity_bill__ + "</total_electricity_bill>"
                        + "<incentive>" + incentive__ + "</incentive>"
                        + "<value_added_tax>" + vat__ + "</value_added_tax>"
                        + "<penalty_fee>" + penalty_fee__ + "</penalty_fee>"
                        + "<previous_meter_reading1>" + prev_meter_reading1__ + "</previous_meter_reading1>"
                        + "<current_meter_reading1>" + curr_meter_reading1__ + "</current_meter_reading1>"
                        + "<previous_meter_reading2>" + prev_meter_reading2__ + "</previous_meter_reading2>"
                        + "<current_meter_reading2>" + curr_meter_reading2__ + "</current_meter_reading2>"
                        + "<previous_meter_reading3>" + prev_meter_reading3__ + "</previous_meter_reading3>"
                        + "<current_meter_reading3>" + curr_meter_reading3__ + "</current_meter_reading3>"
                        + "</bill>";

                blth_summary += ", " + getMonthName(bill_period__.substring(4)) + bill_period__.substring(2, 4);
                sm_last = curr_meter_reading1__;

                total_bk += Double.parseDouble(penalty_fee__);
                total_tag += Double.parseDouble(total_electricity_bill__);
            }

            if (Integer.parseInt(bill_status) > 3) {
                hasil += "<bill>"
                        + "<bill_period>" + bill_period___ + "</bill_period>"
                        + "<due_date>" + due_date___ + "</due_date>"
                        + "<meter_read_date>" + meter_read_date___ + "</meter_read_date>"
                        + "<total_electricity_bill>" + total_electricity_bill___ + "</total_electricity_bill>"
                        + "<incentive>" + incentive___ + "</incentive>"
                        + "<value_added_tax>" + vat___ + "</value_added_tax>"
                        + "<penalty_fee>" + penalty_fee___ + "</penalty_fee>"
                        + "<previous_meter_reading1>" + prev_meter_reading1___ + "</previous_meter_reading1>"
                        + "<current_meter_reading1>" + curr_meter_reading1___ + "</current_meter_reading1>"
                        + "<previous_meter_reading2>" + prev_meter_reading2___ + "</previous_meter_reading2>"
                        + "<current_meter_reading2>" + curr_meter_reading2___ + "</current_meter_reading2>"
                        + "<previous_meter_reading3>" + prev_meter_reading3___ + "</previous_meter_reading3>"
                        + "<current_meter_reading3>" + curr_meter_reading3___ + "</current_meter_reading3>"
                        + "</bill>";

                blth_summary += ", " + getMonthName(bill_period___.substring(4)) + bill_period___.substring(2, 4);
                sm_last = curr_meter_reading1___;

                total_bk += Double.parseDouble(penalty_fee___);
                total_tag += Double.parseDouble(total_electricity_bill___);
            }

            hasil += "</bills>"
                    + "<blth_summary>" + blth_summary + "</blth_summary>"
                    + "<stand_meter_summary>" + stand_meter_summary + " - " + sm_last + "</stand_meter_summary>"
                    + "<bk>" + (long) total_bk + "</bk>"
                    + "<rptag>" + (long) total_tag + "</rptag>"
                    + "<saldo>" + detail[10] + "</saldo>"
                    + "<harga>" + (long) harga + "</harga>"
                    + "</response>";

            //simpan data payment ke database
            logger.log(Level.INFO, "Save to Postpaid Trx");

            double total_vat = Double.parseDouble(vat) + Double.parseDouble(vat_)
                    + Double.parseDouble(vat__) + Double.parseDouble(vat___);

            double total_penalty = Double.parseDouble(penalty_fee) + Double.parseDouble(penalty_fee_)
                    + Double.parseDouble(penalty_fee__) + Double.parseDouble(penalty_fee___);

            if (((!result[0].equalsIgnoreCase("2400") || !result[0].equalsIgnoreCase("2410") || !result[0].equalsIgnoreCase("2411"))
                    && !result[9].equalsIgnoreCase("0000"))
                    || (result[0].equalsIgnoreCase("2210") && result[9].equalsIgnoreCase("0000"))) {
                long idx = insertToTrx(Integer.parseInt(detail[5]), Integer.parseInt(detail[7]), detail[4], detail[1],
                        detail[6], Long.parseLong(result[2].substring(4)) + (Long.parseLong(detail[9]) * Integer.parseInt(bill_status)), Long.parseLong(result[2].substring(4)), Integer.parseInt(detail[11]),
                        result[9], "MULTI-FINANCE", Long.parseLong("0"), Short.parseShort("0"),
                        Short.parseShort("0"), switcher_ref, detail[4], Short.parseShort("0"));

                insertToDB(Integer.parseInt(detail[11]), String.valueOf(idx), "", "", result[9], subscriber_id,
                        subscriber_name, subscriber_segmentation + " / " + power_consuming_category,
                        bill_status, outstanding_bill, String.valueOf(Long.parseLong(result[2].substring(4)) + Long.parseLong(detail[9]) * Integer.parseInt(bill_status)),
                        penalty_fee, total_admin_charge,
                        incentive, blth_summary, stand_meter_summary + " - " + sm_last,
                        switcher_ref, result[12], "", "", "", service_unit_phone, hasil, iso,
                        result[7], result[6], String.valueOf(total_tag + total_bk),
                        String.valueOf(total_vat),
                        String.valueOf(total_penalty),
                        result[10], result[4]
                );
                //simpan data transaksi                   
            }
        } else if (result[9].equalsIgnoreCase("0005") || result[9].equalsIgnoreCase("0012") || result[9].equalsIgnoreCase("0063") || result[9].equalsIgnoreCase("0094")) {
            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"
                    + "<produk>MULTI-FINANCE</produk>"
                    + "<code>" + result[9] + "</code>"
                    + "<stan>" + result[3] + "</stan>"
                    + "<desc>TRANSAKSI SEDANG DIPROSES</desc>"
                    + "<saldo>" + detail[10] + "</saldo>"
                    + "</response>";
        } else {

            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"
                    + "<produk>MULTI-FINANCE</produk>"
                    + "<code>" + result[9] + "</code>"
                    + "<stan>" + result[3] + "</stan>"
                    + "<desc>" + postRC.getPurchaseMsgResponseName(result[9]) + "</desc>"
                    + "<saldo>" + detail[10] + "</saldo>"
                    + "</response>";
        }

        return hasil;
    }

    public String getMonthName(String x) {
        String bulan = "";

        if (x.equalsIgnoreCase("01")) {
            bulan = "JAN";
        } else if (x.equalsIgnoreCase("02")) {
            bulan = "FEB";
        } else if (x.equalsIgnoreCase("03")) {
            bulan = "MAR";
        } else if (x.equalsIgnoreCase("04")) {
            bulan = "APR";
        } else if (x.equalsIgnoreCase("05")) {
            bulan = "MEI";
        } else if (x.equalsIgnoreCase("06")) {
            bulan = "JUN";
        } else if (x.equalsIgnoreCase("07")) {
            bulan = "JUL";
        } else if (x.equalsIgnoreCase("08")) {
            bulan = "AUG";
        } else if (x.equalsIgnoreCase("09")) {
            bulan = "SEP";
        } else if (x.equalsIgnoreCase("10")) {
            bulan = "OKT";
        } else if (x.equalsIgnoreCase("11")) {
            bulan = "NOV";
        } else if (x.equalsIgnoreCase("12")) {
            bulan = "DES";
        }

        return bulan;
    }

    public Long insertToTrx(
            int user_id,
            int product_id,
            String destination,
            String sender,
            String sender_type,
            long price_sell,
            long price_buy,
            int inbox_id,
            String status,
            String remark,
            long saldo_awal,
            short counter,
            short counter2,
            String sn, String receiver,
            short resend) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();
        long idx = 0;

        try {
            Transactions trx = new Transactions();
            em.getTransaction().begin();

            trx.setUserId(user_id);
            trx.setProductId(product_id);
            trx.setDestination(destination);
            trx.setSender(sender);
            trx.setSenderType(sender_type);
            trx.setPriceSell(price_sell);
            trx.setPriceBuy(price_buy);
            trx.setInboxId(inbox_id);
            trx.setStatus(status);
            trx.setRemark(remark);
            trx.setSaldoAwal(saldo_awal);
            trx.setCounter(counter);
            trx.setCounter2(counter2);
            trx.setSn(sn);
            trx.setReceiver(receiver);
            trx.setResend(resend);
            trx.setCreateDate(new Date());

            em.persist(trx);
            em.flush();
            idx = trx.getId();

            em.getTransaction().commit();

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

//            idx = insertToTrx(user_id, product_id, destination, sender,
//                  sender_type, price_sell,price_buy, inbox_id,
//                  status, remark, saldo_awal, counter,
//                  counter2, sn, receiver, resend);
        } finally {
            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
        }

        return idx;
    }

    public void insertToDB(
            int inbox_id, String transid, String partnertid, String restype, String rescode, String idpel, String nama,
            String kwh, String jumlahrek, String outstanding, String tagihan, String denda, String admin, String insentive,
            String bulan, String mlalu, String refnbr, String info, String saldo, String create_date, String destination,
            String telpon, String xml, String iso,
            String ca_id, String merchant, String total_electricity_bill, String ppn,
            String penalty, String terminal_id, String dt_trx
    ) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        try {
            PlnPostpaids post = new PlnPostpaids();
            em.getTransaction().begin();

            //long amt_denda = Long.parseLong(denda);
            //long amt_insentive = Long.parseLong(insentive);
            //long amt_tagihan = Long.parseLong(tagihan);
            //long amt_admin = Long.parseLong(admin);
            post.setAdmin(admin);
            post.setBulan(bulan);

            post.setDenda(denda);
            post.setDestination(destination);
            post.setIdpel(idpel);
            post.setInboxId(inbox_id);
            post.setInfo(info);

            post.setInsentive(String.valueOf(insentive));
            post.setJumlahrek(jumlahrek);
            post.setKwh(kwh);
            post.setMlalu(mlalu);
            post.setNama(nama);
            post.setOutstanding(outstanding);
            post.setPartnertid(partnertid);
            post.setRefnbr(refnbr);
            post.setRescode(rescode);
            post.setTagihan(tagihan);
            post.setTransid(transid);
            post.setXml(xml);
            post.setIso(iso);
            post.setCreateDate(new Date());
            post.setTelpon(telpon);
            post.setBillPeriod(bulan);
            post.setCaid(ca_id);
            post.setMerchant(merchant);
            post.setTotalElectricityBill(total_electricity_bill);
            post.setPpn(ppn);
            post.setPenalty(penalty);
            post.setTerminalId(terminal_id);
            post.setDtTrx(dt_trx);

            em.persist(post);
            em.getTransaction().commit();

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

//            insertToDB(
//             inbox_id,  transid,  partnertid,  restype,  rescode,  idpel,  nama,
//             kwh,  jumlahrek,  outstanding,  tagihan,  denda,  admin,  insentive,
//             bulan,  mlalu,  refnbr,  info,  saldo,  create_date,  destination,
//             telpon,   xml,  iso,
//             ca_id,  merchant,  total_electricity_bill,  ppn,
//             penalty,  terminal_id,  dt_trx
//            );
        } finally {
            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
        }
    }

    public String processInquiry(String hasil, String[] detail) {

        String[] result = post.parseInquiryMsgResponse(hasil, false);

        if (result[8].equalsIgnoreCase("0000")) {

            String[] bit48 = post.parseBit48Inquiry(result[10], result[8]);

            String blth_summary = "";
            String stand_meter_summary = "", sm_last = "";

            String switcher_id = bit48[0];
            String subscriber_id = bit48[1];
            String bill_status = bit48[2];
            String outstanding_bill = bit48[3];
            String switcher_ref = bit48[4];
            String subscriber_name = bit48[5];
            String service_unit = bit48[6];
            String service_unit_phone = bit48[7];
            String subscriber_segmentation = bit48[8];
            String power_consuming_category = bit48[9];
            String total_admin_charge = String.valueOf(Integer.parseInt(detail[9]) * Integer.parseInt(bill_status));

            //1 bill
            String bill_period = bit48[11];
            String due_date = bit48[12];
            String meter_read_date = bit48[13];
            String total_electricity_bill = bit48[14];
            String incentive = bit48[15];
            String vat = bit48[16];
            String penalty_fee = bit48[17];
            String prev_meter_reading1 = bit48[18];
            String curr_meter_reading1 = bit48[19];
            String prev_meter_reading2 = bit48[20];
            String curr_meter_reading2 = bit48[21];
            String prev_meter_reading3 = bit48[22];
            String curr_meter_reading3 = bit48[23];

            String bill_period_ = "", bill_period__ = "", bill_period___ = "";
            String due_date_ = "", due_date__ = "", due_date___ = "";
            String meter_read_date_ = "", meter_read_date__ = "", meter_read_date___ = "";
            String total_electricity_bill_ = "", total_electricity_bill__ = "", total_electricity_bill___ = "";
            String incentive_ = "", incentive__ = "", incentive___ = "";
            String vat_ = "", vat__ = "", vat___ = "";
            String penalty_fee_ = "", penalty_fee__ = "", penalty_fee___ = "";
            String prev_meter_reading1_ = "", prev_meter_reading1__ = "", prev_meter_reading1___ = "";
            String curr_meter_reading1_ = "", curr_meter_reading1__ = "", curr_meter_reading1___ = "";
            String prev_meter_reading2_ = "", prev_meter_reading2__ = "", prev_meter_reading2___ = "";
            String curr_meter_reading2_ = "", curr_meter_reading2__ = "", curr_meter_reading2___ = "";
            String prev_meter_reading3_ = "", prev_meter_reading3__ = "", prev_meter_reading3___ = "";
            String curr_meter_reading3_ = "", curr_meter_reading3__ = "", curr_meter_reading3___ = "";

            if (Integer.parseInt(bill_status) > 1) {
                //2 bill
                bill_period_ = bit48[11 + 13];
                due_date_ = bit48[12 + 13];
                meter_read_date_ = bit48[13 + 13];
                total_electricity_bill_ = bit48[14 + 13];
                incentive_ = bit48[15 + 13];
                vat_ = bit48[16 + 13];
                penalty_fee_ = bit48[17 + 13];
                prev_meter_reading1_ = bit48[18 + 13];
                curr_meter_reading1_ = bit48[19 + 13];
                prev_meter_reading2_ = bit48[20 + 13];
                curr_meter_reading2_ = bit48[21 + 13];
                prev_meter_reading3_ = bit48[22 + 13];
                curr_meter_reading3_ = bit48[23 + 13];
            }

            if (Integer.parseInt(bill_status) > 2) {
                //3 bill
                bill_period__ = bit48[11 + (13 * 2)];
                due_date__ = bit48[12 + (13 * 2)];
                meter_read_date__ = bit48[13 + (13 * 2)];
                total_electricity_bill__ = bit48[14 + (13 * 2)];
                incentive__ = bit48[15 + (13 * 2)];
                vat__ = bit48[16 + (13 * 2)];
                penalty_fee__ = bit48[17 + (13 * 2)];
                prev_meter_reading1__ = bit48[18 + (13 * 2)];
                curr_meter_reading1__ = bit48[19 + (13 * 2)];
                prev_meter_reading2__ = bit48[20 + (13 * 2)];
                curr_meter_reading2__ = bit48[21 + (13 * 2)];
                prev_meter_reading3__ = bit48[22 + (13 * 2)];
                curr_meter_reading3__ = bit48[23 + (13 * 2)];
            }

            if (Integer.parseInt(bill_status) > 3) {
                //4 bill
                bill_period___ = bit48[11 + (13 * 3)];
                due_date___ = bit48[12 + (13 * 3)];
                meter_read_date___ = bit48[13 + (13 * 3)];
                total_electricity_bill___ = bit48[14 + (13 * 3)];
                incentive___ = bit48[15 + (13 * 3)];
                vat___ = bit48[16 + (13 * 3)];
                penalty_fee___ = bit48[17 + (13 * 3)];
                prev_meter_reading1___ = bit48[18 + (13 * 3)];
                curr_meter_reading1___ = bit48[19 + (13 * 3)];
                prev_meter_reading2___ = bit48[20 + (13 * 3)];
                curr_meter_reading2___ = bit48[21 + (13 * 3)];
                prev_meter_reading3___ = bit48[22 + (13 * 3)];
                curr_meter_reading3___ = bit48[23 + (13 * 3)];
            }

            //amount currency + minor desimal + amount
            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"
                    + "<produk>MULTI-FINANCE</produk>";

            if (result[0].equalsIgnoreCase("2400") || result[0].equalsIgnoreCase("2410")) {
                hasil += "<msg_type>REVERSAL</msg_type>";
            }

            //subscriber_name = subscriber_name.replaceAll("<", "&lt;");
            //subscriber_name = subscriber_name.replaceAll(">", "&gt;");
            subscriber_name = StringEscapeUtils.escapeXml(subscriber_name);

            hasil += "<amount>" + (Long.parseLong(result[2].substring(4)) + (Integer.parseInt(detail[9]) * Integer.parseInt(bill_status))) + "</amount>"
                    + "<stan>" + result[3] + "</stan>"
                    + "<datetime>" + result[4] + "</datetime>"
                    + "<merchant_code>" + result[5] + "</merchant_code>"
                    + "<bank_code>" + result[6] + "</bank_code>"
                    + "<rc>" + result[8] + "</rc>"
                    + "<terminal_id>" + result[9] + "</terminal_id>"
                    + "<subscriber_id>" + subscriber_id + "</subscriber_id>"
                    + "<bill_status>" + bill_status + "</bill_status>"
                    + "<outstanding_bill>" + outstanding_bill + "</outstanding_bill>"
                    + "<switcher_refno>" + switcher_ref + "</switcher_refno>"
                    + "<subscriber_name>" + subscriber_name + "</subscriber_name>"
                    + "<service_unit>" + service_unit + "</service_unit>"
                    + "<service_unit_phone>" + service_unit_phone + "</service_unit_phone>"
                    + "<subscriber_segmentation>" + subscriber_segmentation + "</subscriber_segmentation>"
                    + "<power>" + power_consuming_category + "</power>"
                    + "<admin_charge>" + total_admin_charge + "</admin_charge>"
                    + "<bills>"
                    + "<bill>"
                    + "<bill_period>" + bill_period + "</bill_period>"
                    + "<due_date>" + due_date + "</due_date>"
                    + "<meter_read_date>" + meter_read_date + "</meter_read_date>"
                    + "<total_electricity_bill>" + total_electricity_bill + "</total_electricity_bill>"
                    + "<incentive>" + incentive + "</incentive>"
                    + "<value_added_tax>" + vat + "</value_added_tax>"
                    + "<penalty_fee>" + penalty_fee + "</penalty_fee>"
                    + "<previous_meter_reading1>" + prev_meter_reading1 + "</previous_meter_reading1>"
                    + "<current_meter_reading1>" + curr_meter_reading1 + "</current_meter_reading1>"
                    + "<previous_meter_reading2>" + prev_meter_reading2 + "</previous_meter_reading2>"
                    + "<current_meter_reading2>" + curr_meter_reading2 + "</current_meter_reading2>"
                    + "<previous_meter_reading3>" + prev_meter_reading3 + "</previous_meter_reading3>"
                    + "<current_meter_reading3>" + curr_meter_reading3 + "</current_meter_reading3>"
                    + "</bill>";

            blth_summary += getMonthName(bill_period.substring(4)) + bill_period.substring(2, 4);
            stand_meter_summary += prev_meter_reading1;
            sm_last = curr_meter_reading1;

            if (Integer.parseInt(bill_status) > 1) {

                hasil += "<bill>"
                        + "<bill_period>" + bill_period_ + "</bill_period>"
                        + "<due_date>" + due_date_ + "</due_date>"
                        + "<meter_read_date>" + meter_read_date_ + "</meter_read_date>"
                        + "<total_electricity_bill>" + total_electricity_bill_ + "</total_electricity_bill>"
                        + "<incentive>" + incentive_ + "</incentive>"
                        + "<value_added_tax>" + vat_ + "</value_added_tax>"
                        + "<penalty_fee>" + penalty_fee_ + "</penalty_fee>"
                        + "<previous_meter_reading1>" + prev_meter_reading1_ + "</previous_meter_reading1>"
                        + "<current_meter_reading1>" + curr_meter_reading1_ + "</current_meter_reading1>"
                        + "<previous_meter_reading2>" + prev_meter_reading2_ + "</previous_meter_reading2>"
                        + "<current_meter_reading2>" + curr_meter_reading2_ + "</current_meter_reading2>"
                        + "<previous_meter_reading3>" + prev_meter_reading3_ + "</previous_meter_reading3>"
                        + "<current_meter_reading3>" + curr_meter_reading3_ + "</current_meter_reading3>"
                        + "</bill>";

                blth_summary += ", " + getMonthName(bill_period_.substring(4)) + bill_period_.substring(2, 4);
                sm_last = curr_meter_reading1_;

            }

            if (Integer.parseInt(bill_status) > 2) {
                hasil += "<bill>"
                        + "<bill_period>" + bill_period__ + "</bill_period>"
                        + "<due_date>" + due_date__ + "</due_date>"
                        + "<meter_read_date>" + meter_read_date__ + "</meter_read_date>"
                        + "<total_electricity_bill>" + total_electricity_bill__ + "</total_electricity_bill>"
                        + "<incentive>" + incentive__ + "</incentive>"
                        + "<value_added_tax>" + vat__ + "</value_added_tax>"
                        + "<penalty_fee>" + penalty_fee__ + "</penalty_fee>"
                        + "<previous_meter_reading1>" + prev_meter_reading1__ + "</previous_meter_reading1>"
                        + "<current_meter_reading1>" + curr_meter_reading1__ + "</current_meter_reading1>"
                        + "<previous_meter_reading2>" + prev_meter_reading2__ + "</previous_meter_reading2>"
                        + "<current_meter_reading2>" + curr_meter_reading2__ + "</current_meter_reading2>"
                        + "<previous_meter_reading3>" + prev_meter_reading3__ + "</previous_meter_reading3>"
                        + "<current_meter_reading3>" + curr_meter_reading3__ + "</current_meter_reading3>"
                        + "</bill>";

                blth_summary += ", " + getMonthName(bill_period__.substring(4)) + bill_period__.substring(2, 4);
                sm_last = curr_meter_reading1__;
            }

            if (Integer.parseInt(bill_status) > 3) {
                hasil += "<bill>"
                        + "<bill_period>" + bill_period___ + "</bill_period>"
                        + "<due_date>" + due_date___ + "</due_date>"
                        + "<meter_read_date>" + meter_read_date___ + "</meter_read_date>"
                        + "<total_electricity_bill>" + total_electricity_bill___ + "</total_electricity_bill>"
                        + "<incentive>" + incentive___ + "</incentive>"
                        + "<value_added_tax>" + vat___ + "</value_added_tax>"
                        + "<penalty_fee>" + penalty_fee___ + "</penalty_fee>"
                        + "<previous_meter_reading1>" + prev_meter_reading1___ + "</previous_meter_reading1>"
                        + "<current_meter_reading1>" + curr_meter_reading1___ + "</current_meter_reading1>"
                        + "<previous_meter_reading2>" + prev_meter_reading2___ + "</previous_meter_reading2>"
                        + "<current_meter_reading2>" + curr_meter_reading2___ + "</current_meter_reading2>"
                        + "<previous_meter_reading3>" + prev_meter_reading3___ + "</previous_meter_reading3>"
                        + "<current_meter_reading3>" + curr_meter_reading3___ + "</current_meter_reading3>"
                        + "</bill>";

                blth_summary += ", " + getMonthName(bill_period___.substring(4)) + bill_period___.substring(2, 4);
                sm_last = curr_meter_reading1___;
            }

            stand_meter_summary += " - " + sm_last;

            result[10] = StringEscapeUtils.escapeXml(result[10]);

            hasil += "</bills>"
                    + "<blth_summary>" + blth_summary + "</blth_summary>"
                    + "<stand_meter_summary>" + stand_meter_summary + "</stand_meter_summary>"
                    + "<bit48>" + result[10] + "</bit48>"
                    + "</response>";

        } else {
            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"
                    + "<stan>" + result[3] + "</stan>"
                    + "<code>" + result[8] + "</code>"
                    + "<produk>MULTI-FINANCE</produk>"
                    + "<desc>" + postRC.getInquiryMsgResponseName(result[8]) + "</desc>"
                    + "</response>";
        }

        return hasil;
    }

    public String processReversal(String hasil, String[] detail) {

        String[] result = post.parsePaymentMsgResponse(hasil, false);

        if (result[9].equalsIgnoreCase("0000") || result[9].equalsIgnoreCase("0012")) {

            String[] bit48 = post.parseBit48Payment(result[11], result[9]);

            String switcher_id = bit48[0];
            String subscriber_id = bit48[1];
            String bill_status = bit48[2];
            String payment_status = bit48[2];
            String outstanding_bill = bit48[3];
            String switcher_ref = bit48[4];
            String subscriber_name = bit48[5];
            String service_unit = bit48[6];
            String service_unit_phone = bit48[7];
            String subscriber_segmentation = bit48[8];
            String power_consuming_category = bit48[9];
            String total_admin_charge = String.valueOf(Integer.parseInt(detail[9]) * Integer.parseInt(bill_status));

            //1 bill
            String bill_period = bit48[11];
            String due_date = bit48[12];
            String meter_read_date = bit48[13];
            String total_electricity_bill = bit48[14];
            String incentive = bit48[15];
            String vat = bit48[16];
            String penalty_fee = bit48[17];
            String prev_meter_reading1 = bit48[18];
            String curr_meter_reading1 = bit48[19];
            String prev_meter_reading2 = bit48[20];
            String curr_meter_reading2 = bit48[21];
            String prev_meter_reading3 = bit48[22];
            String curr_meter_reading3 = bit48[23];

            String bill_period_ = "", bill_period__ = "", bill_period___ = "";
            String due_date_ = "", due_date__ = "", due_date___ = "";
            String meter_read_date_ = "", meter_read_date__ = "", meter_read_date___ = "";
            String total_electricity_bill_ = "", total_electricity_bill__ = "", total_electricity_bill___ = "";
            String incentive_ = "", incentive__ = "", incentive___ = "";
            String vat_ = "", vat__ = "", vat___ = "";
            String penalty_fee_ = "", penalty_fee__ = "", penalty_fee___ = "";
            String prev_meter_reading1_ = "", prev_meter_reading1__ = "", prev_meter_reading1___ = "";
            String curr_meter_reading1_ = "", curr_meter_reading1__ = "", curr_meter_reading1___ = "";
            String prev_meter_reading2_ = "", prev_meter_reading2__ = "", prev_meter_reading2___ = "";
            String curr_meter_reading2_ = "", curr_meter_reading2__ = "", curr_meter_reading2___ = "";
            String prev_meter_reading3_ = "", prev_meter_reading3__ = "", prev_meter_reading3___ = "";
            String curr_meter_reading3_ = "", curr_meter_reading3__ = "", curr_meter_reading3___ = "";

            if (Integer.parseInt(bill_status) > 1) {
                //2 bill
                bill_period_ = bit48[11 + 13];
                due_date_ = bit48[12 + 13];
                meter_read_date_ = bit48[13 + 13];
                total_electricity_bill_ = bit48[14 + 13];
                incentive_ = bit48[15 + 13];
                vat_ = bit48[16 + 13];
                penalty_fee_ = bit48[17 + 13];
                prev_meter_reading1_ = bit48[18 + 13];
                curr_meter_reading1_ = bit48[19 + 13];
                prev_meter_reading2_ = bit48[20 + 13];
                curr_meter_reading2_ = bit48[21 + 13];
                prev_meter_reading3_ = bit48[22 + 13];
                curr_meter_reading3_ = bit48[23 + 13];
            }

            if (Integer.parseInt(bill_status) > 2) {
                //3 bill
                bill_period__ = bit48[11 + (13 * 2)];
                due_date__ = bit48[12 + (13 * 2)];
                meter_read_date__ = bit48[13 + (13 * 2)];
                total_electricity_bill__ = bit48[14 + (13 * 2)];
                incentive__ = bit48[15 + (13 * 2)];
                vat__ = bit48[16 + (13 * 2)];
                penalty_fee__ = bit48[17 + (13 * 2)];
                prev_meter_reading1__ = bit48[18 + (13 * 2)];
                curr_meter_reading1__ = bit48[19 + (13 * 2)];
                prev_meter_reading2__ = bit48[20 + (13 * 2)];
                curr_meter_reading2__ = bit48[21 + (13 * 2)];
                prev_meter_reading3__ = bit48[22 + (13 * 2)];
                curr_meter_reading3__ = bit48[23 + (13 * 2)];
            }

            if (Integer.parseInt(bill_status) > 3) {
                //4 bill
                bill_period___ = bit48[11 + (13 * 3)];
                due_date___ = bit48[12 + (13 * 3)];
                meter_read_date___ = bit48[13 + (13 * 3)];
                total_electricity_bill___ = bit48[14 + (13 * 3)];
                incentive___ = bit48[15 + (13 * 3)];
                vat___ = bit48[16 + (13 * 3)];
                penalty_fee___ = bit48[17 + (13 * 3)];
                prev_meter_reading1___ = bit48[18 + (13 * 3)];
                curr_meter_reading1___ = bit48[19 + (13 * 3)];
                prev_meter_reading2___ = bit48[20 + (13 * 3)];
                curr_meter_reading2___ = bit48[21 + (13 * 3)];
                prev_meter_reading3___ = bit48[22 + (13 * 3)];
                curr_meter_reading3___ = bit48[23 + (13 * 3)];
            }

            //subscriber_name = subscriber_name.replaceAll("<", "&lt;");
            //subscriber_name = subscriber_name.replaceAll(">", "&gt;");
            subscriber_name = StringEscapeUtils.escapeXml(subscriber_name);

            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"
                    + "<produk>MULTI-FINANCE</produk>"
                    + "<msg_type>REVERSAL</msgtype>"
                    + "<amount>" + (Long.parseLong(result[2].substring(4)) + (Integer.parseInt(detail[9]) * Integer.parseInt(bill_status))) + "</amount>"
                    + "<stan>" + result[3] + "</stan>"
                    + "<datetime>" + result[4] + "</datetime>"
                    + "<date_settlement>" + result[5] + "</date_settlement>"
                    + "<merchant_code>" + result[6] + "</merchant_code>"
                    + "<bank_code>" + result[7] + "</bank_code>"
                    + "<rc>" + result[9] + "</rc>"
                    + "<terminal_id>" + result[10] + "</terminal_id>"
                    + "<bill_status>" + bill_status + "</bill_status>"
                    + "<payment_status>" + result[7] + "</payment_status>"
                    + "<subscriber_id>" + subscriber_id + "</subscriber_id>"
                    + "<switcher_refno>" + switcher_ref + "</switcher_refno>"
                    + "<subscriber_name>" + subscriber_name + "</subscriber_name>"
                    + "<service_unit>" + service_unit + "</service_unit>"
                    + "<service_unit_phone>" + service_unit_phone + "</service_unit_phone>"
                    + "<subscriber_segmentation>" + subscriber_segmentation + "</subscriber_segmentation>"
                    + "<power>" + power_consuming_category + "</power>"
                    + "<admin_charge>" + total_admin_charge + "</admin_charge>"
                    + "<info_text>" + result[12] + "</admin_charge>"
                    + "<bills>"
                    + "<bill>"
                    + "<bill_period>" + bill_period + "</bill_period>"
                    + "<due_date>" + due_date + "</due_date>"
                    + "<meter_read_date>" + meter_read_date + "</meter_read_date>"
                    + "<total_electricity_bill>" + total_electricity_bill + "</total_electricity_bill>"
                    + "<incentive>" + incentive + "</incentive>"
                    + "<value_added_tax>" + vat + "</value_added_tax>"
                    + "<penalty_fee>" + penalty_fee + "</penalty_fee>"
                    + "<previous_meter_reading1>" + prev_meter_reading1 + "</previous_meter_reading1>"
                    + "<current_meter_reading1>" + curr_meter_reading1 + "</current_meter_reading1>"
                    + "<previous_meter_reading2>" + prev_meter_reading2 + "</previous_meter_reading2>"
                    + "<current_meter_reading2>" + curr_meter_reading2 + "</current_meter_reading2>"
                    + "<previous_meter_reading3>" + prev_meter_reading3 + "</previous_meter_reading3>"
                    + "<current_meter_reading3>" + curr_meter_reading3 + "</current_meter_reading3>"
                    + "</bill>";

            if (Integer.parseInt(bill_status) > 1) {

                hasil += "<bill>"
                        + "<bill_period>" + bill_period_ + "</bill_period>"
                        + "<due_date>" + due_date_ + "</due_date>"
                        + "<meter_read_date>" + meter_read_date_ + "</meter_read_date>"
                        + "<total_electricity_bill>" + total_electricity_bill_ + "</total_electricity_bill>"
                        + "<incentive>" + incentive_ + "</incentive>"
                        + "<value_added_tax>" + vat_ + "</value_added_tax>"
                        + "<penalty_fee>" + penalty_fee_ + "</penalty_fee>"
                        + "<previous_meter_reading1>" + prev_meter_reading1_ + "</previous_meter_reading1>"
                        + "<current_meter_reading1>" + curr_meter_reading1_ + "</current_meter_reading1>"
                        + "<previous_meter_reading2>" + prev_meter_reading2_ + "</previous_meter_reading2>"
                        + "<current_meter_reading2>" + curr_meter_reading2_ + "</current_meter_reading2>"
                        + "<previous_meter_reading3>" + prev_meter_reading3_ + "</previous_meter_reading3>"
                        + "<current_meter_reading3>" + curr_meter_reading3_ + "</current_meter_reading3>"
                        + "</bill>";

            }

            if (Integer.parseInt(bill_status) > 2) {
                hasil += "<bill>"
                        + "<bill_period>" + bill_period__ + "</bill_period>"
                        + "<due_date>" + due_date__ + "</due_date>"
                        + "<meter_read_date>" + meter_read_date__ + "</meter_read_date>"
                        + "<total_electricity_bill>" + total_electricity_bill__ + "</total_electricity_bill>"
                        + "<incentive>" + incentive__ + "</incentive>"
                        + "<value_added_tax>" + vat__ + "</value_added_tax>"
                        + "<penalty_fee>" + penalty_fee__ + "</penalty_fee>"
                        + "<previous_meter_reading1>" + prev_meter_reading1__ + "</previous_meter_reading1>"
                        + "<current_meter_reading1>" + curr_meter_reading1__ + "</current_meter_reading1>"
                        + "<previous_meter_reading2>" + prev_meter_reading2__ + "</previous_meter_reading2>"
                        + "<current_meter_reading2>" + curr_meter_reading2__ + "</current_meter_reading2>"
                        + "<previous_meter_reading3>" + prev_meter_reading3__ + "</previous_meter_reading3>"
                        + "<current_meter_reading3>" + curr_meter_reading3__ + "</current_meter_reading3>"
                        + "</bill>";
            }

            if (Integer.parseInt(bill_status) > 3) {
                hasil += "<bill>"
                        + "<bill_period>" + bill_period___ + "</bill_period>"
                        + "<due_date>" + due_date___ + "</due_date>"
                        + "<meter_read_date>" + meter_read_date___ + "</meter_read_date>"
                        + "<total_electricity_bill>" + total_electricity_bill___ + "</total_electricity_bill>"
                        + "<incentive>" + incentive___ + "</incentive>"
                        + "<value_added_tax>" + vat___ + "</value_added_tax>"
                        + "<penalty_fee>" + penalty_fee___ + "</penalty_fee>"
                        + "<previous_meter_reading1>" + prev_meter_reading1___ + "</previous_meter_reading1>"
                        + "<current_meter_reading1>" + curr_meter_reading1___ + "</current_meter_reading1>"
                        + "<previous_meter_reading2>" + prev_meter_reading2___ + "</previous_meter_reading2>"
                        + "<current_meter_reading2>" + curr_meter_reading2___ + "</current_meter_reading2>"
                        + "<previous_meter_reading3>" + prev_meter_reading3___ + "</previous_meter_reading3>"
                        + "<current_meter_reading3>" + curr_meter_reading3___ + "</current_meter_reading3>"
                        + "</bill>";
            }

            hasil += "</bills>"
                    + "</response>";

        } else {

            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"
                    + "<stan>" + result[3] + "</stan>"
                    + "<produk>MULTI-FINANCE</produk>"
                    + "<code>" + result[9] + "</code>"
                    + "<desc>" + postRC.getPurchaseMsgResponseName(result[9]) + "</desc>"
                    + "</response>";
        }

        return hasil;
    }

}
