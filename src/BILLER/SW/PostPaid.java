
package BILLER.SW;

import ppob_biller.Settings;
import org.apache.log4j.*;
import java.sql.*;

import org.jpos.iso.*;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.jpos.iso.packager.GenericPackager;
import utils.Saldo;
import model.Mutations;

import javax.persistence.*;
import model.PartnerCodes;
import model.Users;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAPersistence;

public class PostPaid {

    Settings setting = new Settings();
    SLSLogger slsLog = new SLSLogger();

    Connection conx = null;
    Connection conOtomax = null;

    Saldo mutasi = new Saldo();

    int PORT_SERVER = 12345;
    String SERVER_IP = "localhost";
    String cid = "";
    String switcher_id = "";
    String bank_code = "";
    String bit56 = "";
    int sleepTime = 0;

    private static Logger logger = Logger.getLogger("SLS.POSTPAID");

    public PostPaid() {

        setting.setConnections();

        this.PORT_SERVER = Integer.parseInt(setting.getPostPort());
        this.SERVER_IP = setting.getPostIP();
        this.sleepTime = Integer.parseInt(setting.getPostSleep());
        this.cid = setting.getSwitchingCID();
        this.switcher_id = setting.getSwitcherID();
        this.bank_code = setting.getBankCode();

    }

    public PostPaid(int user_id) {

        setting.setConnections();

        this.PORT_SERVER = Integer.parseInt(setting.getPostPort());
        this.SERVER_IP = setting.getPostIP();
        this.sleepTime = Integer.parseInt(setting.getPostSleep());

        //checking PKS
        try {
            String pks = "jpa";
            pks = this.getPks(user_id);
            File f = new File("./setting.properties");
            if (f.exists()) {
                Properties pro = new Properties();
                FileInputStream in = new FileInputStream(f);
                pro.load(in);
//                this.cid = pro.getProperty("Switching.hulu.cid." + pks.toLowerCase());
                this.cid = getSymphoniCid(pks);
                logger.log(Level.INFO, "destination cid : " + this.cid);
            } else {
                this.cid = setting.getSwitchingCID();
                logger.log(Level.INFO, "priority 2 destination cid : " + this.cid);
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.toString());
            this.cid = setting.getSwitchingCID();
            logger.log(Level.INFO, "priority 3 destination cid : " + this.cid);
        }

        this.switcher_id = setting.getSwitcherID();
        this.bank_code = setting.getBankCode();

    }

    public String getSymphoniCid(String pks) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        String key = "";

        logger.log(Level.INFO, "checking user pks " + pks);

        try {
            String sql = "select o from PartnerCodes o where o.initial=:uid";

            Query query = em.createQuery(sql);
            query.setParameter("uid", pks);
            query.setMaxResults(1);

            for (PartnerCodes m : (List<PartnerCodes>) query.getResultList()) {
                key = m.getCid();
            }

            em.close();
            factory.close();

        } catch (Exception e) {
            logger.log(Level.FATAL, e.toString());
        } finally {
            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
        }

        return key;
    }

    public String getPks(int user_id) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        String key = "";

        logger.log(Level.INFO, "checking user pks " + user_id);

        try {
            String sql = "select o from Users o where o.id=:uid";

            Query query = em.createQuery(sql);
            query.setParameter("uid", user_id);
            query.setMaxResults(1);

            for (Users m : (List<Users>) query.getResultList()) {
                key = m.getPks();
            }

            em.close();
            factory.close();

        } catch (Exception e) {
            logger.log(Level.FATAL, e.toString());
        } finally {
            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
        }

        return key;
    }

    public String request_inquiry2(String msg, String user_id, String inbox_id,
            String merchant_cat_code, String terminal_id,
            String idpel, String jumlahBayar, String[] userData
    ) {
        String hasil = "", log = "", nominal = "", id_pel = "";
        Socket client = null;
        try {
            String tgl = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            logger.log(Level.INFO, "Connecting to " + SERVER_IP + " on port " + PORT_SERVER);
            client = new Socket(SERVER_IP, PORT_SERVER);
            client.setSoTimeout(this.sleepTime);

            id_pel = idpel;
            String flag = "0", no_meter = "0", subscriber_id = "0";

            log = inquiry(id_pel, jumlahBayar, user_id, inbox_id, merchant_cat_code, terminal_id, client, tgl, userData);

            String[] inquiryResponse = parseInquiryMsgResponse(log, true);

            //cek diminta sign on lg ga
            if (inquiryResponse[8].equalsIgnoreCase("0011")) {

                log = networkMsg("1", user_id, client); //sign on
                if (!log.equalsIgnoreCase("")) {

                    logger.log(Level.INFO, log);

                    log = inquiry(id_pel, jumlahBayar, user_id, inbox_id, merchant_cat_code, terminal_id, client, tgl, userData);

                    logger.log(Level.INFO, log);

                    hasil = log;

                }

            }

            logger.log(Level.INFO, log);

            hasil = log;

            client.close();
        }  catch (Exception e) {
            logger.log(Level.INFO, "ISO RESULT : " + e.getMessage());

            try {
                GenericPackager packager = new GenericPackager("packager/iso87ascii.xml");

                ISOMsg isoMsg = new ISOMsg();
                isoMsg.setPackager(packager);

                isoMsg.setMTI("2110");
                isoMsg.set(2, "99501");
                isoMsg.set(11, String.valueOf(inbox_id));
                isoMsg.set(26, "6012");
                isoMsg.set(39, "0068");

                byte[] datax = isoMsg.pack();
                hasil = new String(datax);
                logger.log(Level.INFO, "ISO RESULT : " + hasil);

            } catch (Exception isox) {
                logger.log(Level.FATAL, "ISO : " + isox.getMessage());
            }

            //hasil = "21104000004002000000059950260120068";
            e.printStackTrace();
            logger.log(Level.FATAL, e.toString());
        } finally {
            try {
                client.close();
            } catch (Exception e) {
                logger.log(Level.FATAL, e.toString());
            }
        }

        logger.log(Level.INFO, "hasilnye : " + hasil);

        return hasil;
    }

    public void refund(double nominal, String msg, int inbox_id, int user_id, int admin, int product_id) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();
        try {

            Users user = em.find(Users.class, user_id);
            int price_template_id = user.getPriceTemplateId();

            //get Product price
            String sq = "select m.price from ProductPrices m where m.priceTemplateId=:ptid and m.productId=:pid";
            Query qqq = em.createQuery(sq);
            qqq.setParameter("pid", product_id);
            qqq.setParameter("ptid", price_template_id);
            Number priceq = (Number) qqq.getSingleResult();
            logger.log(Level.INFO, "Harga < " + inbox_id + " > : " + nominal + ", admin : " + priceq.longValue());

            long prcq = priceq.longValue();

            em.getTransaction().begin();

            Mutations mut = new Mutations();
            mut.setAmount((long) (nominal) - prcq + admin);
            mut.setNote(msg);
            mut.setJenis((char) 'K');
            //mut.setBalance(balq);
            mut.setInboxId(inbox_id);
            mut.setUserId(user_id);
            mut.setCreateDate(new java.util.Date());

            em.persist(mut);

            em.getTransaction().commit();
        } catch (Exception e) {
            logger.log(Level.FATAL, e);
            e.printStackTrace();
        }
    }

    public String bill_payment2(String msg, String trx_id, String user_id,
            String merchant_cat_code, String terminal_id, String hasilInquiry, int admin, int product_id) {

        String log = "", nominal = "0", id_pel = "", resultIso = "";
        Socket client = null;
        try {

            String tgl = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

            logger.log(Level.INFO, "Connecting to " + SERVER_IP + " on port " + PORT_SERVER);
            client = new Socket(SERVER_IP, PORT_SERVER);
            client.setSoTimeout(this.sleepTime);

            logger.log(Level.INFO, "msg : " + msg);

            String req[] = msg.split("\\.");

            id_pel = req[2];
            logger.log(Level.INFO, "ID Pel : " + id_pel);

            log = hasilInquiry;
            //log = inquiry(id_pel, user_id, trx_id, merchant_cat_code, terminal_id, client);

            if (!log.equalsIgnoreCase("")) {

                logger.log(Level.INFO, log);

                String[] inquiryResp = parseInquiryMsgResponse(log, false);
                logger.log(Level.INFO, "RC : " + inquiryResp[8]);
                if (inquiryResp[8].equalsIgnoreCase("0000")) {
                    logger.log(Level.INFO, "Entering payment...");

                    //parse bit 48
                    logger.log(Level.INFO, "bit48 : " + inquiryResp[10]);
                    String[] bit48 = parseBit48Inquiry(inquiryResp[10], inquiryResp[8]);

                    String inquiryBit48 = inquiryResp[10];

                    nominal = inquiryResp[2];

                    logger.log(Level.INFO, "Nominal : " + nominal);

                    String[] userData = new String[5];
                    log = payment(id_pel, nominal, trx_id, user_id, merchant_cat_code, terminal_id, inquiryBit48, "", client, tgl, userData);

                    resultIso = log;
                    logger.log(Level.INFO, log);

                    logger.log(Level.INFO, "Parse msg response");
                    String[] purchaseResp = parsePaymentMsgResponse(log, true);

                    if (purchaseResp[9].equalsIgnoreCase("0068") || purchaseResp[9].equalsIgnoreCase("0063") || purchaseResp[9].equalsIgnoreCase("0005") || purchaseResp[9].equalsIgnoreCase("")) {

                        logger.log(Level.INFO, "Entering advice");
//                        //delay 30 detik
//                        try { Thread.sleep(this.sleepTime);}
//                        catch(Exception e){e.printStackTrace();}

                        log = reversal(id_pel, nominal, trx_id, user_id, merchant_cat_code, terminal_id, inquiryBit48, "", client, false, tgl);
                        resultIso = log;
                        String[] adv1Resp = parseReversalMsgResponse(log, true, false);

                        if (adv1Resp[9].equalsIgnoreCase("0068") || adv1Resp[9].equalsIgnoreCase("0063") || adv1Resp[9].equalsIgnoreCase("0005") || adv1Resp[9].equalsIgnoreCase("")) {

                        } else if (adv1Resp[9].equalsIgnoreCase("0000") && !adv1Resp[0].equalsIgnoreCase("2210")) {
                            //////////////////refund the money////////////////////////////////////////////////////////////////////////
                            refund(Double.parseDouble(nominal.substring(4)), "Refund trx bpjs rev sukses ", Integer.parseInt(trx_id), Integer.parseInt(user_id), admin, product_id);
                            //////////////////////////////////////////////////////////////////////////////////////////////////////////
                        } else { //advice 1 berhasil
                            //return log;
                            resultIso = log;
                        }
                    } else if (purchaseResp[9].equalsIgnoreCase("0017")) {
                        logger.log(Level.INFO, "Refund due to saldo sds kurang");
                        //////////////////refund the money due to saldo sds kurang////////////////////////////////////////////////////////////////////////
                        refund(Double.parseDouble(nominal.substring(4)), "Refund trx bpjs saldo kurang", Integer.parseInt(trx_id), Integer.parseInt(user_id), admin, product_id);
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////
                    }
                } else {
                    //mutasi.tambahBalance(Double.parseDouble(nominal), user_id, log, trx_id, String.valueOf(mutasi.getBalance(user_id)));
                    logger.log(Level.INFO, log);
                    resultIso = log;

                    //return log;
                }

            }

            client.close();

            logger.log(Level.INFO, "Return ISO bill payment2 : " + resultIso);
            return resultIso;
        } catch (Exception e) {

            resultIso = "21104000004002000000059950260120068";

            //e.printStackTrace();
            logger.log(Level.FATAL, e.toString());

            logger.log(Level.INFO, "Refund due to " + e.getMessage());
            //////////////////refund the money due to saldo sds kurang////////////////////////////////////////////////////////////////////////
            refund(Double.parseDouble(nominal.substring(4)), "Refund trx postpaid " + e.getMessage(), Integer.parseInt(trx_id), Integer.parseInt(user_id), admin, product_id);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
        } finally {
            try {
                client.close();
            } catch (Exception e) {
                logger.log(Level.FATAL, e);
            }
        }

        return resultIso;
    }

    public String bill_payment_bpjs(String msg, String trx_id, String user_id,
            String merchant_cat_code, String terminal_id, String hasilInquiry, int admin, int product_id, String[] userData) {

        String log = "", nominal = "0", id_pel = "", resultIso = "";
        Socket client = null;
        try {

            String tgl = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

            logger.log(Level.INFO, "Connecting to " + SERVER_IP + " on port " + PORT_SERVER);
            client = new Socket(SERVER_IP, PORT_SERVER);
            client.setSoTimeout(this.sleepTime);

            logger.log(Level.INFO, "msg : " + msg);

            String req[] = msg.split("\\.");

            id_pel = req[2];
            logger.log(Level.INFO, "ID Pel : " + id_pel);

            log = hasilInquiry;

            if (!log.equalsIgnoreCase("")) {

                logger.log(Level.INFO, log);

                String[] inquiryResp = parseInquiryMsgResponse(log, false);
                logger.log(Level.INFO, "RC : " + inquiryResp[8]);
                if (inquiryResp[8].equalsIgnoreCase("0000")) {
                    logger.log(Level.INFO, "Entering payment...");

                    //parse bit 48
                    logger.log(Level.INFO, "bit48 : " + inquiryResp[10]);

                    String inquiryBit48 = inquiryResp[10];
                    nominal = inquiryResp[2];

                    logger.log(Level.INFO, "Nominal : " + nominal);
                    tgl = inquiryResp[4];
                    logger.log(Level.INFO, "tgl : " + tgl);

                    log = payment(id_pel, nominal, trx_id, user_id, merchant_cat_code, terminal_id, inquiryBit48, "", client, tgl, userData);

                    resultIso = log;
                    logger.log(Level.INFO, log);

                    logger.log(Level.INFO, "Parse msg response");
                    String[] purchaseResp = parsePaymentMsgResponse(log, true);

                    int counter = 3;
                    for (int i = 0; i < counter; i++) {
                        //note BPJS tidak ada reversal - hanya ada advice seperti prepaid
                        logger.info("LOOP " + i + "   ::   RC : " + purchaseResp[9]);
                        if (purchaseResp[9].equalsIgnoreCase("0068") || purchaseResp[9].equalsIgnoreCase("0063")
                                || purchaseResp[9].equalsIgnoreCase("0005") || purchaseResp[9].equalsIgnoreCase("")) {

                            logger.log(Level.INFO, "Entering advice " + i);

                            log = adviceBpjs(id_pel, nominal, trx_id, user_id, merchant_cat_code, terminal_id, inquiryBit48, "", client, tgl, userData, i);

                            resultIso = log;

                            purchaseResp = parsePaymentMsgResponse(log, true);

                            if (!purchaseResp[9].equalsIgnoreCase("0000") && !purchaseResp[0].equalsIgnoreCase("2210")) {
                                logger.info("ADVICE GAGAL, RC :: " + purchaseResp[9] + ", REFUND TRANSAKSI BPJS");
                                
                            } else { //advice 1 berhasil
                                //return log;
                                logger.info("ADVICE BERHASIL, RC :: " + purchaseResp[9]);
                                resultIso = log;
                            }
//                            counter++;
                        } else if (purchaseResp[9].equalsIgnoreCase("0017")
                                || purchaseResp[9].equalsIgnoreCase("0076")
                                || purchaseResp[9].equalsIgnoreCase("0046")) {

                            logger.log(Level.INFO, "ADVICE LOOP TIDAK DIJALANKAN");
                            logger.log(Level.INFO, "REFUND KARENA SALDO TIDAK MENCUKUPI");
                            refund(Double.parseDouble(nominal.substring(4)), "Refund trx bpjs saldo kurang", Integer.parseInt(trx_id), Integer.parseInt(user_id), admin, product_id);
                            i = counter;
                        }

                    }

                } else {
                    
                    logger.log(Level.INFO, log);
                    resultIso = log;

                }

            }

            client.close();

            logger.log(Level.INFO, "Return ISO bill payment2 : " + resultIso);
            return resultIso;
        } catch (Exception e) {

            resultIso = "21104000004002000000059950260120068";

            //e.printStackTrace();
            logger.log(Level.FATAL, e.toString());

            logger.log(Level.INFO, "Refund due to " + e.getMessage());
           
        } finally {
            try {
                client.close();
            } catch (Exception e) {
                logger.log(Level.FATAL, e);
            }
        }

        return resultIso;
    }

    public String bill_advice_bpjs(String msg, String trx_id, String user_id,
            String merchant_cat_code, String terminal_id, String hasilInquiry, int admin, int product_id, String[] userData) {

        String log = "", nominal = "0", id_pel = "", resultIso = "";
        Socket client = null;
        try {

            String tgl = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

            logger.log(Level.INFO, "Connecting to " + SERVER_IP + " on port " + PORT_SERVER);
            client = new Socket(SERVER_IP, PORT_SERVER);
            client.setSoTimeout(this.sleepTime);

            logger.log(Level.INFO, "msg : " + msg);

            String req[] = msg.split("\\.");

            id_pel = req[2];
            logger.log(Level.INFO, "ID Pel : " + id_pel);

            log = hasilInquiry;
            //log = inquiry(id_pel, user_id, trx_id, merchant_cat_code, terminal_id, client);

            if (!log.equalsIgnoreCase("")) {

                logger.log(Level.INFO, log);

                String[] inquiryResp = parseInquiryMsgResponse(log, false);
                logger.log(Level.INFO, "RC : " + inquiryResp[8]);
                if (inquiryResp[8].equalsIgnoreCase("0000")) {
                    logger.log(Level.INFO, "Entering payment...");

                    //parse bit 48
                    logger.log(Level.INFO, "bit48 : " + inquiryResp[10]);
                    String[] bit48 = parseBit48Inquiry(inquiryResp[10], inquiryResp[8]);

                    String inquiryBit48 = inquiryResp[10];
                    nominal = inquiryResp[2];

                    logger.log(Level.INFO, "Nominal : " + nominal);
                    tgl = inquiryResp[4];
                    logger.log(Level.INFO, "tgl : " + tgl);

                    resultIso = log;

                    logger.log(Level.INFO, "Entering advice");

                    log = adviceBpjs(id_pel, nominal, trx_id, user_id, merchant_cat_code, terminal_id, inquiryBit48, "", client, tgl, userData, 4);

                    resultIso = log;

                    String[] adv1Resp = parsePaymentMsgResponse(log, true);

                    if (!adv1Resp[9].equalsIgnoreCase("0000") && !adv1Resp[0].equalsIgnoreCase("2210")) {

                    } else { //advice 1 berhasil
                        //return log;
                        logger.info("ADVICE BERHASIL, RC :: " + adv1Resp[9]);
                        resultIso = log;
                    }

                } else {
                    //mutasi.tambahBalance(Double.parseDouble(nominal), user_id, log, trx_id, String.valueOf(mutasi.getBalance(user_id)));
                    logger.log(Level.INFO, log);
                    resultIso = log;

                    //return log;
                }

            }

            client.close();

            logger.log(Level.INFO, "Return ISO bill payment2 : " + resultIso);
            return resultIso;
        } catch (Exception e) {

            resultIso = "21104000004002000000059950260120068";

            //e.printStackTrace();
            logger.log(Level.FATAL, e.toString());

            logger.log(Level.INFO, "Refund due to " + e.getMessage());
            //////////////////refund the money due to saldo sds kurang////////////////////////////////////////////////////////////////////////
            refund(Double.parseDouble(nominal.substring(4)), "Refund trx postpaid " + e.getMessage(), Integer.parseInt(trx_id), Integer.parseInt(user_id), admin, product_id);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
        } finally {
            try {
                client.close();
            } catch (Exception e) {
                logger.log(Level.FATAL, e);
            }
        }

        return resultIso;
    }

    public String[] parsePaymentMsgResponse(String iso, boolean isLogged) {
        String[] hasil = new String[17];
        System.out.println("ISO to parse : " + iso);
        try {

            if (iso.length() > 10) {

                ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");

                ISOMsg isoMsg = new ISOMsg();
                isoMsg.setPackager(packager);
                isoMsg.unpack(iso.getBytes());

                hasil[0] = isoMsg.getMTI();
                hasil[1] = isoMsg.getString(2); //pan
                hasil[2] = isoMsg.getString(4); //trx amount
                hasil[3] = isoMsg.getString(11); //stan
                hasil[4] = isoMsg.getString(12); //datetime trx
                hasil[5] = isoMsg.getString(15); //date settlement
                hasil[6] = isoMsg.getString(26); //merchant cat code
                hasil[7] = isoMsg.getString(32); //bank code
                hasil[8] = isoMsg.getString(33); //cid
                hasil[9] = isoMsg.getString(39);
                logger.log(Level.INFO, "RC : " + hasil[9]);//RC
                hasil[10] = isoMsg.getString(41); //terminal id
                hasil[11] = isoMsg.getString(48); //private data
                hasil[12] = isoMsg.getString(63); //info text

            } else {
                hasil[9] = "";
            }

            return hasil;
        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

    public String[] parseReversalMsgResponse(String iso, boolean isLogged, boolean isRepeat) {
        String[] hasil = new String[17];

        try {
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.unpack(iso.getBytes());

            hasil[0] = isoMsg.getMTI();
            hasil[1] = isoMsg.getString(2);
            hasil[2] = isoMsg.getString(4);
            hasil[3] = isoMsg.getString(11);
            hasil[4] = isoMsg.getString(12);
            hasil[5] = isoMsg.getString(15);
            hasil[6] = isoMsg.getString(26);
            hasil[7] = isoMsg.getString(32);
            hasil[8] = isoMsg.getString(33);
            hasil[9] = isoMsg.getString(39);
            hasil[10] = isoMsg.getString(41);
            hasil[11] = isoMsg.getString(48);
            hasil[12] = isoMsg.getString(62);
            hasil[13] = isoMsg.getString(63);

        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

    public String[] parseBit48InquiryResponseSA_BPJS(String msg, String rc) {

        String[] hasil = new String[65];

        int[] seq = {25, 30, 4, 4, 8, 12, 8, 12};

        String[] title = {
            "idpel",
            "name",
            "angsuran",
            "period",
            "due_date",
            "tagihan",
            "admin_charge",
            "total_bayar"
        };
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";
                if (rc.equalsIgnoreCase("0000")) {
                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
//                        logger.log(Level.INFO, String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l);
//                        logger.log(Level.INFO, String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                n = l;
            }

        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

    public String[] parseBit48PaymentResponseSA_BPJS(String msg, String rc) {

        String[] hasil = new String[65];

        int[] seq = {7, 18, 2, 30, 6, 100, 12, 12, 12, 12, 32, 14};

        String[] title = {
            "switcher id",
            "No VA",
            "jumlah bulan",
            "nama",
            "kode cabang",
            "nama cabang",
            "biaya premu",
            "biaya admin",
            "Rp. Total",
            "sisa",
            "JPA Reff",
            "Tgl Lunas"
        };
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";
                if (rc.equalsIgnoreCase("0000")) {
                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
//                        logger.log(Level.INFO, String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l);
//                        logger.log(Level.INFO, String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                n = l;
            }

        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

    public String[] parseBit63PaymentResponseSA_BPJS(String msg, String rc) {

        String[] hasil = new String[65];

//        int[] seq = {32, 30, 50, 18, 4};
        int[] seq = {32, 30, 50, 18, 4, 30, 10};

        String[] title = {
            "kode_loket",
            "nama_loket",
            "alamat_loket",
            "phone_loket",
            "kode_kab_kota",
            "kab_kota",
            "kode_pos"
        };
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";
                if (rc.equalsIgnoreCase("0000")) {
                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
//                        logger.log(Level.INFO, String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l);
//                        logger.log(Level.INFO, String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                n = l;
            }

        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

    public static String[] parseBit48Inquiry(String msg, String rc) {
        String[] hasil = new String[65];

        int[] seq1 = {7, 12, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
        };

        int[] seq2 = {7, 12, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
        };

        int[] seq3 = {7, 12, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
        };

        int[] seq4 = {7, 12, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
        };

        int[] seq = null;

        if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("1")) {
            seq = seq1;
        } else if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("2")) {
            seq = seq2;
        } else if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("3")) {
            seq = seq3;
        } else if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("4")) {
            seq = seq4;
        }

        String[] title = {
            "switcher id",
            "subscriber id",
            "bill status",
            "total outstanding bill",
            "switcher refnum",
            "subscriber name",
            "service unit",
            "service unit phone",
            "subscriber segmentation",
            "power consuming category",
            "total admin charges",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3"
        };
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";

                if (rc.equalsIgnoreCase("0000")) {
                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
                        logger.log(Level.INFO, String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l);
                        logger.log(Level.INFO, String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                n = l;
            }

        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

    public static String[] parseBit48Payment(String msg, String rc) {
        String[] hasil = new String[65];

        int[] seq1 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
        };

        int[] seq2 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
        };

        int[] seq3 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
        };

        int[] seq4 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
        };

        int[] seq = null;

        if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("1")) {
            seq = seq1;
        } else if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("2")) {
            seq = seq2;
        } else if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("3")) {
            seq = seq3;
        } else if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("4")) {
            seq = seq4;
        }

        String[] title = {
            "switcher id",
            "subscriber id",
            "bill status",
            "Payment status",
            "total outstanding bill",
            "switcher refnum",
            "subscriber name",
            "service unit",
            "service unit phone",
            "subscriber segmentation",
            "power consuming category",
            "total admin charges",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3"
        };
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";

                if (rc.equalsIgnoreCase("0000") || rc.equalsIgnoreCase("0012")) {
                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
                        logger.log(Level.INFO, String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l);
                        logger.log(Level.INFO, String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                n = l;
            }

        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

    public String[] parseInquiryMsgResponse(String iso, boolean isLogged) {
        String[] hasil = new String[17];
        String[] useBit = {
            "MTI",
            "2",
            "4",
            "11",
            "12",
            "26",
            "32",
            "33",
            "39",
            "41",
            "48"};
        try {
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.unpack(iso.getBytes());

            hasil[0] = isoMsg.getMTI();
            for (int i = 1; i < useBit.length; i++) {
                String useBit1 = useBit[i];
                try {
                    hasil[i] = isoMsg.getString(Integer.parseInt(useBit1));
                } catch (Exception e) {
                    hasil[i] = "";
                }
            }
//            hasil[1] = isoMsg.getString(2);  //pan
//            hasil[2] = isoMsg.getString(4); //transaksi amount
//            hasil[3] = isoMsg.getString(11); //stan
//            hasil[4] = isoMsg.getString(12); // date time
//            hasil[5] = isoMsg.getString(26); // merchant cat code
//            hasil[6] = isoMsg.getString(32); // bank code
//            hasil[7] = isoMsg.getString(33); //cid
//            hasil[8] = isoMsg.getString(39); //RC
//            hasil[9] = isoMsg.getString(41); //Terminal ID
//            hasil[10] = isoMsg.getString(48); //private data

        } catch (Exception e) {
            logger.log(Level.FATAL, "parse inq msg respon : " + e.toString());
            //e.printStackTrace();
        }
        System.out.println("hasil parse : " + hasil[8]);
        return hasil;
    }

    public String[] parseNetworkMsgResponse(String iso, boolean isLogged) {
        String[] hasil = new String[10];

        try {
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.unpack(iso.getBytes());

            hasil[0] = isoMsg.getMTI();
            hasil[1] = isoMsg.getString(12); //datetime
            hasil[2] = isoMsg.getString(33); //cid
            hasil[3] = isoMsg.getString(39); //RC
            hasil[4] = isoMsg.getString(40); //type
            hasil[5] = isoMsg.getString(41); //terminal id
            hasil[6] = isoMsg.getString(48); //priv msg

            String typeMsg = "";
            if (hasil[4].equalsIgnoreCase("001")) { //sign on
                typeMsg = "SIGNON";
            } else if (hasil[4].equalsIgnoreCase("002")) { //sign off
                typeMsg = "SIGNOFF";
            } else if (hasil[4].equalsIgnoreCase("301")) { //echo test
                typeMsg = "ECHO";
            } else {
                typeMsg = "SIGNON";
            }

            return hasil;
        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

    public String inquiry(String id_pel, String jumlahBayar, String user_id, String inbox_id,
            String merchant_cat_code, String terminal_id, Socket client, String tgl, String[] userData) {
        String hasil = "";

        try {
            if (terminal_id.length() < 12) {
                terminal_id = String.format("%16s", terminal_id).replace(' ', '0');
            }

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");
            
            // String tgl = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2100");
            isoMsg.set(2, "99508");
//            isoMsg.set(2, "00339");
            isoMsg.set(11, String.format("%12s", inbox_id).replace(' ', '0'));
            isoMsg.set(12, tgl);
            isoMsg.set(26, merchant_cat_code);
            isoMsg.set(32, this.bank_code);
            isoMsg.set(33, this.cid);
            isoMsg.set(41, terminal_id);

            logger.log(Level.INFO, "switcher id : " + this.switcher_id + ", id pel : " + id_pel);

            String bit48 = this.switcher_id + String.format("%-18s", id_pel) + jumlahBayar;

            isoMsg.set(48, bit48);
//            isoMsg.set(49, "360");
            String bit63 = String.format("%-32s", userData[0])
                    + String.format("%-30s", userData[1])
                    + String.format("%-50s", userData[2])
                    + String.format("%-18s", userData[3])
                    + String.format("%-4s", userData[6].replace(".", ""))
                    + String.format("%-30s", userData[5].replace(".", ""))
                    + String.format("%-10s", userData[7].replace(".", ""))
                    ;
//            ;
//note: bypass dulu (testing tahap 1 tanpa merubah SA Symphoni)            
            isoMsg.set(63, bit63);

            //////////////////SENDING ISO MSG////////////////////
            byte[] datax = isoMsg.pack();
            logger.log(Level.INFO, "SENDING : " + new String(datax));
            String networkRequest = new String(datax);

            String trailer = new String(new char[]{10});

            networkRequest = networkRequest + trailer;

//            DataOutputStream outgoing = new DataOutputStream(client.getOutputStream());
//            DataInputStream incoming = new DataInputStream(client.getInputStream());
//
//            outgoing.writeBytes(networkRequest);
            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            outgoing.print(networkRequest);
            outgoing.flush();

            int data;
            StringBuffer sb = new StringBuffer();

            while ((data = incoming.read()) != -1) {
                if (data == -1 || data == 255 || data == 10 || data == 65533) {
                    break;
                }

                sb.append((char) data);
            }

            logger.log(Level.INFO, "RECEIVE : " + sb.toString());

//            incoming.close();
//            outgoing.close();
            return sb.toString();

        } catch (ConnectException e) {
            logger.log(Level.FATAL, "Connect exception Postpaid inquiry : " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            logger.log(Level.FATAL, "Error Postpaid inquiry : " + e.getMessage());
        }

        return "Error";
    }

    public String networkMsg(String type, String terminal_id, Socket client) {
        String hasil = "";
        String typeMsg = "";

        try {

            if (terminal_id.length() < 12) {
                terminal_id = String.format("%16s", terminal_id).replace(' ', '0');
            }

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");
            //ISOPackager packager = new CustomPackager2003();

            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2800");
            isoMsg.set(12, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
            isoMsg.set(33, this.cid);

            if (type.equalsIgnoreCase("1")) { //sign on
                isoMsg.set(40, "001");
                typeMsg = "SIGNON";
            } else if (type.equalsIgnoreCase("2")) { //sign off
                isoMsg.set(40, "002");
                typeMsg = "SIGNOFF";
            } else if (type.equalsIgnoreCase("3")) { //echo test
                isoMsg.set(40, "301");
                typeMsg = "ECHO";
            } else {
                isoMsg.set(40, "001");
                typeMsg = "SIGNON";
            }

            isoMsg.set(41, terminal_id);
            isoMsg.set(48, switcher_id);

            byte[] datax = isoMsg.pack();
            logger.log(Level.INFO, "SENDING : " + new String(datax));
            String networkRequest = new String(datax);

            //////////////////SENDING ISO MSG////////////////////
            String trailer = new String(new char[]{10});

            networkRequest = networkRequest + trailer;

//            DataOutputStream outgoing = new DataOutputStream(client.getOutputStream());
//            DataInputStream incoming = new DataInputStream(client.getInputStream());
//
//            outgoing.writeBytes(networkRequest);
            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            outgoing.print(networkRequest);
            outgoing.flush();

            int data;
            StringBuffer sb = new StringBuffer();

            while ((data = incoming.read()) != -1) {
                if (data == -1 || data == 255 || data == 10 || data == 65533) {
                    break;
                }

                sb.append((char) data);
            }

            logger.log(Level.INFO, "RECEIVE : " + sb.toString());

//            incoming.close();
//            outgoing.close();
            return sb.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return hasil;
    }

    public String payment(String id_pel, String nominal, String trx_id, String user_id,
            String merchant_cat_code, String terminal_id,
            String inquiry_response48, String inquiry_response62, Socket client, String tgl, String[] userData) {
        String hasil = "";

        try {

            if (terminal_id.length() < 12) {
                terminal_id = String.format("%16s", terminal_id).replace(' ', '0');
            }

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2200");
//            isoMsg.set(2, "00339");
            isoMsg.set(2, "99508");

            String curr_code = "360";
            String minor_unit = "0";
            String amount = String.format("%12s", nominal).replace(' ', '0');

            String bit4 = nominal; //curr_code + minor_unit + amount;

            isoMsg.set(4, bit4);

            //String tgl = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            isoMsg.set(11, String.format("%12s", trx_id).replace(' ', '0'));
            isoMsg.set(12, tgl);
            isoMsg.set(26, merchant_cat_code);
            isoMsg.set(32, this.bank_code);
            isoMsg.set(33, this.cid);
            isoMsg.set(41, terminal_id);
            isoMsg.set(48, inquiry_response48);

            String bit63 = String.format("%-32s", userData[0])
                    + String.format("%-30s", userData[1])
                    + String.format("%-50s", userData[2])
                    + String.format("%-18s", userData[3])
                    + String.format("%-4s", userData[6].replace(".", ""))
                    + String.format("%-30s", userData[5].replace(".", ""))
                    + String.format("%-10s", userData[7].replace(".", ""))
                    ;

            logger.log(Level.INFO, "Bit 63 : " + bit63);
//            searchingKabupatenKota();

            isoMsg.set(63, bit63);
            //isoMsg.set(62,inquiry_response62);

            String bit56x = "2200" + String.format("%12s", trx_id).replace(' ', '0') + tgl + this.bank_code;
            logger.log(Level.INFO, "Bit 56 : " + bit56x);

            byte[] datax = isoMsg.pack();
            logger.log(Level.INFO, "SENDING : " + new String(datax));

//            System.exit(1);
            String networkRequest = new String(datax);

            //////////////////SENDING ISO MSG////////////////////
            String trailer = new String(new char[]{10});

            networkRequest = networkRequest + trailer;

//            DataOutputStream outgoing = new DataOutputStream(client.getOutputStream());
//            DataInputStream incoming = new DataInputStream(client.getInputStream());
//
//            outgoing.writeBytes(networkRequest);
            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            outgoing.print(networkRequest);
            outgoing.flush();

            int data;
            StringBuffer sb = new StringBuffer();

            while ((data = incoming.read()) != -1) {
                if (data == -1 || data == 255 || data == 10 || data == 65533) {
                    break;
                }

                sb.append((char) data);
            }

            logger.log(Level.INFO, "RECEIVE : " + sb.toString());

//            incoming.close();
//            outgoing.close();
            return sb.toString();

        } catch (Exception e) {
            logger.log(Level.ERROR, e.getMessage());
        }

        return hasil;
    }

    public String adviceBpjs(String id_pel, String nominal, String trx_id, String user_id,
            String merchant_cat_code, String terminal_id,
            String inquiry_response48, String inquiry_response62, Socket client, String tgl, String[] userData, int isRepeat) {
        String hasil = "";

        try {

            if (terminal_id.length() < 12) {
                terminal_id = String.format("%16s", terminal_id).replace(' ', '0');
            }

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
//            isoMsg.setMTI("2200");
            isoMsg.setMTI("2220");
            if (isRepeat > 0) {
                isoMsg.setMTI("2221");
            }
//            isoMsg.set(2, "00339");
            isoMsg.set(2, "99508");

            String curr_code = "360";
            String minor_unit = "0";
            String amount = String.format("%12s", nominal).replace(' ', '0');

            String bit4 = nominal; //curr_code + minor_unit + amount;

            isoMsg.set(4, bit4);

            //String tgl = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            isoMsg.set(11, String.format("%12s", trx_id).replace(' ', '0'));
            isoMsg.set(12, tgl);
            isoMsg.set(26, merchant_cat_code);
            isoMsg.set(32, this.bank_code);
            isoMsg.set(33, this.cid);
            isoMsg.set(41, terminal_id);
            isoMsg.set(48, inquiry_response48);

            String bit63 = String.format("%-32s", userData[0])
                    + String.format("%-30s", userData[1])
                    + String.format("%-50s", userData[2])
                    + String.format("%-18s", userData[3])
                    + String.format("%-4s", userData[6].replace(".", ""))
                    + String.format("%-30s", userData[5].replace(".", ""))
                    + String.format("%-10s", userData[7].replace(".", ""))
                    ;
            isoMsg.set(63, bit63);

//isoMsg.set(62,inquiry_response62);
//
//            String bit56x = "2200" + String.format("%12s", trx_id).replace(' ', '0') + tgl + this.bank_code;
//            logger.log(Level.INFO, "Bit 56 : " + bit56x);
            byte[] datax = isoMsg.pack();
            logger.log(Level.INFO, "SENDING : " + new String(datax));
            String networkRequest = new String(datax);

            //////////////////SENDING ISO MSG////////////////////
            String trailer = new String(new char[]{10});

            networkRequest = networkRequest + trailer;

//            DataOutputStream outgoing = new DataOutputStream(client.getOutputStream());
//            DataInputStream incoming = new DataInputStream(client.getInputStream());
//
//            outgoing.writeBytes(networkRequest);
            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            outgoing.print(networkRequest);
            outgoing.flush();

            int data;
            StringBuffer sb = new StringBuffer();

            while ((data = incoming.read()) != -1) {
                if (data == -1 || data == 255 || data == 10 || data == 65533) {
                    break;
                }

                sb.append((char) data);
            }

            logger.log(Level.INFO, "RECEIVE : " + sb.toString());

//            incoming.close();
//            outgoing.close();
            return sb.toString();

        } catch (Exception e) {
            logger.log(Level.ERROR, e.getMessage());
        }

        return hasil;
    }

    public String repeat(String id_pel, String nominal, String trx_id, String user_id,
            String merchant_cat_code, String terminal_id,
            String inquiry_response48, String inquiry_response62, Socket client, boolean isRepeat, String tgl) {
        String hasil = "";

        try {

            if (terminal_id.length() < 12) {
                terminal_id = String.format("%16s", terminal_id).replace(' ', '0');
            }

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2401");
            isoMsg.set(2, "99501");

            String curr_code = "360";
            String minor_unit = "0";
            String amount = String.format("%12s", nominal).replace(' ', '0');

            String bit4 = curr_code + minor_unit + amount;

            //String tgl = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            isoMsg.set(4, nominal);
            isoMsg.set(11, String.format("%12s", trx_id).replace(' ', '0'));
            isoMsg.set(12, tgl);
            isoMsg.set(26, merchant_cat_code);
            isoMsg.set(32, this.bank_code);
            isoMsg.set(33, this.cid);
            isoMsg.set(41, terminal_id);
            isoMsg.set(48, inquiry_response48);

            String bit56x = "2200" + String.format("%12s", trx_id).replace(' ', '0') + tgl + this.bank_code;
            isoMsg.set(56, bit56x);
            //isoMsg.set(62,inquiry_response62);

            byte[] datax = isoMsg.pack();
            logger.log(Level.INFO, "SENDING : " + new String(datax));
            String networkRequest = new String(datax);

            //////////////////SENDING ISO MSG////////////////////
            String trailer = new String(new char[]{10});

            networkRequest = networkRequest + trailer;

//            DataOutputStream outgoing = new DataOutputStream(client.getOutputStream());
//            DataInputStream incoming = new DataInputStream(client.getInputStream());
//
//            outgoing.writeBytes(networkRequest);
            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            outgoing.print(networkRequest);
            outgoing.flush();

            int data;
            StringBuffer sb = new StringBuffer();

            while ((data = incoming.read()) != -1) {
                if (data == -1 || data == 255 || data == 10 || data == 65533) {
                    break;
                }

                sb.append((char) data);
            }

            logger.log(Level.INFO, "RECEIVE : " + sb.toString());

//            incoming.close();
//            outgoing.close();
            return sb.toString();

        } catch (Exception e) {
            logger.log(Level.ERROR, e);
        }

        return hasil;
    }

    public String reversal(String id_pel, String nominal, String trx_id, String user_id,
            String merchant_cat_code, String terminal_id,
            String inquiry_response48, String inquiry_response62, Socket client, boolean isRepeat, String tgl) {
        String hasil = "";

        try {

            if (terminal_id.length() < 12) {
                terminal_id = String.format("%16s", terminal_id).replace(' ', '0');
            }

            // membuat sebuah packager
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");

            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI("2400");
            isoMsg.set(2, "99501");

            String curr_code = "360";
            String minor_unit = "0";
            String amount = String.format("%12s", nominal).replace(' ', '0');

            String bit4 = curr_code + minor_unit + amount;

            //String tgl = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            isoMsg.set(4, nominal);
            isoMsg.set(11, String.format("%12s", trx_id).replace(' ', '0'));
            isoMsg.set(12, tgl);
            isoMsg.set(26, merchant_cat_code);
            isoMsg.set(32, this.bank_code);
            isoMsg.set(33, this.cid);
            isoMsg.set(41, terminal_id);
            isoMsg.set(48, inquiry_response48);

//            String bit56x = "2200" + String.format("%12s", trx_id).replace(' ', '0') + tgl + this.bank_code;
//            logger.log(Level.INFO, "bit56x : " + bit56x);
//            isoMsg.set(56, bit56x);
            //isoMsg.set(62,inquiry_response62);
            byte[] datax = isoMsg.pack();
            logger.log(Level.INFO, "SENDING : " + new String(datax));
            String networkRequest = new String(datax);

            //////////////////SENDING ISO MSG////////////////////
            String trailer = new String(new char[]{10});

            networkRequest = networkRequest + trailer;

//            DataOutputStream outgoing = new DataOutputStream(client.getOutputStream());
//            DataInputStream incoming = new DataInputStream(client.getInputStream());
//
//            outgoing.writeBytes(networkRequest);
            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            outgoing.print(networkRequest);
            outgoing.flush();

            int data;
            StringBuffer sb = new StringBuffer();

            while ((data = incoming.read()) != -1) {
                if (data == -1 || data == 255 || data == 10 || data == 65533) {
                    break;
                }

                sb.append((char) data);
            }

            logger.log(Level.INFO, "RECEIVE : " + sb.toString());

            return sb.toString();

        } catch (Exception e) {
            logger.log(Level.ERROR, e);
        }

        return hasil;
    }

}
